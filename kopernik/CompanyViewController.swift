//
//  CompanyViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/10/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class CompanyViewController: UIViewController {

    @IBOutlet weak var companyImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if(LANG == "PL") {
            companyImage.image = UIImage(named: "company_pl")
        }
        else {
            companyImage.image = UIImage(named: "company_en")
        }
        
        self.addDownImage()
    }
    
    func addDownImage() {
        let image = UIImageView()
        let height = floor(BUTTON_WIDTH * (103/196))
        let screenSize = UIScreen.main.bounds
        
        image.frame = CGRect(x: screenSize.width - BUTTON_WIDTH * 1.5, y: screenSize.height - BUTTON_WIDTH * 1.5, width: BUTTON_WIDTH, height: height)
        image.image = UIImage(named: "prod_nav_down")
        self.view.addSubview(image)
    }

}
