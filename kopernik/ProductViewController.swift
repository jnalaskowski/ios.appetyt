//
//  ProductViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/10/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {

    var pageIndex:Int = 0
    var productImage:String!
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = UIImage(named: productImage)
    }
    
    
}
