//
//  HomeViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var favoritesBtnOutlet: UIButton!
    @IBOutlet weak var settingsBtnOutlet: UIButton!
    @IBOutlet weak var recipiesBtnOutlet: UIButton!
    @IBOutlet weak var companyBtnOutlet: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    
    
    override func viewDidLoad() {
        
        let width = floor(backgroundImage.frame.width / 10) // 1/10 szerokości ekranu
        BUTTON_WIDTH = width
        
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        setButtonsText(lang: LANG)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "favoritesSegue") {
            let vc = segue.destination as! RecipiesCollectionViewController
            vc.isFavoritesMode = true
        }
    }
    
    func setButtonsText(lang:String) {
        if(lang == "PL") {
            favoritesBtnOutlet.setImage(UIImage(named: "homeBtnFavorites"), for: .normal)
            settingsBtnOutlet.setImage(UIImage(named: "homeBtnSettings"), for: .normal)
            recipiesBtnOutlet.setImage(UIImage(named: "homeBtnRecipies"), for: .normal)
            companyBtnOutlet.setImage(UIImage(named: "homeBtnCompany"), for: .normal)
        }
        else {
            favoritesBtnOutlet.setImage(UIImage(named: "homeBtnFavoritesEn"), for: .normal)
            settingsBtnOutlet.setImage(UIImage(named: "homeBtnSettingsEn"), for: .normal)
            recipiesBtnOutlet.setImage(UIImage(named: "homeBtnRecipiesEn"), for: .normal)
            companyBtnOutlet.setImage(UIImage(named: "homeBtnCompanyEn"), for: .normal)
        }
    }
}


// rozszerzenia
extension UIImage {
    
    func scaledImage(withSize size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    func scaleImageToFitSize(size: CGSize) -> UIImage {
        let aspect = self.size.width / self.size.height
        if size.width / aspect <= size.height {
            return scaledImage(withSize: CGSize(width: size.width, height: size.width / aspect))
        } else {
            return scaledImage(withSize: CGSize(width: size.height * aspect, height: size.height))
        }
    }
    
}
