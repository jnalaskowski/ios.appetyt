//
//  RecipiesCollectionViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 25/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class RecipiesCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var recipiesCollectionView: UICollectionView!
    
    var recipes:[Recipe] = RECIPES
    let favorites = UDFavorites()
    var isReloaded:Bool = false
    var isFavoritesMode:Bool = false
    
    override func viewDidLoad() {
        self.isReloaded = true
        recipiesCollectionView.dataSource = self
        recipiesCollectionView.delegate = self
        
        if(self.isFavoritesMode) {
            self.getFavoritesArray()
        }
        
        self.addHomeButton()
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(!self.isReloaded) {
            if(self.isFavoritesMode) {
                self.getFavoritesArray()
            }
            recipiesCollectionView.reloadData()
        }
        self.isReloaded = true
        

    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func getFavoritesArray() {
        //utworzenie tablicy z ulubionymi
        var tempArray:[Recipe] = []
        for favoriteIndex in favorites.getList() {
            tempArray.append(RECIPES[favoriteIndex])
        }
        self.recipes = tempArray
    }
    
    func addHomeButton() {
        let button = UIButton(type: .system)
        
        let height = floor(BUTTON_WIDTH * (89/109))
        button.frame = CGRect(x: BUTTON_WIDTH/2, y: BUTTON_WIDTH/2, width: BUTTON_WIDTH, height: height)
        button.setBackgroundImage(UIImage(named: "returnBtn"), for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // ilość kafelek
        return self.recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // rozmiar kafelek
        let cubesInRow = 2
        
        let width = floor(recipiesCollectionView.frame.width / CGFloat(cubesInRow))
        let height = floor(width * (810/540))
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // uzupełnianie kafelek
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeCell", for: indexPath as IndexPath) as! RecipiesCollectionViewCell
            cell.recipeID = self.recipes[indexPath.item].recipeID
            cell.titleLabel.text = self.recipes[indexPath.item].recipeName
            cell.thumbImage.image = self.recipes[indexPath.item].recipeThumb
            cell.favoriteViewConstraintWidth.constant = BUTTON_WIDTH * 1.2
            cell.titleViewConstraintHeight.constant = BUTTON_WIDTH * 1.5
        
            if(favorites.checkList(recipeId: self.recipes[indexPath.item].recipeID)) {
                cell.favoriteBtnOutlet.setImage(UIImage(named: "favoritesBtnActive"), for: .normal)
            }
            else {
                cell.favoriteBtnOutlet.setImage(UIImage(named: "favoritesBtn"), for: .normal)
            }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // animacja kafelek
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.duration = 0.3
        animation.fromValue = 0.6
        animation.toValue = 1
        animation.isRemovedOnCompletion = false
        cell.layer.add(animation, forKey: animation.keyPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "recipeSeque") {
            let indexPaths = self.recipiesCollectionView!.indexPathsForSelectedItems
            let indexPath = indexPaths?[0]
            
            let vc = segue.destination as! RecipePageViewController
            vc.recipeIndex = (indexPath?.item)!
            vc.recipes = self.recipes
            self.isReloaded = false
        }
    }
}
