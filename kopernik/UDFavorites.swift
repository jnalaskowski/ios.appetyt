//
//  UDFavorites.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 25/09/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import Foundation

class UDFavorites {
    
    var userDefaults = UserDefaults.standard
    
    init() {
        
    }
    
    //usunięcie lub dodanie do ulubionych
    func updateList(recipeId:Int) {
        
        //wczytanie listy
        var favorites:[Int] = []
        if(userDefaults.array(forKey: "favorite") != nil) {
            favorites = userDefaults.array(forKey: "favorite") as! [Int]
        }
        
        //usunięcie lub dodanie przepisu do listy zakupów
        if let index = favorites.firstIndex(of: recipeId) {
            favorites.remove(at: index)
        }
        else {
            favorites.append(recipeId)
        }
        
        //zapisanie listy
        userDefaults.setValue(favorites, forKeyPath: "favorite")
    }
    
    //zwraca ilość ulubionych
    func getCount() -> Int {
        //wczytanie kroków do listy
        var favorites:[Int] = []
        if(userDefaults.array(forKey: "favorite") != nil) {
            favorites = userDefaults.array(forKey: "favorite") as! [Int]
        }
        return favorites.count
    }
    
    //sprawdzenie czy przepis jest ulubiony
    func checkList(recipeId:Int) -> Bool {
        
        //wczytanie kroków do listy
        var favorites:[Int] = []
        if(userDefaults.array(forKey: "favorite") != nil) {
            favorites = userDefaults.array(forKey: "favorite") as! [Int]
        }
        
        //sprawdzenie
        if favorites.firstIndex(of: recipeId) != nil {
            return true
        }
        else {
            return false
        }
    }
    
    //przekazanie listy ulubionych
    func getList() -> [Int] {
        //wczytanie listy
        var favorites:[Int] = []
        if(userDefaults.array(forKey: "favorite") != nil) {
            favorites = userDefaults.array(forKey: "favorite") as! [Int]
        }
        return favorites
    }
}

