//
//  CompanyPageViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 25/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class CompanyPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    lazy var viewControllerList:[UIViewController] = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        let vc1 = sb.instantiateViewController(withIdentifier: "company1")
        let vc2 = sb.instantiateViewController(withIdentifier: "products")
        
        return [vc1, vc2]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = self
        self.addHomeButton()
        
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func addHomeButton() {
        let button = UIButton(type: .system)
        let height = floor(BUTTON_WIDTH * (89/109))
        button.frame = CGRect(x: BUTTON_WIDTH/2, y: BUTTON_WIDTH/2, width: BUTTON_WIDTH, height: height)
        button.setBackgroundImage(UIImage(named: "returnBtn"), for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else {return nil}

        let previousIndex = vcIndex - 1
        
        guard  previousIndex >= 0 else {return nil}

        guard viewControllerList.count > previousIndex else {return nil}

        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {

        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else {return nil}
        
        let nextIndex = vcIndex + 1
        
        guard viewControllerList.count != nextIndex else {return nil}
        
        guard viewControllerList.count > nextIndex else {return nil}
        
        return viewControllerList[nextIndex]
    }

}
