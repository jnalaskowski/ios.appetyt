//
//  RecipePageViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 26/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class RecipePageViewController: UIPageViewController, UIPageViewControllerDataSource {

    var recipeIndex:Int!
    var recipes:[Recipe]!
    
    var arrPageTitle: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                        
        self.dataSource = self
        
        self.addHomeButton()

        self.setViewControllers([getViewControllerAtIndex(index: recipeIndex)] as [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func addHomeButton() {
        let button = UIButton(type: .system)
        let height = floor(BUTTON_WIDTH * (89/109))
        button.frame = CGRect(x: BUTTON_WIDTH/2, y: BUTTON_WIDTH/2, width: BUTTON_WIDTH, height: height)
        button.setBackgroundImage(UIImage(named: "returnBtn"), for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    
    @objc func buttonAction(sender: UIButton!) {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        let pageContent: RecipeViewController = viewController as! RecipeViewController
        var index = pageContent.pageIndex
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        index = index - 1;
        return getViewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
 
        let pageContent: RecipeViewController = viewController as! RecipeViewController
        var index = pageContent.pageIndex
        if (index == NSNotFound)
        {
            return nil;
        }
        index = index + 1;
        if (index == self.recipes.count)
        {
            return nil;
        }
        return getViewControllerAtIndex(index: index)
    }
    
    func getViewControllerAtIndex(index: NSInteger) -> RecipeViewController
    {
        // Create a new view controller and pass suitable data.
        let recipeViewController = self.storyboard?.instantiateViewController(withIdentifier: "recipeVC") as! RecipeViewController
        
        recipeViewController.pageIndex = index
        recipeViewController.recipe = self.recipes[index]
        
        return recipeViewController
    }

}
