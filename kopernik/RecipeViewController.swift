//
//  RecipeViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 26/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController {

    @IBOutlet weak var recipeBtnOutlet: UIButton!
    @IBAction func favoriteBtnAction(_ sender: UIButton) {
        self.favorites.updateList(recipeId: self.recipe.recipeID)
        self.refreshFavoriteBtn()
    }
    
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var favoriteBtnOutlet: UIButton!
    @IBOutlet weak var guestOutlet: UIImageView!
    @IBOutlet weak var recipeBtnConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var favoriteBtnConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var favoriteBtnConstraintRight: NSLayoutConstraint!
    @IBOutlet weak var favoriteBtnConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var guestConstraintRight: NSLayoutConstraint!
    @IBOutlet weak var guestConstraintWidth: NSLayoutConstraint!
    
    
    
    var recipe:Recipe!
    var pageIndex:Int = 0
    let favorites = UDFavorites()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        recipeTitle.text = self.recipe.recipeName
        recipeImage.image = UIImage(named: self.recipe.recipeImage)
        recipeImage.clipsToBounds = true
        favoriteBtnConstraintWidth.constant = BUTTON_WIDTH
        favoriteBtnConstraintTop.constant = BUTTON_WIDTH / 2
        favoriteBtnConstraintRight.constant = BUTTON_WIDTH / 2
        recipeBtnConstraintWidth.constant = BUTTON_WIDTH * 3
        guestConstraintRight.constant = 0
        guestConstraintWidth.constant = BUTTON_WIDTH * 5
        
        self.drawScreen()
        
        self.refreshFavoriteBtn()
    }
    
    private func drawScreen() {
        
        recipeTitle.text = self.recipe.recipeName
        recipeImage.image = UIImage(named: self.recipe.recipeImage)
        recipeImage.clipsToBounds = true
        favoriteBtnConstraintWidth.constant = BUTTON_WIDTH
        favoriteBtnConstraintTop.constant = BUTTON_WIDTH / 2
        favoriteBtnConstraintRight.constant = BUTTON_WIDTH / 2
        recipeBtnConstraintWidth.constant = BUTTON_WIDTH * 3
        guestConstraintRight.constant = 0
        guestConstraintWidth.constant = BUTTON_WIDTH * 5
        
        if(LANG == "PL"){
            guestOutlet.image = UIImage(named: "r_image_ico_pl_guest")
            recipeBtnOutlet.setImage(UIImage(named: "r_image_ico_pl_method"), for: .normal)
        }
        else {
            guestOutlet.image = UIImage(named: "r_image_ico_en_guest")
            recipeBtnOutlet.setImage(UIImage(named: "r_image_ico_en_method"), for: .normal)
        }
        
        if(self.recipe.recipeID < 0) {
            if(self.recipe.recipeID == 0 && LANG == "PL") {placeImage.image = UIImage(named: "r_image_ico_pl_first")}
            if(self.recipe.recipeID == 0 && LANG == "EN") {placeImage.image = UIImage(named: "r_image_ico_en_first")}
            if(self.recipe.recipeID == 1 && LANG == "PL") {placeImage.image = UIImage(named: "r_image_ico_pl_second")}
            if(self.recipe.recipeID == 1 && LANG == "EN") {placeImage.image = UIImage(named: "r_image_ico_en_second")}
            if(self.recipe.recipeID == 2 && LANG == "PL") {placeImage.image = UIImage(named: "r_image_ico_pl_third")}
            if(self.recipe.recipeID == 2 && LANG == "EN") {placeImage.image = UIImage(named: "r_image_ico_en_third")}
            placeImage.alpha = 1
        }
        
        if(recipe.recipeGuest) {
            guestOutlet.alpha = 1
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func refreshFavoriteBtn() {
        if(favorites.checkList(recipeId: self.recipe.recipeID)) {
            favoriteBtnOutlet.setImage(UIImage(named: "favoritesBtnActive"), for: .normal)
        }
        else {
            favoriteBtnOutlet.setImage(UIImage(named: "favoritesBtn"), for: .normal)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "methodSeque") {
            
            let vc = segue.destination as! MethodViewController
            vc.recipe = self.recipe
        }
    }

}
