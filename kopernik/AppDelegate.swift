//
//  AppDelegate.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //lokal z telefonu
        if(NSLocale.preferredLanguages.first!.prefix(2) == "pl") {
            LANG = "PL"
        }
        
        //dane z UD
        let settings = UDSettings()
        if(settings.getLang() != "err") {
            LANG = settings.getLang()
        }
        
        UNIT = settings.getUnit()
        RECIPES = loadData(lang: LANG)

        return false
    }

}

