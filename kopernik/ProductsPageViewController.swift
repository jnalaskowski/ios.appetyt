//
//  ProductsPageViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 25/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class ProductsPageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var products:[String] = ["prod1_pl","prod2_pl","prod3_pl","prod4_pl","prod5_pl","prod6_pl"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addImages()
        
        if(LANG == "EN") {
            products = ["prod1_en","prod2_en","prod3_en","prod4_en","prod5_en","prod6_en"]
        }
        
        self.dataSource = self
        
        self.setViewControllers([getViewControllerAtIndex(index: 0)] as [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func addImages() {
        let imageLeft = UIImageView()
        let imageRight = UIImageView()
        let width = BUTTON_WIDTH * 0.7
        let height = floor(width * (197/103))
        let screenSize = UIScreen.main.bounds
        
        imageLeft.frame = CGRect(x: width * 0.5, y: (screenSize.height / 2) - width, width: width, height: height)
        imageRight.frame = CGRect(x: screenSize.width - width * 1.5, y: (screenSize.height / 2) - width, width: width, height: height)
        
        imageLeft.image = UIImage(named: "prod_nav_left")
        imageRight.image = UIImage(named: "prod_nav_right")
        self.view.addSubview(imageLeft)
        self.view.addSubview(imageRight)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            }
            else if view is UIPageControl {
                view.backgroundColor = UIColor.clear
            }
        }
    }
    

    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let pageContent: ProductViewController = viewController as! ProductViewController
        var index = pageContent.pageIndex
        
        if (index == NSNotFound) {
            return nil
        }
        
        if(index == 0) {
            return getViewControllerAtIndex(index: self.products.count-1)
        }
        
        index = index - 1;
        return getViewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let pageContent: ProductViewController = viewController as! ProductViewController
        var index = pageContent.pageIndex
        if (index == NSNotFound)
        {
            return nil;
        }
        index = index + 1;
        if (index == self.products.count)
        {
            return getViewControllerAtIndex(index: 0);
        }
        return getViewControllerAtIndex(index: index)
    }
    
    func getViewControllerAtIndex(index: NSInteger) -> ProductViewController
    {
        // Create a new view controller and pass suitable data.
        let productViewController = self.storyboard?.instantiateViewController(withIdentifier: "product1") as! ProductViewController
        
        productViewController.pageIndex = index
        productViewController.productImage = products[index]
        
        return productViewController
    }
}
