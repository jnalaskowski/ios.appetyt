//
//  RecipiesCollectionViewCell.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 26/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class RecipiesCollectionViewCell: UICollectionViewCell {
    var recipeID:Int = 0
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var favoriteBtnOutlet: UIButton!
    @IBOutlet weak var titleViewConstraintHeight: NSLayoutConstraint!
    @IBOutlet weak var favoriteViewConstraintWidth: NSLayoutConstraint!
    
}
