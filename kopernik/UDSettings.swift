//
//  UDSettings.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 12/10/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import Foundation

class UDSettings {
    
    var userDefaults = UserDefaults.standard
    
    init() {
        
    }
    
    func getLang() -> String{
        return userDefaults.string(forKey: "appetyt.lang") ?? "PL"
    }
    
    func getUnit() -> String{
        return userDefaults.string(forKey: "appetyt.unit") ?? "metric"
    }
    
    func setLang(lang:String) {
        userDefaults.set(lang, forKey: "appetyt.lang")
        if(lang == "PL") {
            UNIT = "metric"
            self.setUnit(unit: UNIT)
        }
        userDefaults.synchronize()
    }
    
    func setUnit(unit:String) {
        userDefaults.set(unit, forKey: "appetyt.unit")
        userDefaults.synchronize()
    }
}
