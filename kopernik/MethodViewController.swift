//
//  MethodViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 26/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

class MethodViewController: UIViewController {


    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var mainTextView: UITextView!
    @IBAction func closeBtnAction(_ sender: UIButton) {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    @IBOutlet weak var closeBtnConstraintWidth: NSLayoutConstraint!
    @IBOutlet weak var methodTitle: UILabel!
    @IBOutlet weak var methodView: UIView!
    
    var recipeID:Int!
    var recipe:Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        methodView.layer.cornerRadius = 20.0
        mainTextView.isScrollEnabled = false
        self.fillText()
        closeBtnConstraintWidth.constant = BUTTON_WIDTH * 0.8
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mainTextView.isScrollEnabled = true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func choseFontSize(size:CGFloat) -> CGFloat {
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        switch (deviceIdiom) {
        case .pad:
            return (size * 1.4)
        default:
            return size
        }
    }
    
    func fillText() {
        
        var author:String = self.recipe.recipeAuthor
        if(self.recipe.recipeBlog != "") {
            author = author + "\n" + self.recipe.recipeBlog
        }
        authorLabel.font = methodTitle.font.withSize(self.choseFontSize(size: 17.0))
        authorLabel.text = author
        
        let textAttachment = NSTextAttachment()
        let attributedString :NSMutableAttributedString = NSMutableAttributedString(string: "")
        let attributesH1 : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "HVD Comic Serif Pro", size: self.choseFontSize(size: 34.0)) ?? UIFont.systemFont(ofSize: self.choseFontSize(size: 34.0)),
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor(hue: 0.0167, saturation: 0.24, brightness: 0.63, alpha: 1.0)
        ]
        let attributesH2 : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "Rubik-Medium", size: self.choseFontSize(size: 18.0)) ?? UIFont.systemFont(ofSize: self.choseFontSize(size: 18.0)),
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor(hue: 0.0167, saturation: 0.24, brightness: 0.63, alpha: 1.0)
        ]
        let attributesH3 : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "HVD Comic Serif Pro", size: self.choseFontSize(size: 28.0)) ?? UIFont.systemFont(ofSize: self.choseFontSize(size: 28.0)),
            NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue) : UIColor(hue: 0.0167, saturation: 0.24, brightness: 0.63, alpha: 1.0)
        ]
        let attributesP : [NSAttributedString.Key : Any] = [
            NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue) : UIFont(name: "Rubik-Regular", size: self.choseFontSize(size: 15.0)) ?? UIFont.systemFont(ofSize: self.choseFontSize(size: 15.0))
        ]
        
        // tytuł
        methodTitle.text = self.recipe.recipeName
        methodTitle.font = methodTitle.font.withSize(self.choseFontSize(size: 23.0))

        // nagłówek składniki
        if(LANG == "PL") {
            attributedString.append(NSAttributedString(string: "SKŁADNIKI ", attributes: attributesH1))
        }
        else {
            attributedString.append(NSAttributedString(string: "INGREDIENTS ", attributes: attributesH1))
        }
        textAttachment.image = UIImage(named: "methodsIcon")?.scaleImageToFitSize(size: CGSize(width: self.choseFontSize(size: 28.0), height: self.choseFontSize(size: 28.0)))
        attributedString.append(NSAttributedString(attachment: textAttachment))
        
        attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n")))
        
        // składniki
        for ingredient in self.recipe.recipeIngredients {
            
            var ingradientName: String = ingredient.ingredientNameImperial
            
            if(UNIT == "metric") {
                ingradientName = ingredient.ingredientNameMetric
            }
            
            if(ingredient.isTitle) {
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n")))
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string:  ingradientName.uppercased(), attributes: attributesH2)))
            }
            else {
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "• " + ingradientName, attributes: attributesP)))
            }
            attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n")))
        }
        
        // nagłówek przygotowanie
        attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n\n")))
        if(LANG == "PL") {
            attributedString.append(NSAttributedString(string: "PRZYGOTOWANIE ", attributes: attributesH3))
        }
        else {
            attributedString.append(NSAttributedString(string: "PREPARATION ", attributes: attributesH3))
        }
        textAttachment.image = UIImage(named: "methodsIcon")?.scaleImageToFitSize(size: CGSize(width: self.choseFontSize(size: 28.0), height: self.choseFontSize(size: 28.0)))
        //attributedString.append(NSAttributedString(attachment: textAttachment))
        
        attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n")))
        
        // kroki
        for method in self.recipe.recipeMethods {
            
            var methodName: String = method.imperialStep
            
            if(UNIT == "metric") {
               methodName = method.methodStep
            }
            
            if(method.isTitle) {
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n")))
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string:  methodName.uppercased(), attributes: attributesH2)))
            }
            else {
                attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: methodName, attributes: attributesP)))
            }
            attributedString.append(NSMutableAttributedString(attributedString:NSMutableAttributedString(string: "\n\n")))
        }
        
        mainTextView.attributedText = attributedString
    }

}

