//
//  SettingsViewController.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 14/09/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit
import StoreKit

class SettingsViewController: UIViewController {
    
    // outlety
    @IBOutlet weak var englishBtnOutlet: UIButton!
    @IBOutlet weak var polishBtnOutlet: UIButton!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var imperialBtnOutlet: UIButton!
    @IBOutlet weak var metricBtnOutlet: UIButton!
    @IBOutlet weak var unitsLabelView: UIView!
    @IBOutlet weak var unitsButtonView: UIView!
    @IBOutlet weak var navigationImage: UIImageView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var rateusButtonOutlet: UIButton!
    @IBOutlet weak var termsButtonOutlet: UIButton!
    @IBOutlet weak var separator1Constraint: NSLayoutConstraint!
    @IBOutlet weak var versionLabel: UILabel!
    
    let settings = UDSettings()
    
    
    // akcje
    @IBAction func polishBtnAction(_ sender: UIButton) {
        LANG = "PL"
        settings.setLang(lang: "PL")
        self.drawView()
    }
    @IBAction func englishBtnAction(_ sender: UIButton) {
        LANG = "EN"
        settings.setLang(lang: "EN")
        self.drawView()
    }
    @IBAction func imperialBtnAction(_ sender: UIButton) {
        UNIT = "imperial"
        settings.setUnit(unit: "imperial")
        self.drawView()
    }
    @IBAction func metricBtnAction(_ sender: UIButton) {
        UNIT = "metric"
        settings.setUnit(unit: "metric")
        self.drawView()
    }
    @IBAction func rateusBtnAction(_ sender: UIButton) {
        SKStoreReviewController.requestReview()
    }
    @IBAction func termsBtnAction(_ sender: UIButton) {
        if let url = URL(string: "http://lookingoodapps.com/termsofservice") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    
    override func viewDidLoad() {
        
        self.drawView()
        super.viewDidLoad()
        self.addHomeButton()
    }
    
    func drawView() {
        if(LANG == "PL") {
            languageLabel.text = "JĘZYK"
            polishBtnOutlet.setImage(UIImage(named: "settBtnPolishPLActive"), for: .normal)
            englishBtnOutlet.setImage(UIImage(named: "settBtnEnglishEN"), for: .normal)
            
            unitsLabelView.alpha = 0
            unitsButtonView.alpha = 0
            separator1Constraint.constant = -(unitsLabelView.frame.height + unitsButtonView.frame.height)
            
            navigationImage.image = UIImage(named: "settNavPL")
            rateusButtonOutlet.setImage(UIImage(named: "settBtnRatePL"), for: .normal)
            termsButtonOutlet.setImage(UIImage(named: "settBtnTermsPL"), for: .normal)
        }
        else {
            languageLabel.text = "LANGUAGE"
            polishBtnOutlet.setImage(UIImage(named: "settBtnPolishPL"), for: .normal)
            englishBtnOutlet.setImage(UIImage(named: "settBtnEnglishENActive"), for: .normal)
            
            unitsLabelView.alpha = 1
            unitsButtonView.alpha = 1
            separator1Constraint.constant = 16
            
            if(UNIT == "metric") {
                imperialBtnOutlet.setImage(UIImage(named: "settBtnImperial"), for: .normal)
                metricBtnOutlet.setImage(UIImage(named: "settBtnMetricActive"), for: .normal)
            }
            else {
                imperialBtnOutlet.setImage(UIImage(named: "settBtnImperialActive"), for: .normal)
                metricBtnOutlet.setImage(UIImage(named: "settBtnMetric"), for: .normal)
            }
            
            navigationImage.image = UIImage(named: "settNavEN")
            rateusButtonOutlet.setImage(UIImage(named: "settBtnRateEN"), for: .normal)
            termsButtonOutlet.setImage(UIImage(named: "settBtnTermsEN"), for: .normal)
        }
        versionLabel.text = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func addHomeButton() {
        let button = UIButton(type: .system)
        
        let height = floor(BUTTON_WIDTH * (89/109))
        button.frame = CGRect(x: BUTTON_WIDTH/2, y: BUTTON_WIDTH/2, width: BUTTON_WIDTH, height: height)
        button.setBackgroundImage(UIImage(named: "returnBtn"), for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: nil)
        
        RECIPES = loadData(lang: LANG)
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func englishButtonAction(sender: UIButton!) {

    }

}
