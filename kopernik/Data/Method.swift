//
//  Method.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

class Method {
    
    var methodStep:String
    var imperialStep:String
    var isTitle:Bool
    
    init(imperialStep:String, methodStep:String, isTitle:Bool) {
        self.methodStep = methodStep
        self.isTitle = isTitle
        self.imperialStep = imperialStep
    }
}
