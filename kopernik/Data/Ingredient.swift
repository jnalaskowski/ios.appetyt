//
//  Ingredient.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

class Ingredient {
    
    var ingredientNameImperial:String
    var ingredientNameMetric:String
    var isTitle:Bool
    
    init(ingredientNameImperial:String, ingredientNameMetric:String, isTitle:Bool) {
        self.ingredientNameImperial = ingredientNameImperial
        self.ingredientNameMetric = ingredientNameMetric
        self.isTitle = isTitle
    }
}
