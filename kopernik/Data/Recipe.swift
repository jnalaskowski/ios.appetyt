//
//  Recipe.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//
import UIKit

class Recipe {
    
    var recipeID: Int
    var recipeName: String
    var recipeThumb: UIImage
    var recipeImage: String
    var recipeIngredients:[Ingredient]
    var recipeMethods:[Method]
    var recipeGuest: Bool
    var recipeAuthor: String
    var recipeBlog: String
    
    init(recipeID:Int,recipeName:String,recipeAuthor:String,recipeBlog:String,recipeThumb:String,recipeImage:String,recipeIngredients:[Ingredient],recipeMethods:[Method],recipeGuest:Bool = false) {
        self.recipeID = recipeID
        self.recipeName = recipeName
        self.recipeThumb = UIImage(named:recipeThumb)!
        self.recipeImage = recipeImage
        self.recipeIngredients = recipeIngredients
        self.recipeMethods = recipeMethods
        self.recipeGuest = recipeGuest
        self.recipeAuthor = recipeAuthor
        self.recipeBlog = recipeBlog
    }
}
