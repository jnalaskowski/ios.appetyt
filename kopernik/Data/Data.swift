//
//  Data.swift
//  kopernik
//
//  Created by Jakub Nalaskowski on 24/07/2018.
//  Copyright © 2018 Lookin'good Apps. All rights reserved.
//

import UIKit

var RECIPES:[Recipe] = []
var LANG:String = "EN"
var UNIT:String = "metric"
var BUTTON_WIDTH:CGFloat = 0.0


func loadData(lang:String) -> [Recipe] {
    var recipes:[Recipe] = []
    var ingrediens:[Ingredient] = []
    var methods:[Method] = []
    var recipe:Recipe
    
    // PL
    if(lang == "PL") {
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "Tartę można przygotować na dwa sposoby – w wersji klasycznej lub wegańskiej /w nawiasach opcjonalne składniki/", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Piernikowo-orzechowy spód", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g Katarzynek® VEGAN lub bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "70 g zmielonych orzechów włoskich"))
        ingrediens.append(createIngredient(imperial: "", metric: "70 g herbatników /klasycznych lub wegańskich/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g roztopionego masła /lub oleju kokosowego/"))
        ingrediens.append(createIngredient(imperial: "", metric: "Róże z jabłek", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "5-6 jabłek /najlepiej mocno czerwonych/"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok. 900 ml wody"))
        ingrediens.append(createIngredient(imperial: "", metric: "Waniliowy budyń z kaszy jaglanej", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "750 ml mleka /krowiego lub roślinnego/"))
        ingrediens.append(createIngredient(imperial: "", metric: "130 g kaszy jaglanej"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/3 szklanki cukru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 laska wanilii"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Katarzynki® miksujemy razem z herbatnikami, orzechami włoskimi i cynamonem. Dodajemy roztopione masło (lub olej kokosowy), mieszamy, a następnie wykłady masą spód i boki formy na tartę. Podpiekamy w piekarniku, nagrzanym do 180°C, przez około 15 minut. Odstawiamy."))
        methods.append(createMethodStep(step: "900 ml wody zagotowujemy razem z 1 szklanką cukru i sokiem, wyciśniętym z dwóch cytryn. Jabłka przecinamy na pół, usuwamy gniazda nasienne i kroimy na cienkie plasterki (nie obieramy ze skóry). Wrzucamy jabłka do wody i gotujemy przez około 2 minuty (do czasu aż plasterki staną się elastyczne). Odcedzamy i studzimy. Kaszę jaglaną wsypujemy do garnuszka, dodajemy cukier oraz ziarenka wanilii."))
        methods.append(createMethodStep(step: "Zalewamy mlekiem i wstawiamy na gaz – gotujemy przez około 15 minut (mieszamy co jakiś czas). Następnie masę miksujemy blenderem, uzyskując konsystencję budyniu. Masę budyniową przekładamy na przygotowany wcześniej spód, a następnie od razu układamy plasterki jabłek, zwinięte na kształt różyczek (najszybciej kilka plasterków jabłek ułożyć wzdłuż obok siebie, by na siebie delikatnie nachodziły i zwijać). Tak powstałe „jabłkowe różyczki” można posmarować opcjonalnie np. miodem lub syropem klonowym, by nabrały blasku. Tartę chłodzimy w lodówce, a następnie podajemy."))
        // ---- recipe
        recipe = Recipe(recipeID: 0, recipeName: "Tarta z waniliowym budyniem i różami z jabłek na piernikowo-orzechowym spodzie", recipeAuthor: "Paulina Reczkowska", recipeBlog: "www.czekolada-utkane.pl", recipeThumb: "1t", recipeImage: "1", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 6 porcji/", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Bułki z piernikową kruszonką", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "500 g mąki pszennej typu 550"))
        ingrediens.append(createIngredient(imperial: "", metric: "90 g miękkiego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "30 g świeżych drożdży"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 ml ciepłego, tłustego mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 jajka /rozmiar L/"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "Burgery", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "6 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok.750 g mielonej wołowiny"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki musztardy francuskiej"))
        ingrediens.append(createIngredient(imperial: "", metric: "pieprz i sól"))
        ingrediens.append(createIngredient(imperial: "", metric: "Konfitura z szalotek na ciemnym piwie", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "6-8 szt. szalotek"))
        ingrediens.append(createIngredient(imperial: "", metric: "350 ml ciemnego piwa"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "łyżka miodu gryczanego"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 liście laurowe"))
        ingrediens.append(createIngredient(imperial: "", metric: "5-6 kulek ziela angielskiego"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz"))
        ingrediens.append(createIngredient(imperial: "", metric: "Majonez korzenny", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 duże lub 2 małe żółtka"))
        ingrediens.append(createIngredient(imperial: "", metric: "łyżka octu /najlepiej ryżowego/"))
        ingrediens.append(createIngredient(imperial: "", metric: "łyżka musztardy"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki oleju rzepakowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 szklanki oliwy z oliwek"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka drobnego cukru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "mielony cynamon"))
        ingrediens.append(createIngredient(imperial: "", metric: "utarta gałka muszkatołowa"))
        ingrediens.append(createIngredient(imperial: "", metric: "chili w płatkach"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz"))
        ingrediens.append(createIngredient(imperial: "", metric: "Dodatkowo"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 dojrzałe figi"))
        ingrediens.append(createIngredient(imperial: "", metric: "sałata dębowa"))
        ingrediens.append(createIngredient(imperial: "", metric: "bazylia fioletowa"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Bułki z piernikową kruszonką", title: true))
        methods.append(createMethodStep(step: "Drożdże zasypujemy cukrem i czekamy, aż się rozpuszczą. Mieszamy z łyżką mąki i 3 łyżkami ciepłego mleka. Odstawiamy zaczyn do wyrośnięcia na ok. 15 minut. Mąkę przesiewamy i mieszamy z pozostałymi składnikami (oprócz jednego jajka). Wyrabiamy gładkie ciasto. Powinno być dość rzadkie. Odstawiamy do wyrośnięcia pod ściereczką na ok. 40 minut w ciepłe miejsce."))
        methods.append(createMethodStep(step: "Wyrośnięte ciasto dzielimy na 6 równych porcji. Dłońmi nasmarowanymi oliwą formujemy bułki, układamy w dużych odstępach na blasze wyłożonej papierem. Odstawiamy do wyrośnięcia na ok. 30 minut. Wyrośnięte bułki formujemy raz jeszcze, tak aby zmniejszyły objętość. Odstawiamy na kolejne 30 minut. Bułki smarujemy roztrzepanym jajkiem, posypujemy pokruszonymi piernikami. Piekarnik nagrzewamy do 200°C. Na najniższym poziomie stawiamy blachę z wodą, by wytwarzała się para. Nad nią stawiamy blachę z bułkami i pieczemy ok. 35-40 minut."))
        methods.append(createMethodStep(step: "Burgery", title: true))
        methods.append(createMethodStep(step: "Wołowinę wyrabiamy dłońmi z musztardą, solą i pieprzem, formujemy równe burgery, rozpłaszczamy. Pierniki rozkruszamy w malakserze, aż uzyskamy strukturę grubego piasku z grudkami, panierujemy w nich burgery. Grillujemy przez ok. 2 minuty, odstawiamy na 3-4 minuty, by odpoczęły."))
        methods.append(createMethodStep(step: "Konfitura z szalotek na ciemnym piwie", title: true))
        methods.append(createMethodStep(step: "Szalotki kroimy w piórka, wrzucamy na rozgrzane masło wraz z liśćmi laurowymi i zielem angielskim. Podsmażamy chwilę, aż cebula lekko się zeszkli. Zalewamy piwem, dodajemy miód, sól i pieprz. Konfiturę smażymy na małym ogniu, aż niemal cały płyn odparuje i stanie się lepka."))
        methods.append(createMethodStep(step: "Majonez korzenny", title: true))
        methods.append(createMethodStep(step: "Jajka sparzamy. Oddzielamy białka od żółtek. Żółtka dokładnie ucieramy z octem, musztardą, cukrem, solą i pieprzem. Wąskim strumieniem dolewamy olej rzepakowy i ubijamy rózgą lub mikserem (nie używamy blendera). Gdy majonez zacznie gęstnieć, stopniowo dolewamy oliwę z oliwek i ubijamy krótko do uzyskania pożądanej konsystencji. Doprawiamy cynamonem, gałką i chilli."))
        methods.append(createMethodStep(step: "Na koniec", title: true))
        methods.append(createMethodStep(step: "Ostudzone bułki kroimy na pół, grillujemy od wewnątrz. Smarujemy majonezem. Układamy sałatę, burgery, konfiturę, pokrojone w plastry figi, bazylię"))
        // ---- recipe
        recipe = Recipe(recipeID: 1, recipeName: "Burgery piernikowe", recipeAuthor: "Jagoda Paździor-Saba", recipeBlog: "www.zucchini-blues.blogspot.com", recipeThumb: "2t", recipeImage: "2", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "200 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "220 g płynnego miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 ml mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 jajka"))
        ingrediens.append(createIngredient(imperial: "", metric: "340 g mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g drobnego cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "80 g Serduszek Toruńskich lukrowanych"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżeczki sody"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "Krem", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "2 kostki masła /w temperaturze pokojowej/"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "40 g ciemnego kakao"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g mlecznej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "Dodatkowo", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "80 g Serduszek Toruńskich lukrowanych"))
        ingrediens.append(createIngredient(imperial: "", metric: "30 g gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "30 g białej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "opcjonalnie świeże kwiaty"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Pierniczki przekładamy do malaksera i miksujemy na drobną mąkę. Mąkę przesiewamy ze zmiksowanymi pierniczkami, cukrem, sodą i solą. Masło przekładamy do rondelka wraz z miodem. Podgrzewamy, aż się roztopią, po czym dodajemy mleko i dokładnie mieszamy. Odstawiamy do przestudzenia, a następnie wraz z jajkami przelewamy do mąki. Mieszamy do połączenia składników, dzielimy na dwie równe części i przelewamy do dwóch foremek o średnicy 20 cm. Ciasto pieczemy w 180°C przez ok. 45 minut, studzimy na kratce, a wystudzone przekrawamy na pół, aby uzyskać 4 krążki."))
        methods.append(createMethodStep(step: "Przygotowujemy krem: masło ucieramy z cukrem pudrem i kakao. Obie czekolady rozpuszczamy w kąpieli wodnej. Przestudzone dodajemy do masła, cały czas miksując na gładki krem. 1/4 kremu zostawiamy do posmarowania ciasta z wierzchu, resztą przekładamy krążki. Czekolady na ozdoby rozpuszczamy w kąpieli wodnej. Rozsmarowujemy je na czystej folii bąbelkowej i przekładamy do lodówki. Po kilku minutach, gdy czekolada zastygnie, łamiemy ją na mniejsze kawałki. Tort ozdabiamy połamaną czekoladą, Serduszkami Toruńskimi lukrowanymi i świeżymi kwiatami. Przed podaniem schładzamy co najmniej 2 godziny."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 2, recipeName: "Ciasto miodowo-piernikowe", recipeAuthor: "Diana Kowalczyk", recipeBlog: "www.mientablog.com", recipeThumb: "3t", recipeImage: "3", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/blaszka o wymiarach 20 x 30 cm/"))
        ingrediens.append(createIngredient(imperial: "", metric: "120 g Katarzynek® w czekoladzie"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g mąki pszennej tortowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 duże jajko"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 łyżeczki soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka proszku do pieczenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok. 800 g śliwek węgierek"))
        ingrediens.append(createIngredient(imperial: "", metric: "cukier puder do oprószenia"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Pierniczki mielimy na proszek w robocie kuchennym z końcówką z nożykiem. Do Katarzynek® dodajemy wszystkie pozostałe składniki i mieszamy do czasu, aż ciasto zacznie przypominać konsystencją kruszonkę. Wyjmujemy z miski robota, krótko zagniatamy i wstawiamy do lodówki na około 1 godzinę. Im bardziej je schłodzimy, tym lepszą konsystencję będzie miało ciasto po upieczeniu."))
        methods.append(createMethodStep(step: "Ciasto można przygotować także bez użycia robota kuchennego: mąkę siekamy z zimnym masłem, dodajemy pozostałe składniki i rozcieramy palcami, aż otrzymamy kruszonkę. Uwaga: ten typ ciasta nie lubi ciepła i nie może być za długo wyrabiany, bo ciasto będzie zbyt twarde. Śliwki myjemy, przecinamy na pół, usuwamy pestki. Blaszkę wykładamy papierem do pieczenia."))
        methods.append(createMethodStep(step: "2/3 ciasta rozkruszamy na dnie blaszki, delikatnie dociskamy dłonią, układamy połówki śliwek i pokrywamy resztą ciasta, krusząc je na śliwki. Blaszkę z ciastem wstawiamy do piekarnika nagrzanego do 170°C i pieczemy około 45 minut, aż się zrumieni. Ciasto wyjmujemy z piekarnika, studzimy w blaszce i kroimy na porcje. Przed podaniem oprószamy cukrem pudrem."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 3, recipeName: "Ciasto kruszonkowo-pierniczkowe ze śliwkami", recipeAuthor: "Agnieszka Czapska-Pruszak", recipeBlog: "www.zpamietnikapiekarnika.pl", recipeThumb: "4t", recipeImage: "4", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka wody"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 łyżeczki soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 duże jajka"))
        ingrediens.append(createIngredient(imperial: "", metric: "Krem", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "400 ml śmietanki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "120 g Katarzynek® w czekoladzie"))
        ingrediens.append(createIngredient(imperial: "", metric: "Powidła", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "0,5 kg śliwek węgierek"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Mleko mieszamy z wodą, dodajemy sól oraz pokrojone na kawałki masło – zagotowujemy. W momencie, kiedy całość zacznie wrzeć, wsypujemy mąkę i energicznie mieszamy, aż masa zgęstnieje. Mieszamy dodatkowo jeszcze przez chwilę, aby nieco wysuszyć ciasto i odstawiamy do wystudzenia. Do wystudzonej masy dodajemy pojedynczo jajka i po każdym miksujemy. Powinniśmy uzyskać gładkie i błyszczące ciasto. Przekładamy je do rękawa cukierniczego z odpowiednią końcówką i na blaszce wyłożonej papierem do pieczenia wyciskamy paski o długości około 5-6 cm."))
        methods.append(createMethodStep(step: "Blaszkę z ciastkami wstawiamy do piekarnika nagrzanego do 200°C i pieczemy ok. 15 minut. Eklerki powinny się zarumienić i wysuszyć z zewnątrz i być miękkie w środku. Katarzynki® kroimy na kawałki. Śmietankę wlewamy do garnka, dodajemy pokrojone Katarzynki®. Całość zagotowujemy i odstawiamy. Co jakiś czas mieszamy łyżką starając się rozgnieść pierniki. Katarzynki® nasiąkając śmietanką, stają się miękkie. W momencie, kiedy całość przestygnie, miksujemy blenderem i wstawiamy do lodówki – do całkowitego schłodzenia. Schłodzony krem katarzynkowy będzie tak gęsty, że można nim napełniać eklery przy użyciu szprycy cukierniczej."))
        methods.append(createMethodStep(step: "Śliwki myjemy, kroimy na połówki i usuwamy pestki. Wkładamy do garnka z grubym dnem i gotujemy do momentu, aż uzyskamy gęste powidła. Kiedy wszystkie składniki są gotowe, zaczynamy składać eklery. Ciastka kroimy wzdłuż w połowie ich wysokości. Dolną część smarujemy powidłami, nakładamy krem katarzynkowy, przykrywamy drugą połówką i posypujemy cukrem pudrem. Eklery najlepiej smakują pierwszego dnia, po lekkim schłodzeniu, kiedy ciastko jest jeszcze chrupiące. "))
        
        // ---- recipe
        recipe = Recipe(recipeID: 4, recipeName: "Eklery z powidłami i kremem katarzynkowym", recipeAuthor: "Agnieszka Czapska-Pruszak", recipeBlog: "www.zpamietnikapiekarnika.pl", recipeThumb: "5t", recipeImage: "5", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/tortownica o średnicy 18 cm/"))
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 i 3/4 szklanki mąki"))
        ingrediens.append(createIngredient(imperial: "", metric: "1,5 szklanki cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 jajka"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 szklanki kakao"))
        ingrediens.append(createIngredient(imperial: "", metric: "0,5 szklanki oleju"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka proszku do pieczenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "1,5 łyżeczki sody"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka maślanki"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka gorącej kawy "))
        ingrediens.append(createIngredient(imperial: "", metric: "0,5 łyżeczki soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka ekstraktu waniliowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "Krem", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "500 g twarogu śmietankowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 szklanki cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "80 g białej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 ml śmietanki kremówki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 dowolne pierniczki z Fabryki Cukierniczej KOPERNIK"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Mąkę przesiewamy z kakao, mieszamy z cukrem, solą, cynamonem, proszkiem do pieczenia i sodą. Maślankę mieszamy z jajkami, wanilią i olejem. Łączymy składniki suche i mokre – miksujemy na niskich obrotach miksera. Podczas miksowania dodajemy kawę i mieszamy jeszcze przez krótką chwilę. Ciasto przelewamy do tortownicy wyłożonej papierem do pieczenia. Rozgrzewamy piekarnik do 180°C. Ciasto pieczemy ok. 60 minut, sprawdzając patyczkiem czy jest już gotowe. Po wyłączeniu piekarnika uchylamy drzwiczki i pozwalamy ciastu przestygnąć przez kilkanaście minut. Ciasto studzimy do pełnego ochłodzenia – najlepiej zostawić je na całą noc."))
        methods.append(createMethodStep(step: "Wystudzone ciasto kroimy na 3 blaty. Przygotowujemy krem: czekoladę roztapiamy w kąpieli wodnej, cały czas mieszając, żeby się nie przypaliła. Następnie zostawiamy do ostudzenia na kilka minut. Twarożek ucieramy z cukrem pudrem i cynamonem. Do masy twarogowej stopniowo dodajemy czekoladę i dalej ucieramy. Dolewamy śmietankę i miksujemy do powstania puszystego kremu, z którego 4 łyżki odkładamy do dekoracji."))
        methods.append(createMethodStep(step: "Kremem przekładamy blaty ciasta i każdą jego warstwę posypujemy pokruszonymi pierniczkami. Krem rozsmarowujemy na bokach oraz na wierzchu ciasta. Tort wstawiamy do lodówki na minimum 45 minut.  Przygotowujemy polewę: śmietankę podgrzewamy, aż będzie gorąca nie dopuszczając do zagotowania. Czekoladę łamiemy na kosteczki, dodajemy do podgrzanej śmietanki i mieszamy do czasu aż czekolada się rozpuści, uważając przy tym aby nie powstały grudki. Zostawiamy do ostudzenia na ok. 1 minutę. Tort wyjmujemy z lodówki i oblewamy obficie lekko ciepłą czekoladą. Dekorujemy tort wedle uznania używając pierników, pozostałych kostek czekolady, kawałków posypki karmelowej, po czym ponownie wkładamy go do lodówki na min. 20 minut."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 5, recipeName: "Tort piernikowy z masą serową", recipeAuthor: "Agnieszka Trawińska", recipeBlog: "www.instagram.com/trawkagotuje", recipeThumb: "6t", recipeImage: "6", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "200 ml słodkiej śmietanki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g serka mascarpone"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki świeżo otartej gałki muszkatołowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki zmielonego imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki ekstraktu waniliowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g malin"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cukru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 szt. Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Pierniczków Nadziewanych z Fabryki Cukierniczej KOPERNIK"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Śmietankę ubijamy na sztywno za pomocą miksera na wysokich obrotach. W połowie miksowania dodajemy cukier puder. Wstawiamy do lodówki i schładzamy przez około 10 minut. W międzyczasie napowietrzamy mikserem serek mascarpone, dodajemy do niego wszystkie korzenne przyprawy oraz ekstrakt waniliowy i mieszamy. Delikatnie łączymy śmietankę i serek, wstawiamy do lodówki i schładzamy przez co najmniej 30 minut."))
        methods.append(createMethodStep(step: "Serca Toruńskie® kruszymy na kawałki różnej wielkości, dodajemy do masy bezpośrednio przed podaniem i delikatnie z nią łączymy. Maliny podgrzewamy z łyżeczką cukru w rondelku z grubym dnem. Gdy pojawi się sok, delikatnie rozgniatamy je widelcem i pozostawiamy na wolnym ogniu, co jakiś czas mieszając. Krem podajemy w szklankach, na dnie układając Pierniczki Nadziewane. Polewamy przed podaniem ciepłym sosem malinowym i dekorujemy zmrożonymi owocami."))
        // ---- recipe
        recipe = Recipe(recipeID: 6, recipeName: "Korzenny krem z ciepłym sosem z malin", recipeAuthor: "Aleksandra Kocerba", recipeBlog: "", recipeThumb: "7t", recipeImage: "7", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na ok. 8-10 porcji/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g płatków owsianych"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g orzechów laskowych, obranych"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g daktyli"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki kakao"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 pomarańcza"))
        ingrediens.append(createIngredient(imperial: "", metric: "5-6 szt. Serduszek Toruńskich lukrowanych"))
        ingrediens.append(createIngredient(imperial: "", metric: "woda kokosowa"))
        ingrediens.append(createIngredient(imperial: "", metric: "jogurt naturalny"))
        ingrediens.append(createIngredient(imperial: "", metric: "owoce"))
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Daktyle zalewamy wrzątkiem na ok. pół godziny. Po tym czasie je odsączamy. Orzechy prażymy na suchej patelni, aż lekko się zezłocą i staną się chrupiące. 200 g płatków umieszczamy w pojemniku blendera. Dodajemy odsączone daktyle, orzechy, kakao, pierniczki i skórkę startą z pomarańczy. Wszystkie składniki mielimy, aż osiągną konsystencję grubszego piasku. Dodajemy resztę płatków owsianych i mieszamy wszystko łyżką."))
        methods.append(createMethodStep(step: "Przygotowujemy owsiankę: umieszczamy porcję przygotowanej mieszanki (60-70 g) w niewielkim garnku i zalewamy wodą kokosową lub zwykłą wodą i ulubionym mlekiem. Owsiankę gotujemy kilka minut, aż do osiągnięcia pożądanej konsystencji. Podajemy z jogurtem i owocami. Resztę mieszanki przechowujemy w szczelnie zamkniętym słoiku."))
        // ---- recipe
        recipe = Recipe(recipeID: 7, recipeName: "Czekoladowo-piernikowa owsianka z orzechami i pomarańczą", recipeAuthor: "Aleksandra Obołończyk", recipeBlog: "www.olalacooking.com ", recipeThumb: "8t", recipeImage: "8", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 2 porcje/", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "2 filety z piersi kaczki ze skórą o wadze ok. 250 g"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 nieduża kapusta pak choy, pokrojona w większą kostkę"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 nieduża drobno pokrojona cebula"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 drobno posiekane ząbki czosnku "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka świeżo startego imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "5-6 śliwek węgierek pokrojonych w ćwiartki"))
        ingrediens.append(createIngredient(imperial: "", metric: "świeża kolendra"))
        ingrediens.append(createIngredient(imperial: "", metric: "Sos", title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki sosu hoisin"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki jasnego sosu sojowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "5 łyżek zmielonych drobno Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka soku z cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "5-6 łyżek wody"))
        ingrediens.append(createIngredient(imperial: "", metric: "1,5 łyżeczki świeżo starego imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki domowej konfitury śliwkowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "0,5 łyżeczki drobno posiekanego chili"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Rozpoczynamy od przygotowania sosu. Katarzynki® wrzucamy do kielicha blendera i mielimy na proszek. W rondelku umieszczamy składniki sosu i chwilę podgrzewamy na niewielkim ogniu, aż składniki się połączą, a sos lekko zgęstnieje. Jeśli jest za gęsty, dodajemy więcej wody. Sos odstawiamy. Kaczkę doprawiamy solą i pieprzem. Skórę nacinamy ostrym nożem co ok. 0,5 cm uważając, aby nie przeciąć mięsa."))
        methods.append(createMethodStep(step: "Kaczkę układamy skórą do dołu w woku i podgrzewamy na niewielkim ogniu, aż tłuszcz ze skóry się wytopi i stanie się ona brązowa i chrupiąca. Przewracamy filety na drugą stronę i smażymy jeszcze przez 3-4 minuty. Przekładamy kaczkę na talerz, a z woka odlewamy nadmiar tłuszczu, pozostawiając na dnie ok. 1 łyżkę. Na rozgrzany tłuszcz wrzucamy posiekaną cebulę, czosnek, imbir i chwilę smażymy. Dodajemy pokrojony pak choy i chwilę razem przesmażamy."))
        methods.append(createMethodStep(step: "Warzywa zalewamy sosem i mieszamy wszystko. Dodajemy świeże śliwki węgierki. Kaczkę kroimy ostrym nożem na plastry i na chwilę układamy na sosie, aby się zagrzała i doszła w środku, jeśli jest zbyt różowa. Danie dekorujemy świeżą kolendrą i chili. Podajemy z ugotowanym ryżem jaśminowym."))
        // ---- recipe
        recipe = Recipe(recipeID: 8, recipeName: "Kaczka w piernikowym sosie hoisin ze śliwkami", recipeAuthor: "Aleksandra Obołończyk", recipeBlog: "www.olalacooking.com ", recipeThumb: "9t", recipeImage: "9", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "3 jajka"))
        ingrediens.append(createIngredient(imperial: "", metric: "40 g rozpuszczonego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "80 g mąki"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 szt. Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka jagód"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżeczki cukru waniliowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki jagód"))
        ingrediens.append(createIngredient(imperial: "", metric: "garść posiekanych orzechów włoskich"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Masło roztapiamy i studzimy. Kruszymy jedno Serce Toruńskie®. Oddzielamy żółtka od białek. Białka ubijamy na sztywno. Żółtka z cukrem ubijamy na puszysta gładką masę. Przestudzone masło, cukier waniliowy, pokruszone Serca Toruńskie® oraz mąkę dodajemy do żółtek. Mieszamy łyżką to połączenia się składników. Białka dodajemy do masy i delikatnie wymieszamy. Patelnię smarujemy masłem. Ciasto wylewamy na patelnię, dodajemy dwa Serca Toruńskie® oraz posypujemy wierzch jagodami. Patelnię przykrywamy szczelną patelnią i smażymy ok. 10 minut na małym ogniu. Ciasto posypujemy przed podaniem jagodami oraz orzechami włoskimi."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 9, recipeName: "Smażone ciasto biszkoptowe od Serca", recipeAuthor: "Aneta Gwiner", recipeBlog: "www.kuchniawoparach.pl", recipeThumb: "10t", recipeImage: "10", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "270 g mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g zimnego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 jajko"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 kg śliwek"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 gram Pierniczków Nadziewanych w czekoladzie"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki mąki ziemniaczanej"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki cukru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 jajko do posmarowania ciasta"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Mąkę z cukrem pudrem przesiewamy na stolnicę, wbijamy jajko i dodajemy pokrojone w kostkę zimne masło. Wszystkie składniki ugniatamy. Ciasto rozpłaszczamy, zawijamy folią spożywczą i odkładamy na minimum pół godziny do lodówki. Piekarnik nastawiamy na 200°C bez termoobiegu. Pierniki kroimy na kawałki, czekoladę szatkujemy na kawałki. Odstawiamy."))
        methods.append(createMethodStep(step: "Śliwki myjemy, kroimy na ćwiartki, wyjmujemy pestki. Kawałki śliwek przekładamy do miski zasypujemy mąką ziemniaczaną i cukrem, mieszamy. Do miski z owocami dodajemy pokrojone pierniki i czekoladę, mieszamy. Odstawiamy. Schłodzone ciasto lekko oprószamy mąką i rozwałkowujemy na grubość ok. 5 mm, najlepiej od razu na papierze do pieczenia."))
        methods.append(createMethodStep(step: "Papier z ciastem przenosimy na dużą blachę. Środek ciasta oprószamy jeszcze odrobiną mąki ziemniaczanej. Wykładamy owoce wymieszane z piernikami i czekoladą pozostawiając ok. 7 cm brzeg ciasta. Brzegi ciasta zawijamy na nadzienie i smarujemy roztrzepanym jajkiem. Tartę wstawiamy na ok. 35 minut do rozgrzanego piekarnika. Galette, bo tak też nazywa się rustykalną tartę, podajemy posypany potartą czekoladą."))
        // ---- recipe
        recipe = Recipe(recipeID: 10, recipeName: "Rustykalna tarta śliwkowo-piernikowa", recipeAuthor: "Aniela Straszewska", recipeBlog: "www.fotografia.kawaiczekolada.pl", recipeThumb: "11t", recipeImage: "11", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "2 kacze piersi"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki kminu rzymskiego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki nasion kolendry"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki imbiru mielonego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szczypta gałki muszkatołowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szczypta anyżu gwiazdkowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 goździki zmłotkowane w moździerzu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szczypta chili w płatkach"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 białka ubite na sztywno"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka sosu sojowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 ząbek czosnku"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szt. Katarzynek® bez dodatku czekolady /starta na drobnych oczkach/"))
        ingrediens.append(createIngredient(imperial: "", metric: "Chutney",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "250 gram śliwek węgierek"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta cynamonu i anyżu gwiazdkowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki octu winnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Sos musztardowo-sezamowy",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki musztardy"))
        ingrediens.append(createIngredient(imperial: "", metric: "ocet jabłkowy"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżeczki tahiny z sezamu"))
        ingrediens.append(createIngredient(imperial: "", metric: "pieprz oraz szczypta anyżu gwiazdkowego"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Mięso mielimy wraz ze skórą. Doprawiamy solą, sosem sojowym, cynamonem, goździkiem, anyżem, chili. Dodajemy starte Katarzynki®, posiekany czosnek oraz ubite białka. Zwilżonymi rękami formujemy małe pulpeciki. Pulpeciki smażymy na gorącym oleju rzepakowym. Na koniec smażenia polewamy całość miodem i chwilę karmelizujemy. Odsączamy pulpeciki na papierowym ręczniku."))
        methods.append(createMethodStep(step: "Przygotowujemy chutney: śliwki obieramy z pestek, przekładamy do rondla, dodajemy przyprawy i dolewamy trochę wody. Dusimy pod przykryciem do miękkości. Pod koniec duszenia dodajemy ocet i odparowujemy. Miksujemy na gładko. Przygotowujemy sos musztardowo-sezamowy: składniki sosu łączymy ze sobą na jednolitą konsystencję."))
        // ---- recipe
        recipe = Recipe(recipeID: 11, recipeName: "Pulpeciki z piersi kaczki z piernikiem, chutneyem śliwkowym i dipem musztardowo-sezamowym", recipeAuthor: "Anita Zegadło", recipeBlog: "www.azgotuj.blogspot.com", recipeThumb: "12t", recipeImage: "12", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "4 żółtka"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/3 szklanki cukru pudru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "300 ml słodkiej śmietanki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "6 szt. Katarzynek® bez dodatku czekolady "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka świeżej dyni pokrojonej w kostkę"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki słodzonego mleka skondensowanego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki kandyzowanej skórki pomarańczowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki cynamonu"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Dynię przekładamy do rondelka, zalewamy mlekiem skondensowanym i gotujemy na małym ogniu przez około 20 minut. W tym czasie dynia zmięknie i zacznie się rozpadać. Studzimy dynię i blendujemy na puree. Do dużej miski wlewamy żółtka, dodajemy połowę cukru pudru. Miskę ustawiamy na garnku z gotującą się wodą i ubijamy. Żółtka powinny potroić objętość. Masa musi być lśniąca i gęsta. Gotową masę studzimy."))
        methods.append(createMethodStep(step: "Schłodzoną śmietankę ubijamy na sztywno z resztą cukru. Do ubitej śmietanki dodajemy masę z żółtek i delikatnie mieszamy. Do masy dodajemy puree dyniowe, skórkę pomarańczową, cynamon oraz pokruszone Katarzynki®. Masę przekładamy do pojemnika i wstawiamy do zamrażarki na co najmniej 6 godzin. Gotowe semifreddo podajemy z syropem klonowym i skórką pomarańczową."))
        // ---- recipe
        recipe = Recipe(recipeID: 12, recipeName: "Lodowe semifreddo z dynią, skórką pomarańczy i Katarzynkami®", recipeAuthor: "Anna Łukasiewicz", recipeBlog: "www.piatapopoludniu.blogspot.com", recipeThumb: "13t", recipeImage: "13", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 7 porcji/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "7 średnich jabłek"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g wędzonego boczku"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 średnia cebula"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 spory ząbek czosnku"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g świeżej lub mrożonej porzeczki"))
        ingrediens.append(createIngredient(imperial: "", metric: "6 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "pęczek świeżych ziół (rozmaryn, szałwia oraz majeranek)"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki octu balsamicznego"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Piekarnik nagrzewamy do 190°C. W tym czasie odkrawamy górę jabłek (część z ogonkiem) i odkładamy na bok. Środki jabłek drążymy tworząc miejsce na nadzienie, uważając aby nie przerwać skórki. Wydrążoną część jabłek odkładamy. Na suchej patelni podsmażamy boczek pokrojony w kostkę. Gdy tłuszcz się wytopi, dorzucamy posiekaną w piórka cebulę. Smażymy, aż cebula się zeszkli, po czym na patelnię dodajemy pokrojony czosnek, obraną porzeczkę, garść wydrążonych środków jabłek oraz posiekane zioła. Całość smażymy."))
        methods.append(createMethodStep(step: "Pierniki rozdrabniamy w malakserze na grubsze okruchy, po czym dodajemy je również na patelnię.  Nadzienie jeszcze chwilę smażymy, aż wszystkie składniki się połączą. Na koniec doprawiamy octem balsamicznym, solą i pieprzem. Wszystkie jabłka nadziewamy, układamy na blaszce wyłożonej papierem do pieczenia i wstawiamy do nagrzanego piekarnika. Zapiekamy około 25-30 minut, aż jabłka zmiękną i się zrumienią."))
        // ---- recipe
        recipe = Recipe(recipeID: 13, recipeName: "Jabłka zapiekane z piernikami, boczkiem, porzeczkami i ziołami", recipeAuthor: "Anna Witkiewicz", recipeBlog: "www.everydayflavours.blogspot.com", recipeThumb: "14t", recipeImage: "14", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "1 kg obranej i pokrojonej w kawałki dyni"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 cebula"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 marchewka"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka masła klarowanego"))
        ingrediens.append(createIngredient(imperial: "", metric: "10 ziaren kolendry"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta kminu rzymskiego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 listek laurowy"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szklanki bulionu warzywnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka masła klarowanego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 małego kalafiora"))
        ingrediens.append(createIngredient(imperial: "", metric: "pokruszone i podpieczone Katarzynki® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "świeże listki maggi"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz czarny"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "W garnku rozgrzewamy klarowane masło. Dodajemy przyprawy: kolendrę, kmin rzymski i listek laurowy, smażymy chwilę, aż poczujemy aromat przypraw. Dodajemy pokrojone na kawałki warzywa. Warzywa smażymy, mieszając łyżka, około 10 minut, po czym dodajemy pierniczki, wlewamy bulion i gotujemy do miękkości. Usuwamy listek laurowy, a całość miksujemy na gładki krem."))
        methods.append(createMethodStep(step: "Kalafior kroimy na plastry lub dzielimy na małe kawałeczki. Na patelni rozgrzewamy masło i podsmażamy kawałki kalafiora na złoty kolor. Pierniczki rozkruszamy i podpiekamy na suchej patelni. Zupę nalewamy do miseczek, podajemy z kawałkami kalafiora, posypaną pierniczkami i ozdobioną świeżymi listkami maggi."))
        // ---- recipe
        recipe = Recipe(recipeID: 14, recipeName: "Krem z dyni z pieczonym kalafiorem i piernikową nutą", recipeAuthor: "Arkadiusz Czapski-Pruszak", recipeBlog: "", recipeThumb: "15t", recipeImage: "15", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "kilka sztuk Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "pełnoziarnista bagietka"))
        ingrediens.append(createIngredient(imperial: "", metric: "ser ricotta"))
        ingrediens.append(createIngredient(imperial: "", metric: "kilka jeżyn"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 figi"))
        ingrediens.append(createIngredient(imperial: "", metric: "miód"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Bagietkę kroimy na kawałki i zapiekamy przez chwilę w piekarniku lub tosterze, żeby stała się chrupiąca. Każdy kawałek smarujemy grubą warstwą ricotty, na to układamy plasterki figi i kilka jeżyn oraz polewamy obficie miodem i posypujemy pokruszonymi pierniczkami. Po upieczeniu, grzanki możemy także posmarować masłem."))
        // ---- recipe
        recipe = Recipe(recipeID: 15, recipeName: "Owocowe grzanki z posypką z toruńskich pierników i miodem", recipeAuthor: "Bogumiła Szymańska", recipeBlog: "www.slodkiezycie.com", recipeThumb: "16t", recipeImage: "16", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 5 tartaletek o średnicy ok. 8 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "4 szt. Katarzynek® VEGAN"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 łyżki oleju kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki syropu klonowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok. 150 g batatów"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki masła orzechowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki mleka kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/3 szklanki syropu klonowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "świeże owoce: figi, maliny, jeżyny"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Katarzynki® VEGAN rozdrabniamy w blenderze, dodajemy olej kokosowy i syrop klonowy. Miksujemy do otrzymania masy o konsystencji mokrego piasku. Masą wyklejamy brzegi i dno foremek do tartaletek, dociskając dokładnie. Tartaletki wstawiamy do lodówki na około 2 godziny. Bataty zawijamy w folię aluminiową i pieczemy w temperaturze 200°C przez około godzinę. Odstawiamy do ostudzenia."))
        methods.append(createMethodStep(step: "W misie blendera umieszczamy upieczonego i ostudzonego batata, masło orzechowe i mleko kokosowe. Miksujemy do otrzymania gładkiej masy. Dodajemy czekoladę rozpuszczoną w kąpieli wodnej i miksujemy ponownie. Następnie dodajemy syrop klonowy, dla uzyskania pożądanej słodkości. Pierniczkowe spody delikatnie wyjmujemy z foremek. Na schłodzone tartaletki układamy mus czekoladowy i wkładamy ponownie do lodówki. Po około 2 godzinach, możemy przystąpić do dekorowania tartaletek. Ozdabiamy je świeżymi owocami według własnego uznania."))
        // ---- recipe
        recipe = Recipe(recipeID: 16, recipeName: "Wegańskie tartaletki z musem czekoladowym na bazie batatów", recipeAuthor: "Bogumiła Szymańska", recipeBlog: "www.slodkiezycie.com", recipeThumb: "17t", recipeImage: "17", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/tortownica o średnicy ok. 20 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Spód",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Uszatków®"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 ml mleka kokosowego z puszki /płynna część/ "))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki oleju kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Masa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "3 kostki tofu naturalnego o wadze ok. 180 gramów"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 gramów ksylitolu"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 gramów gorzkiej czekolady o zawartości kakao 80%"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki mąki ziemniaczanej"))
        ingrediens.append(createIngredient(imperial: "", metric: "sok z jednej cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "stała część mleka kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Polewa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "100 gramów gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 ml dowolnego mleka roślinnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Do ozdoby",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Uszatki® lub Katarzynki® w czekoladzie"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Uszatki® rozdrabniamy w mikserze na mąkę. Dodajemy płynną część mleka kokosowego i 2 łyżki oleju kokosowego. Mieszamy i wykładamy masę do tortownicy wyłożonej papierem do pieczenia. Spód tofurnika wkładamy do lodówki na około 30 minut. Czekoladę rozpuszczamy w kąpieli wodnej. Tofu odsączamy, dodajemy sok z cytryny, stałą część mleka kokosowego, ksylitol, rozpuszczoną czekoladę i mąkę ziemniaczaną. Masę dokładnie miksujemy – musi być aksamitna i pozbawiona grudek. Wylewamy na spód. Pieczemy w temperaturze 150°C ok. 40 minut.  Sprawdzamy patyczkiem, czy tofurnik jest już upieczony. Przygotowujemy polewę: czekoladę rozpuszczam z mlekiem roślinnym. Polewamy ostudzony tofurnik."))
        // ---- recipe
        recipe = Recipe(recipeID: 17, recipeName: "Tofurnik Piernikowy", recipeAuthor: "Emilia Konkol-Pastuszak", recipeBlog: "www.konkolpolny.pl", recipeThumb: "18t", recipeImage: "18", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "200 g pieczonych batatów"))
        ingrediens.append(createIngredient(imperial: "", metric: "400 ml mleczka kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g mrożonych owoców: maliny, czerwone porzeczki, czarne porzeczki"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki ekstraktu waniliowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/3 łyżeczki soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "kilka opakowań Katarzynek® bez dodatku czekolady lub Katarzynek® VEGAN"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Bataty oraz wszystkie składniki, z wyjątkiem owoców, obieramy ze skórki i dodajemy do małego rondelka. Podgrzewamy, aż do rozpuszczenia składników. Blendujemy i odstawiamy do ostygnięcia. Do ostudzonej masy dodajemy mrożone owoce. Blendujemy. Masę przelewamy do plastikowego pudełka i wkładamy do zamrażarki przez 3 godziny dokładnie i intensywnie mieszamy – co 30 minut. Podajemy z Katarzynkami® lub Katarzynkami® VEGAN."))
        // ---- recipe
        recipe = Recipe(recipeID: 18, recipeName: "Lodowa Katarzynka®", recipeAuthor: "Ewelina Jaroszczuk", recipeBlog: "www.instagram.com/wszechlinka", recipeThumb: "19t", recipeImage: "19", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "Kluski",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g twarogu półtłustego"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 szklanki mąki orkiszowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Katarzynek® w czekoladzie"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 duże jajko"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól"))
        ingrediens.append(createIngredient(imperial: "", metric: "Sos śliwkowy",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "15 węgierek"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka cukru muscovado"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki brandy"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Katarzynek® w czekoladzie"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Twaróg rozkruszamy dobrze widelcem. Pierniki mielimy na proszek, razem z mąką orkiszową i solą – dodajemy do twarogu wraz z jajkiem i wyrabiamy gładkie i zwarte ciasto. Ciasto owijamy w folię spożywczą i wkładamy na 1 godzinę do lodówki. Śliwki przekrawamy na pół, wyjmujemy pestki. Na patelni rozpuszczamy masło, dodajemy cukier oraz śliwki. Po krótkim przesmażeniu dolewamy brandy. Gotujemy razem, aż sos zgęstnieje. Wkruszamy pierniczki w czekoladzie."))
        methods.append(createMethodStep(step: "Ciasto wyjmujemy z lodówki, dzielimy na kulki nieco większe od orzecha włoskiego. Każdą kulę delikatnie spłaszczamy i robimy po środku wgłębienie. Kluski gotujemy w osolonej wodzie ok. 5 minut. Odkładamy na talerz. Podajemy na ciepło z sosem śliwkowym."))
        // ---- recipe
        recipe = Recipe(recipeID: 19, recipeName: "Kluski piernikowe z sosem śliwkowym", recipeAuthor: "Iwona Jakubowska", recipeBlog: "www.najedzeni.blogspot.com", recipeThumb: "20t", recipeImage: "20", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "Brownie",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Serca Toruńskie® z lukrem o smaku pomarańczowym "))
        ingrediens.append(createIngredient(imperial: "", metric: "tabliczka czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szklanki mąki tortowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 i 1/2 szklanki puree z upieczonej dyni"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 jaja"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka jogurtu naturalnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 szklanki oleju"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/4 szklanki roztopionego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "Masa dyniowa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "szklanka puree z dyni"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g serka mascarpone"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 ml śmietanki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "galaretka bezbarwna"))
        ingrediens.append(createIngredient(imperial: "", metric: "5 łyżek cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "Dodatkowo",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "tabliczka czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "cukier na karmel"))
        ingrediens.append(createIngredient(imperial: "", metric: "owoce do ozdoby"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Jajka ubijamy z cukrem na jednolitą masę. Dodajemy jogurt, rozpuszczone masło, olej, puree z dyni oraz roztopioną, ostudzoną czekoladę. Do mąki dodajemy pokruszone Serca Toruńskie®. Całość mieszamy i pieczemy w temp 180°C przez 30 min. Ciasto dobrze chłodzimy. Mocno schłodzoną śmietankę kremówkę ubijamy, dodajemy zimny serek mascarpone, cukier puder, puree z dyni i rozpuszczoną w niecałej szklance wody tężejącą galaretkę. Masę wylewamy na brownie i schładzamy przez ponad godzinę. Ciasto polewamy rozpuszczoną czekoladą i zdobimy dekoracjami z karmelu – cukrem rozpuszczonym na gorącej patelni i rozlanym na papierze do pieczenia w formie wzorów – piernikami i owocami."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 20, recipeName: "Piernikowo-dyniowy torcik czekoladowy z pianką z mascarpone z dynią", recipeAuthor: "Izabela Michalczyk", recipeBlog: "", recipeThumb: "21t", recipeImage: "21", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 jajka /rozmiar L/"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g roztopionego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1,5 łyżeczki proszku do pieczenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok.1 szklanka mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "3-4 gruszki"))
        ingrediens.append(createIngredient(imperial: "", metric: "ok. 150 g gorgonzoli "))
        ingrediens.append(createIngredient(imperial: "", metric: "tymianek"))
        ingrediens.append(createIngredient(imperial: "", metric: "rozmaryn"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól"))
        ingrediens.append(createIngredient(imperial: "", metric: "chili"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Pierniki rozdrabniamy w malakserze i mieszamy z pozostałymi składnikami do uzyskania gładkiego ciasta. Gofry pieczemy w gofrownicy przez ok. 3-4 minuty. Gruszki kroimy w plastry o grubości ok. 1,5 cm i grillujemy. Gruszki układamy na wierzchu gofrów razem z pokrojoną gorgonzolą i posiekanymi ziołami."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 21, recipeName: "Wytrawne gofry piernikowe z gruszkami i gorgonozlą", recipeAuthor: "Jagoda Paździor-Saba", recipeBlog: "www.zucchini-blues.blogspot.com", recipeThumb: "22t", recipeImage: "22", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 10 piernikowych popsów/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g orzeszków ziemnych, niesolonych"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "garść wiórków lub płatków kokosowych"))
        ingrediens.append(createIngredient(imperial: "", metric: "garść sezamu lub drobno pokrojonych orzeszków, pestek"))
        ingrediens.append(createIngredient(imperial: "", metric: "Dodatkowo",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "10 patyczków drewnianych, np. do grilla oraz kokardki lub rafia – do dekoracji"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Orzeszki ziemne wsypujemy do malaksera lub blendera i miksujemy przez około 5-7 minut do uzyskania gładkiego masła orzechowego bez grudek. Dodajemy pokruszone pierniczki i miksujemy dalej, do uzyskania konsystencji mokrego piasku. Następnie formujemy kulki wielkości orzecha włoskiego, kładziemy na większym talerzu i wkładamy do lodówki na minimum godzinę. Przed wyciągnięciem kulek z lodówki, łamiemy lub siekamy czekoladę i roztapiamy ją w kąpieli wodnej."))
        methods.append(createMethodStep(step: "Gotujemy wodę w niedużym garnku, kładziemy na nim miskę, tak aby nie dotykała wody. Do miski wkładamy czekoladę. Zmniejszamy ogień i czekamy chwilę, aż czekolada się rozpuści. Przygotowujemy osobne talerzyki z wiórkami i sezamem (lub orzechami, pestkami). Wyjmujemy schłodzone kulki piernikowe i nabijamy po jednej na każdy patyczek. Każdą kulkę moczymy w roztopionej czekoladzie, a następnie w wybranej posypce, np. wiórkach, sezamie lub orzechach. Wszystkie piernikowe popsy wkładamy do wysokiego naczynia (np. słoika), aby czekolada zastygła. Podajemy od razu lub przechowujemy w lodówce maksymalnie do 2-3 dni."))
        // ---- recipe
        recipe = Recipe(recipeID: 22, recipeName: "Piernikowe popsy", recipeAuthor: "Joanna Byczkiewicz", recipeBlog: "www.opietruszka.pl", recipeThumb: "23t", recipeImage: "23", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/porcja dla 2 osób/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Katarzynek® VEGAN"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 puszka mleka kokosowego domowego lub gotowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 garść orzechów nerkowca /wcześniej namoczonych przez minimum 2 godziny/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 mały mrożony banan /pokrojony na kawałki/"))
        ingrediens.append(createIngredient(imperial: "", metric: "pół łyżeczki cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 cm kłącza świeżego imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "śmietanka kokosowa"))
        ingrediens.append(createIngredient(imperial: "", metric: "garść płatków kokosowych"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Blendujemy dokładnie na gładką masę namoczone i osuszone orzechy nerkowca. Dolewamy mleko kokosowe. Dodajemy pokruszone 3 pierniczki, mrożonego banana i cynamon."))
        methods.append(createMethodStep(step: "Ścieramy na tarce o małych oczkach imbir i dodajemy do pozostałych składników. Całość ponownie blendujemy. Przelewamy koktajl do szklanek. Ubijamy śmietankę kokosową, nakładamy po kilka łyżeczek na koktajl i lekko mieszamy. Dekorujemy pokruszonymi pierniczkami oraz płatkami kokosa. Wkładamy jeden pierniczek do dekoracji."))
        // ---- recipe
        recipe = Recipe(recipeID: 23, recipeName: "Wegański koktajl piernikowy", recipeAuthor: "Joanna Byczkiewicz", recipeBlog: "www.opietruszka.pl", recipeThumb: "24t", recipeImage: "24", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto ptysiowe",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "250 ml mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 ml wody"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "350 g mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "40 g naturalnego ciemnego kakao"))
        ingrediens.append(createIngredient(imperial: "", metric: "9 jajek /rozmiar M/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "Posypka piernikowa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Serc Toruńskich® z lukrem o smaku pomarańczowym"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 łyżki cukru gruboziarnistego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Orange Curd",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "50 ml świeżo wyciśniętego soku z pomarańczy"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 żółtka"))
        ingrediens.append(createIngredient(imperial: "", metric: "mała szczypta soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "Krem piernikowo-pomarańczowy",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g śmietanki kremówki 36% /dobrze schłodzonej/"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g serka mascarpone"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 szt. Serc Toruńskich® z lukrem o smaku pomarańczowym"))
        ingrediens.append(createIngredient(imperial: "", metric: "przygotowany wcześniej Orange Curd"))
        ingrediens.append(createIngredient(imperial: "", metric: "Karmel",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "500 g cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 ml wody"))
        ingrediens.append(createIngredient(imperial: "", metric: "Do dekoracji",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Kilka sztuk Serc Toruńskich® z lukrem o smaku pomarańczowym oraz skórka z pomarańczy."))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Posypka piernikowa",title: true))
        methods.append(createMethodStep(step: "Pierniczki mielimy w młynku do kawy na piach, następnie mieszamy z gruboziarnistym cukrem."))
        methods.append(createMethodStep(step: "Ciasto ptysiowe",title: true))
        methods.append(createMethodStep(step: "Wodę, mleko, cukier oraz pokrojone w kostkę masło umieszczamy w garnuszku, gotujemy do rozpuszczenia się masła. Wsypujemy przesianą mąkę wymieszaną z kakao i mieszając trzymamy jeszcze chwilę na ogniu – powinna powstać bardzo gęsta masa. Zdejmujemy z ognia i studzimy. Do ostudzonej masy dodajemy po kolei jajka i dobrze miksujemy (masa będzie wyglądać na zwarzoną, ale po chwili się ujednolici). Piekarnik rozgrzewamy do temperatury 200°C. Ciasto ptysiowe przekładamy do rękawa cukierniczego z okrągłą końcówką. Na dużą płaską blaszkę wyłożoną papierem do pieczenia wyciskamy małe, okrągłe porcje ciasta, zachowując odległości pomiędzy ptysiami i posypujemy je piernikową posypką."))
        methods.append(createMethodStep(step: "Gdy wyciśniemy pierwszą blaszkę ptysi, wstawiamy je do piekarnika i pieczemy w temperaturze 200°C przez około 12-14 minut, aż wyrosną. W międzyczasie wyciskamy kolejną porcję. Upieczone ptysie wyjmujemy z piekarnika i odstawiamy do wystudzenia. "))
        methods.append(createMethodStep(step: "Orange Curd",title: true))
        methods.append(createMethodStep(step: "Sok z pomarańczy, cukier, sól oraz żółtka umieszczamy w metalowej misce, roztrzepujemy i umieszczamy nad garnuszkiem z gotującą się wodą. Nieustannie ubijając, podgrzewamy, aż masa wyraźnie się spieni i będzie gorąca. Dodajemy pokrojone w kostkę masło i trzymamy nad parą jeszcze kilka minut, aż Orange Curd zgęstnieje. Studzimy."))
        methods.append(createMethodStep(step: "Krem piernikowo-pomarańczowy",title: true))
        methods.append(createMethodStep(step: "Serek mascarpone krótko miksujemy, dodając łyżka po łyżce ostudzony Orange Curd. Pierniczki mielimy w młynku do kawy na piach i dokładamy do masy z mascarpone. W osobnej misce ubijamy na sztywno schłodzoną śmietankę kremówkę, dodając pod koniec cukier puder. Bitą śmietanę łączymy z piernikowo-pomarańczowym serkiem mascarpone i krótko miksujemy. W razie potrzeby dosładzamy krem. Ostudzone ptysie nadziewamy od dołu kremem. Najłatwiej za pomocą rękawa lub szprycy cukierniczej z długą, cienką i okrągłą końcówką."))
        methods.append(createMethodStep(step: "Karmel",title: true))
        methods.append(createMethodStep(step: "Cukier wsypujemy do garnuszka i zalewamy wodą. Podgrzewamy, aż się rozpuści i powstanie bursztynowy karmel. Uwaga – nie mieszamy karmelu, można jedynie delikatnie przechylać garnek w jedną i drugą stronę."))
        methods.append(createMethodStep(step: "Nadziane kremem ptysie ostrożnie maczamy od spodu w gorącym bursztynowym karmelu i przyklejamy do przygotowanego wcześniej stożka z brystolu pokrytego papierem do pieczenia (mój stożek miał 60 cm wysokości i średnicę podstawy ok. 15 cm). Ptysie sklejamy od dołu do góry tworząc efektowną piramidę, gdzieniegdzie przyklejając pierniki. Na koniec wypełniamy luki skórką pomarańczową. Bierzemy do ręki widelec i maczając go w karmelu, polewamy wierzch ptysiowej piramidy, tworząc nitki, które będą otulać wieżę. Jeśli karmel w garnuszku zastygnie, wystarczy, że ponownie podgrzejemy go na małym ogniu, aż znów będzie płynny."))
        methods.append(createMethodStep(step: "Czekoladową wieżę ptysiową najlepiej podawać od razu – z czasem ptysie miękną, a karmelowe nici pod wpływem wilgoci się rozpuszczają. Smacznego!"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 24, recipeName: "Czekoladowa wieża ptysiowa z pierniczkami i nutą pomarańczy", recipeAuthor: "Justyna Dragan", recipeBlog: "www.nastoletniewypiekanie.blogspot.com", recipeThumb: "25t", recipeImage: "25", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "2 tabliczki gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "8-10 szt. Uszatków®"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 kostki masła "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mleka"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mrożonych lub świeżych malin"))
        ingrediens.append(createIngredient(imperial: "", metric: "6 jajek"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 czubate łyżki kakao"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 g mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "30 ml rumu /opcjonalnie/"))
        ingrediens.append(createIngredient(imperial: "", metric: "Krem z mascarpone",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g śmietanki 30%"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g mascarpone"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "Do dekoracji",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "kakao do oprószenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "Serca Toruńskie® i Uszatki®"))
        ingrediens.append(createIngredient(imperial: "", metric: "mrożone maliny"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Czekoladę razem z masłem i mlekiem rozpuszczamy w kąpieli wodnej. Mieszamy ze sobą całość i odstawiamy do ostudzenia. Przygotowanie ciasta rozpoczynamy od zmiksowania jajek z cukrem na puszystą i jasną masę. Wlewamy rum i miksujemy ponownie. Do masy dodajemy przesianą mąkę, kakao i cynamon i delikatnie mieszamy szpatułką. Na koniec wsypujemy posiekane drobno pierniczki (na kawałki ok. 1 cm) oraz maliny. Tortownicę o średnicy 26 cm wykładamy papierem do pieczenia. Przelewamy ciasto do blachy i pieczemy przez 50-60 minut w temperaturze 160°C do tzw. suchego patyczka. Po tym czasie wyjmujemy ciasto z piekarnika i zostawiamy do ostudzenia."))
        methods.append(createMethodStep(step: "Kiedy ciasto ostygnie przygotowujemy krem: miksujemy na najwyższych obrotach śmietankę z cukrem pudrem aż będzie sztywna, a następnie dodajemy mascarpone i krótko miksujemy – do połączenia się składników. Krem możemy nałożyć szpatułką na wierzch ciasta, tworząc chmurkę lub przełożyć do szprycy z końcówką i stworzyć małe rozetki na powierzchni brownie. Krem posypujemy delikatnie kakao, a potem ozdabiamy piernikami i mrożonymi malinami."))
        // ---- recipe
        recipe = Recipe(recipeID: 25, recipeName: "Pierniczkowe brownie", recipeAuthor: "Klaudia Sroczyńska", recipeBlog: "www.dusiowakuchnia.pl", recipeThumb: "26t", recipeImage: "26", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "400 g sarniego combra"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g dyni"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki oliwy"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 gałązki rozmarynu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 świeży liść laurowy"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 gałązka tymianku"))
        ingrediens.append(createIngredient(imperial: "", metric: "liście kolendry"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 ząbek czosnku"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 ml czerwonego wytrawnego wina"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 pomarańcza"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 dojrzała gruszka"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 pietruszka"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól, pieprz"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Comber",title: true))
        methods.append(createMethodStep(step: "Mięso zalewamy mlekiem i zostawiamy w chłodnym miejscu na kilka godzin. Posiekaną kolendrę i rozmaryn mieszamy z solą i świeżym pieprzem. Nacieramy comber w przyprawach i obsmażamy na oliwie przez krótka chwilę. Przekładamy mięso do naczynia żaroodpornego. Następnie dodajemy zmiażdżony czosnek i trochę wody. Wstawiamy do piekarnika i pieczemy przez 30 minut w temperaturze 180°C. Sos, który otrzymamy z pieczenia, przelewamy na patelnię. Wlewamy na patelnię wino i wyciśnięty sok z pomarańczy. Po zagotowaniu zmniejszamy ogień. Dodajemy świeży liść laurowy, gałązkę tymianku i rozdrobnioną masę piernikową. Całość przyprawiamy odrobiną soli i pieprzu. Podgrzewamy, aż zgęstnieje. Pokrojony comber polewamy sosem."))
        methods.append(createMethodStep(step: "Puree z dyni",title: true))
        methods.append(createMethodStep(step: "Dynię kroimy na mniejsze kawałki i usuwamy pestki. Piekarnik nagrzewamy do temperatury 180°C. Wstawiamy do piekarnika i pieczemy przez około 35 minut. Upieczoną dynię studzimy, obieramy ze skórki i miksujemy blenderem do otrzymania gładkiego puree."))
        methods.append(createMethodStep(step: "Pieczona gruszka",title: true))
        methods.append(createMethodStep(step: "Rozgrzewamy piekarnik do temperatury 180°C. Gruszki kroimy wzdłuż na pół i usuwamy gniazda nasienne. Rozpuszczamy masło na patelni i dodajemy miód. Całość dokładnie mieszamy. Ułożone w piekarniku gruszki należy często smarować sosem z masła i miodu podczas pieczenia. Pieczemy przez około 30 minut."))
        methods.append(createMethodStep(step: "Chipsy z pietruszki",title: true))
        methods.append(createMethodStep(step: "Korzeń pietruszki myjemy i obieramy. W garnku do smażenia frytek rozgrzewamy olej. Kroimy pietruszkę na cieniutkie wstążki. Wkładamy do rozgrzanego tłuszczu i trzymamy do momentu zarumienienia się. Po wyjęciu odsączamy na papierowych ręcznikach."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 26, recipeName: "Comber sarni w wytrawnym sosie piernikowym z pieczoną gruszką i dyniowym puree", recipeAuthor: "Krzysztof Tonder", recipeBlog: "", recipeThumb: "27t", recipeImage: "27", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "1 puszka ciecierzycy /400 g/"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki pasty tahini"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka soku z cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "5 łyżek oliwy"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 ząbek czosnku"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka kuminu"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz "))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Katarzynki® mielimy na proszek i odkładamy dwie łyżki. W wysokim naczyniu umieszczamy odsączoną z zalewy ciecierzycę oraz resztę składników. Miksujemy kilka minut do uzyskania jednolitej konsystencji. Jeżeli hummus będzie zbyt gęsty dodajemy odrobinę zalewy z ciecierzycy lub ciepłej wody. Gotowy hummus przekładamy do miski i posypujemy zmielonymi pierniczkami."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 27, recipeName: "Piernikowy Hummus", recipeAuthor: "Magdalena Ornowska", recipeBlog: "", recipeThumb: "28t", recipeImage: "28", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "500 g wołowiny "))
        ingrediens.append(createIngredient(imperial: "", metric: "400 ml piwa karmelowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "500 ml ciepłego bulionu "))
        ingrediens.append(createIngredient(imperial: "", metric: "90 g Uszatków®"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g miniaturek pieczarek"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 małe marchewki "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 cebula szalotka /duża/"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 ząbki czosnku /duże/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki chili"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżeczki musztardy ostrej"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 liście laurowe"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 goździki"))
        ingrediens.append(createIngredient(imperial: "", metric: "kilka gałązek świeżego tymianku"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki mąki"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki oliwy"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Przygotowujemy marynatę do mięsa: bardzo drobno siekamy czosnek. Obieramy listki z 2-3 gałązek tymianku. Rozcieramy w moździerzu 30 g pierników. Dodajmy posiekany czosnek, listki tymianku (możemy zastąpić suszonym – ok. pół łyżeczki), imbir, cynamon, odrobinę pieprzu i soli raz oliwę. Całość mieszamy. Marynatę wcieramy w mięso i zostawiamy na około 10 minut."))
        methods.append(createMethodStep(step: "W garnku o grubym dnie (najlepiej takim, który możemy wstawić do piekarnika), rozgrzewamy łyżkę masła i dodajemy pieczarki. Lekko solimy i doprawiamy pieprzem, dodajemy listki tymianku z jednej gałązki. Smażymy pieczarki na sporym ogniu, aby się nie dusiły, a smażyły (powinny nabrać złotego koloru). Przekładamy pieczarki na talerz. Na tej samej patelni ponownie roztapiamy jedną łyżkę masła i wrzucamy pokrojone skośnie (w około 4 milimetrowe plasterki) marchewki. Przyprawiamy tak samo jak pieczarki i podsmażamy przez około 3 minuty. Zdejmujemy i dokładamy do pieczarek. "))
        methods.append(createMethodStep(step: "Mięso w temperaturze pokojowej podsmażamy w tym samym garnku na dużym ogniu – ważne, żeby zamknęły się pory i nie puściło soków. Wołowina powinna mieć ładny, złoty kolor. Podsmażone mięso podsypujemy mąką i mieszamy dla równego koloru. Po dwóch minutach zalewamy zimnym piwem karmelowym. Po zagotowaniu się, zmniejszamy ogień, dodajemy ciepły bulion i ponownie gotujemy. Dodajemy marchewki, pieczarki i pokruszone pierniki. Doprawiamy liśćmi laurowymi, goździkami, tymiankiem, musztardą, solą, pieprzem i ewentualnie ostrą papryką."))
        methods.append(createMethodStep(step: "Całość przykrywamy szczelnie pokrywką lub dusimy na najmniejszym ogniu przez 150 minut. Możemy też włożyć do piekarnika i tam dusić w temperaturze 90°C przez taki sam czas. Na koniec odkrywamy, całość doprawiamy i dusimy jeszcze przez pół godziny – bez przykrycia."))
        // ---- recipe
        recipe = Recipe(recipeID: 28, recipeName: "Długo duszony gulasz wołowy w sosie karmelowo-piernikowym", recipeAuthor: "Małgorzata Socha", recipeBlog: "www.relaksodkuchni.pl", recipeThumb: "29t", recipeImage: "29", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "8 szt. matyjasów śledziowych z zalewy octowej /dobrze ukwaszonych/"))
        ingrediens.append(createIngredient(imperial: "", metric: "20 szt. śliwek suszonych "))
        ingrediens.append(createIngredient(imperial: "", metric: "skórka starta z 1 cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "5 szt. czerwonej cebuli "))
        ingrediens.append(createIngredient(imperial: "", metric: "sól morska "))
        ingrediens.append(createIngredient(imperial: "", metric: "pieprz czarny świeżo mielony "))
        ingrediens.append(createIngredient(imperial: "", metric: "pieprz cayenne"))
        ingrediens.append(createIngredient(imperial: "", metric: "olej rzepakowy do podsmażenia cebuli"))
        ingrediens.append(createIngredient(imperial: "", metric: "Sos piernikowy",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka oleju rzepakowego nierafinowanego "))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki mielonego kardamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka mielonych goździków"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka mielonego imbiru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczki chilli"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżeczki miodu "))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Katarzynek® bez dodatku czekolady "))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki wody przegotowanej "))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 soku z cytryny"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Śledzie kroimy w duże kawałki, a śliwki suszone w drobną kosteczkę. Podsmażamy pokrojoną w krążki cebule na patelni. Przygotowujemy sos: w małym garnku rozgrzewamy olej rzepakowy, dodajemy do niego wszystkie przyprawy i mieszamy przez minutę, aby wydobyć aromat. Następnie dodajemy zmielone na proszek pierniki i 3 łyżeczki miodu. Mieszając masę, możemy użyć trzepaczki i rozrzedzić wodą. Jako ostatni dodajemy sok z cytryny (nie można go dodać wcześniej, aby sos się nie rozwarstwił). Na koniec bierzemy naczynie, w którym danie będzie podawane i poprzekładamy warstwami: śledzie, cebula, śliwki, skórka z cytryny, sos. Możemy tak przełożyć kilka warstw, w zależności od kształtu naczynia. Zostawiamy w lodówce na min. 2-3 dni, aby wszystko nabrało wyjątkowego smaku i aromatu."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 29, recipeName: "Śledzie po toruńsku", recipeAuthor: "Małgorzata Kufel", recipeBlog: "", recipeThumb: "30t", recipeImage: "30", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 1 porcję/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 kwiat cukinii "))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g koziego sera"))
        ingrediens.append(createIngredient(imperial: "", metric: "20 g orzechów włoskich "))
        ingrediens.append(createIngredient(imperial: "", metric: "20 g jabłka "))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g młodej cukinii "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szt. Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka oleju sezamowego "))
        ingrediens.append(createIngredient(imperial: "", metric: "50 ml piwa piernikowego "))
        ingrediens.append(createIngredient(imperial: "", metric: "10 g masła "))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Ze środka kwiatka usuwamy pręcik i delikatnie go oczyszczamy (najlepiej pędzelkiem). Przygotowujemy farsz do nadziana kwiatka. Jabłko i ser kozi kroimy w kostkę 1 x 1 cm. Siekamy orzechy i pół piernika. Na koniec dodajemy olej sezamowy. Dokładnie mieszamy i odstawiamy na 10-15 minut. "))
        methods.append(createMethodStep(step: "Młodą cukinię kroimy w kostkę 1 x 1 cm, przekładamy do rondla, dodajemy piwo i podgrzewamy, aż do momentu zredukowania płynu, zdejmujemy z ognia i dodajemy masło, delikatnie mieszając. Ostrożnie wypełniamy kwiat cukinii kozim farszem. Rozgrzewamy patelnię i bez dodatku tłuszczu obracamy kwiat na gorącej powierzchni przez 1 minutę. Przed ułożeniem na talerzu siekamy pozostała połowę piernika, którego możemy użyć jako „crunch” pod cukinię. "))
        // ---- recipe
        recipe = Recipe(recipeID: 30, recipeName: "Kwiat cukinii faszerowany kozim serem i piernikiem", recipeAuthor: "Mateusz Jagielski", recipeBlog: "", recipeThumb: "31t", recipeImage: "31", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/Tortownica o średnicy 24 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Spód",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "3 op. Katarzynek® VEGAN "))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki cukru kokosowego /nierafinowanego/ "))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżeczki ekstraktu pomarańczowego "))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki masła roślinnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "Masa serowa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "800 g tofu"))
        ingrediens.append(createIngredient(imperial: "", metric: "500 ml mleka kokosowego z puszki"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 opakowania budyniu waniliowego wegańskiego /w proszku/"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki mąki kokosowej "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka cukru pudru trzcinowego "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka ekstraktu migdałowego "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka proszku do pieczenia "))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki świeżo wyciśniętego soku z cytryny "))
        ingrediens.append(createIngredient(imperial: "", metric: "skórka starta z jednej cytryny "))
        ingrediens.append(createIngredient(imperial: "", metric: "Do dekoracji",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 mały słoik powideł śliwkowych "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Katarzynek® VEGAN"))
        ingrediens.append(createIngredient(imperial: "", metric: "400 ml śmietany kokosowej do ubijania"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g chipsów kokosowych"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Spód",title: true))
        methods.append(createMethodStep(step: "Do malaksera wkładamy wszystkie składniki potrzebne do przygotowania spodu i miksujemy, aż otrzymamy konsystencję przypominającą drobny piasek. Przekładamy do tortownicy i ugniatamy łyżką. Wkładamy do lodówki na czas przygotowania masy serowej."))
        methods.append(createMethodStep(step: "Masa serowa",title: true))
        methods.append(createMethodStep(step: "Tofu dokładnie odcedzamy z wody. Wkładamy do malaksera wraz z wszystkimi pozostałymi składnikami, potrzebnymi do przygotowania masy serowej. Dokładnie miksujemy, aż powstanie nam idealnie gładka konsystencja. Tortownicę wykładamy papierem do pieczenia. Ciasto przelewamy na spód piernikowy. Pieczemy około 60 minut w temperaturze około 160°C z funkcją termoobiegu. Ciasto zostawiamy do wystygnięcia, najlepiej na całą noc."))
        methods.append(createMethodStep(step: "Dekoracje",title: true))
        methods.append(createMethodStep(step: "Śmietanę ubijamy, aż będzie idealnie sztywna. Wierzch tofurnika smarujemy powidłami śliwkowymi. Nakładamy ubitą śmietanę, pokruszone pierniki i posypujemy chipsami kokosowymi."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 31, recipeName: "Wegański tofurnik na piernikowo-pomarańczowym spodzie ze śmietaną kokosową", recipeAuthor: "Monika Kozak", recipeBlog: "www.pinupcooksuperfood.blogspot.com", recipeThumb: "32t", recipeImage: "32", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 16 sztuk/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g mąki pszennej typ 650"))
        ingrediens.append(createIngredient(imperial: "", metric: "150 ml mleka 3,2%"))
        ingrediens.append(createIngredient(imperial: "", metric: "25 g roztopionego masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "12 g świeżych drożdży"))
        ingrediens.append(createIngredient(imperial: "", metric: "25 g cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 jajo"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "Nadzienie",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g Pierników nadziewanych w białej czekoladzie Z Serca Fabryki®"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki płynnego miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki dżemu z czarnej porzeczki /opcjonalnie powidła śliwkowe/"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Do miski wkruszamy drożdże, dodajemy letnie mleko, cukier, 2 łyżki mąki. Wszystko mieszamy, odstawiamy na 15 minut, aby rozczyn zaczął pracować. Do rozczynu dolewamy roztopione masło. Dodajemy przesianą mąkę, sól, cynamon, wbijamy jajko i wyrabiamy gładkie, elastyczne ciasto. Jeśli ciasto jest zbyt luźne dosypujemy mąki. Formujemy kulę i wkładamy ją do miski, przykrywamy lnianą ściereczką i odstawiamy w ciepłe miejsce na godzinę w celu podwojenia objętości."))
        methods.append(createMethodStep(step: "Przygotowujemy nadzienie. Do blendera wrzucamy przekrojone na pół pierniki, dodajemy miód i dżem, blendujemy do uzyskania jednolitej masy. Wyrośnięte ciasto chwilę wyrabiamy, na posypanej mąką stolnicy, rozwałkowujemy je na prostokąt. Smarujemy przygotowanym nadzieniem. Ciasto zawijamy w rulon zaczynając od dłuższego boku prostokąta. Ostrym nożem kroimy je na plastry o grubości 1 cm. Zawijańce układamy na blaszce wyłożonej papierem do pieczenia, zachowujemy odstępy, bo ciasto się napuszy. Całość przykrywamy lnianą ściereczką i odstawiamy w ciepłe miejsce na około 25 minut w celu wyrośnięcia."))
        methods.append(createMethodStep(step: "Piekarnik rozgrzewamy do temperatury 180°C z funkcją grzania: góra-dół. Wkładamy zawijańce do piekarnika i pieczemy 20-22 minuty do ładnego zezłocenia. Odstawiamy do ostygnięcia, choć ciepłe są również przepyszne!"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 32, recipeName: "Zawijańce z piernikowym nadzieniem ", recipeAuthor: "Monika Adamczyk", recipeBlog: "www.matkawariatka.net", recipeThumb: "33t", recipeImage: "33", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/tortownica o średnicy 20 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "8 szt. Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "250 g maki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka cukru pudru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka proszku do pieczenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta soli"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 całe jajko"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 żółtko"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki kwaśnej śmietany"))
        ingrediens.append(createIngredient(imperial: "", metric: "Masa jabłeczna",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "600 g obranych jabłek"))
        ingrediens.append(createIngredient(imperial: "", metric: "3 łyżki cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka soku z cytryny"))
        ingrediens.append(createIngredient(imperial: "", metric: "1,5 łyżeczki żelatyny"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki wrzątku"))
        ingrediens.append(createIngredient(imperial: "", metric: "Kruszonka",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "2 szt. Serc Toruńskich®"))
        ingrediens.append(createIngredient(imperial: "", metric: "20 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki brązowego cukru"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Pierniki na ciasto i na kruszonkę łamiemy na kawałeczki, rozkładamy na blaszce i zostawiamy na 8-10 godzin. Wysuszone pierniki miksujemy na proszek. Około 160 g zmielonych pierników mieszamy na stolnicy z mąką, solą, cukrem pudrem i proszkiem do pieczenia. Dodajemy masło i siekamy na drobne kawałki. Dodajemy jajko, żółto oraz śmietanę. Zagniatamy dłońmi ciasto i lepimy kulę. Następnie rozwałkowujemy na koło o średnicy ok. 30 cm."))
        methods.append(createMethodStep(step: "Przekładamy do natłuszczonej tortownicy i wylepiamy nim jej dno i brzegi. Wstawiamy do lodówki na 30 minut. Jabłka kroimy w kostkę 1 x 1 cm. Wrzucamy je do garnka i mieszamy z cukrem. Stawiamy na ogniu i podgrzewamy, aż jabłka puszczą sok i cukier się rozpuści. Dodajemy sok z cytryny i cynamon. Żelatynę rozpuszczamy we wrzątku i wlewamy do jabłek. Dobrze mieszamy i zestawiamy z ognia. Piekarnik rozgrzewamy do temperatury 200°C."))
        methods.append(createMethodStep(step: "Z podanych składników zagniatamy rękami kruszonkę. Tortownicę wyjmujemy z lodówki, spód ciasta nakłuwamy widelcem w kilku miejscach. Wstawiamy do pieca i pieczemy 10 minut. Wyjmujemy, wypełniamy ciasto masą jabłeczną, a na wierzchu rozsypujemy kruszonkę. Wstawiamy ponownie do piekarnika i pieczemy przez 25 minut. Po upieczeniu zdejmujemy rant tortownicy i odstawiamy szarlotkę na minimum 5 godzin do wystygnięcia."))
        // ---- recipe
        recipe = Recipe(recipeID: 33, recipeName: "Piernikowa szarlotka", recipeAuthor: "Natalia Bogubowicz", recipeBlog: "www.deliciousplacebynatalia.blogspot.com", recipeThumb: "34t", recipeImage: "34", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "3 płaty śledzia solonego"))
        ingrediens.append(createIngredient(imperial: "", metric: "bagietka"))
        ingrediens.append(createIngredient(imperial: "", metric: "Mus śliwkowy",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g śliwek"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 szt. Katarzynek® bez dodatku czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "1-2 szczypty przyprawy do piernika"))
        ingrediens.append(createIngredient(imperial: "", metric: "Konfitura z czerwonej cebuli",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "4 duże cebule"))
        ingrediens.append(createIngredient(imperial: "", metric: "100 ml wina porto"))
        ingrediens.append(createIngredient(imperial: "", metric: "4 łyżki octu balsamicznego"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "sól i pieprz "))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Śliwki drylujemy i wrzucamy do rondelka z nieprzywierającą powłoką. Dodajemy rozdrobnione pierniki i przyprawę. Przez około 20 minut gotujemy powoli na małym ogniu, aż śliwki się rozpadną, a całość zgęstnieje. Następnie blendujemy. "))
        methods.append(createMethodStep(step: "Konfitura",title: true))
        methods.append(createMethodStep(step: "Cebulę kroimy w piórka i wrzucamy do rondelka na roztopione masło i solimy. Smażymy, aż będzie szklista. Wtedy dodajemy resztę składników i gotujemy na małym ogniu, do zredukowania płynów. Przyprawiamy solą i pieprzem. Z tej ilości składników konfitury wyjdzie zdecydowanie za dużo. Gorącą wkładamy do słoiczka i odstawiamy do góry dnem, na następny raz. "))
        methods.append(createMethodStep(step: "Musimy upewnić się czy śledzie nie są zbyt słone. Jeśli tak, wkładamy je na chwilę do zimnej wody. Bagietkę kroimy na kromki ok 1-1,5 cm grubości. Na suchej patelni robimy grzanki, rozsmarowujemy na nich mus śliwkowy i kładziemy kawałeczki śledzia. Wykańczamy konfiturą cebulową i posypujemy pokruszonymi pierniczkami i pieprzem. Możemy też na talerzu rozsmarować mus, płat śledziowy posmarować konfiturą cebulową, zwinąć w roladkę i spiąć wykałaczką. Całość kładziemy na musie i posypujemy pokruszonymi pierniczkami i pieprzem."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 34, recipeName: "Śledzie z konfiturą cebulową", recipeAuthor: "Renata Wolszczak", recipeBlog: "", recipeThumb: "35t", recipeImage: "35", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka pokruszonych Katarzynek® bez dodatku czekolady lub Uszatków®"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka ulubionych orzechów /np. nerkowce, laskowe, migdały i orzeszki ziemne/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka płatków owsianych górskich"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki siemienia lnianego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki sezamu"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 szklanki miodu"))
        ingrediens.append(createIngredient(imperial: "", metric: "500 ml jogurtu"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Pierniki kruszymy i dodajemy orzechy, płatki, siemię, sezam oraz 1/2 szklanki miodu. Mieszamy i wykładamy na wyłożoną papierem do pieczenia blachę. Wkładamy do nagrzanego do 150°C piekarnika na około 30 min. Pozostałą część miodu mieszamy dokładnie z jogurtem. Nakładamy jogurt na przemian z granolą do szklanego naczynia w dowolnych proporcjach. "))
        // ---- recipe
        recipe = Recipe(recipeID: 35, recipeName: "Piernikowa granola z jogurtem", recipeAuthor: "Roger Kołomocki", recipeBlog: "www.cookzoom.pl", recipeThumb: "36t", recipeImage: "36", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "500 ml śmietanki kremówki 36%"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Katarzynek® bez dodatku czekolady "))
        ingrediens.append(createIngredient(imperial: "", metric: "4 żółtka jaj"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g cukru"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 tabliczki gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g orzechów"))
        ingrediens.append(createIngredient(imperial: "", metric: "50 g rodzynek lub innych bakalii "))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Żółtka miksujemy z cukrem na jednolitą masę. Do osobnego dużego naczynia wlewamy śmietanę i dodajemy pokruszone pierniki. Miksujemy, dodając w trakcie wcześniej ubite żółtka. Na koniec dodajemy pokruszoną drobno czekoladę, orzechy i bakalie. Całość tym razem tylko mieszamy. Lody przekładamy do metalowej formy lub plastikowego pojemnika i mrozimy przez całą noc. Na samym początku mieszamy masę co ok. 1 godz. (3-4 razy). Lody możemy przyozdobić piernikami i posypać cynamonem lub kakao."))
        // ---- recipe
        recipe = Recipe(recipeID: 36, recipeName: "Lody piernikowe", recipeAuthor: "Roger Kołomocki", recipeBlog: "www.cookzoom.pl", recipeThumb: "37t", recipeImage: "37", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/forma o wymiarach 21 x 28 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g masła"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g czekolady gorzkiej o zawartości kakao 70% "))
        ingrediens.append(createIngredient(imperial: "", metric: "4 jajka "))
        ingrediens.append(createIngredient(imperial: "", metric: "230 g cukru trzcinowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "180 g mąki orkiszowej 180g"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta soli himalajskiej"))
        ingrediens.append(createIngredient(imperial: "", metric: "0,5 łyżeczki kardamonu "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka nalewki orzechowej /opcjonalnie/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 op. Mieszanki Toruńskiej"))
        ingrediens.append(createIngredient(imperial: "", metric: "Polewa",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "100 g czekolady gorzkiej o zawartości kakao 70%"))
        ingrediens.append(createIngredient(imperial: "", metric: "Do dekoracji",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "kulki z melona i kiwi"))
        ingrediens.append(createIngredient(imperial: "", metric: "serca wycięte z arbuza"))
        ingrediens.append(createIngredient(imperial: "", metric: "mięta"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Masło roztapiamy w garnku na małym ogniu, a następnie dodajemy połamaną na kawałki czekoladę. Składniki podgrzewamy i mieszamy do momentu całkowitego rozpuszczenia i połączenia. Powstałą masę odstawiamy z palnika i pozostawiamy do lekkiego przestygnięcia. W misce mieszamy ze sobą (za pomocą trzepaczki lub miksera ręcznego): jajka, cukier trzcinowy i nalewkę orzechową. "))
        methods.append(createMethodStep(step: "Do masy jajecznej dodajemy wcześniej przygotowaną masę maślano-czekoladową. Mieszamy (lub krótko miksujemy) do połączenia się składników. Dodajemy przesianą mąkę orkiszową, szczyptę soli himalajskiej i kardamon. Mieszamy do momentu połączenia się składników. Dodajemy pokrojoną dość grubo Mieszankę Toruńską (zostawiamy 5 sztuk pierników do dekoracji)."))
        methods.append(createMethodStep(step: "Przygotowaną masę lekko mieszamy, aby pokrojone pierniki zmieszały z ciastem. Przelewamy je do foremki wyłożonej papierem do pieczenia. Na górnej części ciasta kładziemy odłożone pierniki i lekko je wciskamy do środka. Ciasto pieczemy w temperaturze 160°C przez ok. 35 min. Upieczone i przestudzone ciasto polewamy rozpuszczoną w kąpieli wodnej gorzką czekoladą. Do dekoracji możemy użyć melona, kiwi, arbuza lub mięty. "))
        
        // ---- recipe
        recipe = Recipe(recipeID: 37, recipeName: "Brownie z Mieszanką Toruńską ", recipeAuthor: "Roksana Wilk", recipeBlog: "www.instagram.com/planet.kai", recipeThumb: "38t", recipeImage: "38", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/przepis na ok. 8-10 sztuk/",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "Ciasto na naleśniki",title: true))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mleka 3,2 %"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mąki pszennej"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta cynamonu "))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka cukru waniliowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 jajka"))
        ingrediens.append(createIngredient(imperial: "", metric: "3/4 szklanki wody gazowanej"))
        ingrediens.append(createIngredient(imperial: "", metric: "do smażenia: olej kokosowy"))
        ingrediens.append(createIngredient(imperial: "", metric: "do posypania: 1 op. Katarzynek® VEGAN"))
        ingrediens.append(createIngredient(imperial: "", metric: "Mus śliwkowy",title:true))
        ingrediens.append(createIngredient(imperial: "", metric: "ok. 1 kg śliwek węgierek"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 laska cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "3-4 łyżki cukru lub ksylitolu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 laska wanilii /możemy zastąpić ekstraktem waniliowym/"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 łyżeczki gałki muszkatołowej + 2 gałązki świeżego tymianku"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Aby zrobić naleśniki: wszystkie składniki na ciasto miksujemy na gładką konsystencję. Ciasto schładzamy w lodówce min. 30 minut. Na rozgrzanej patelni z olejem kokosowym smażymy naleśniki."))
        methods.append(createMethodStep(step: "Aby zrobić mus śliwkowy: w garnku o żeliwnym dnie podgrzewamy cukier, świeży tymianek i cynamon. Następnie dodajemy drylowane śliwki oraz naciętą wzdłuż laskę wanilii. Całość doprowadzamy do wrzenia, zmniejszamy ogień i pozostawiamy pod przykryciem na ok. 15 minut – od czasu do czasu mieszając, aby śliwki nie przywarły do dna. Naleśniki podajemy z gorącym musem śliwkowym i piernikami startymi na tzw. grubych oczkach."))
        // ---- recipe
        recipe = Recipe(recipeID: 38, recipeName: "Naleśniki z posypką piernikową", recipeAuthor: "Zofia Cudny", recipeBlog: "www.makecookingeasier.pl", recipeThumb: "39t", recipeImage: "39", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "/na 2 porcje/",title:true))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki płatków owsianych błyskawicznych"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka masła tahini"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 szklanka mleka migdałowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka syropu klonowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1/2 szklanki owoców sezonowych"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 opakowanie Katarzynek® VEGAN"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "W rondelku podgrzewamy płatki owsiane wraz z mlekiem migdałowym, syropem i pastą sezamową (tahini). Całość gotujemy aż masa zgęstnieje. Kiedy płatki wchłoną płyn, miksujemy blenderem na gładką masę. Jeżeli całość jest zbyt gęsta dodajemy więcej mleka. Masę przekładamy do pucharków na przemian z pierniczkami i owocami. Deser odstawiamy na 5-10 minut, aby smaki się przegryzły. "))
        methods.append(createMethodStep(step: "Możliwości",title:true))
        methods.append(createMethodStep(step: "Zamiast płatków owsianych możemy użyć: kaszy jaglanej, ryżu, płatków orkiszowych lub żytnich."))
        methods.append(createMethodStep(step: "Pastę tahini – możemy zastąpić dowolnym masłem orzechowym."))
        methods.append(createMethodStep(step: "Mleko migdałowe możemy zastąpić dowolnym mlekiem roślinnym lub zwykłym mlekiem krowim."))
        methods.append(createMethodStep(step: "Syrop klonowy możemy zastąpić: syropem z agawy, daktylami, miodem, ksylitolem, cukrem kokosowym."))
        // ---- recipe
        recipe = Recipe(recipeID: 39, recipeName: "Budyń owsiany z Katarzynki® VEGAN", recipeAuthor: "Kinga Paruzel", recipeBlog: "www.kingaparuzel.pl", recipeThumb: "40t", recipeImage: "40", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "", metric: "3-4 op. Katarzynek® VEGAN"))
        ingrediens.append(createIngredient(imperial: "", metric: "300 g upieczonej dyni np. hokaido"))
        ingrediens.append(createIngredient(imperial: "", metric: "200 g mąki jaglanej lub orkiszowej"))
        ingrediens.append(createIngredient(imperial: "", metric: "4-5 łyżek cukru trzcinowego lub kokosowego"))
        ingrediens.append(createIngredient(imperial: "", metric: "około 1/2 szklanki mleka roślinnego"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 tabliczki gorzkiej czekolady"))
        ingrediens.append(createIngredient(imperial: "", metric: "1-2 łyżek imbiru suszonego"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżka cynamonu"))
        ingrediens.append(createIngredient(imperial: "", metric: "1 łyżeczka bezglutenowego proszku do pieczenia"))
        ingrediens.append(createIngredient(imperial: "", metric: "2 łyżki oleju"))
        ingrediens.append(createIngredient(imperial: "", metric: "szczypta soli"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(step: "Dynię kroimy na pół. Pieczemy w temperaturze 180-200ºC przez około 20 minut, aż zmięknie. Przekładamy ostudzony miąższ z dyni do miski. Blendujemy na jednolitą masę. Dodajemy mąkę jaglaną, cukier, przyprawy, proszek i sól. Mieszamy. Powoli dodajemy mleko, tak by masa była mokra i delikatnie rzadka. Rozpuszczamy czekoladę w kąpieli wodnej, dodajemy do przygotowanej masy i dokładnie mieszamy. Ciasto powinno mieć jednolitą barwę. Na koniec dodajemy olej i ponownie mieszamy."))
        methods.append(createMethodStep(step: "Delikatnie natłuszczamy dno foremki układamy Katarzynki® VEGAN, tak by przykryć dno formy. Przelewamy ciasto i wyrównujemy łopatką. Jeśli został pierniczki, można je pokruszyć i posypać wierzch ciasta. Pieczemy w rozgrzanym do 175-180ºC piekarniku do suchego patyczka."))
        // ---- recipe
        recipe = Recipe(recipeID: 40, recipeName: "Korzenne brownie z dynią na piernikowym spodzie", recipeAuthor: "Alicja Rokicka", recipeBlog: "www.wegannerd.blogspot.com", recipeThumb: "41t", recipeImage: "41", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
    }
    else {
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "The tart can be made in two ways – a classic or a vegan version /optional ingredients in brackets/", metric: "The tart can be made in two ways – a classic or a vegan version /optional ingredients in brackets/", title: true))
        ingrediens.append(createIngredient(imperial: "Gingerbread and walnut base", metric: "Gingerbread and walnut base",title: true))
        ingrediens.append(createIngredient(imperial: "3.5 oz Katarzynki ® VEGAN or without chocolate", metric: "100 g Katarzynki ® VEGAN or without chocolate"))
        ingrediens.append(createIngredient(imperial: "2.5 oz ground walnuts", metric: "70 g ground walnuts"))
        ingrediens.append(createIngredient(imperial: "2.5 oz biscuits /classic or vegan/", metric: "70 g biscuits /classic or vegan/"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "3.5 oz melted butter /or coconut oil/", metric: "100 g melted butter /or coconut oil/"))
        ingrediens.append(createIngredient(imperial: "Apple roses", metric: "Apple roses",title: true))
        ingrediens.append(createIngredient(imperial: "5-6 apples /preferably the reddest ones/", metric: "5-6 apples /preferably the reddest ones/"))
        ingrediens.append(createIngredient(imperial: "2 lemons", metric: "2 lemons"))
        ingrediens.append(createIngredient(imperial: "1 glass sugar ", metric: "1 glass sugar "))
        ingrediens.append(createIngredient(imperial: "approx. 32 fl oz water", metric: "approx. 900 ml water"))
        ingrediens.append(createIngredient(imperial: "Vanilla pudding from millet groats", metric: "Vanilla pudding from millet groats",title: true))
        ingrediens.append(createIngredient(imperial: "26 fl oz milk /cow’s or vegetable/", metric: "750 ml milk /cow’s or vegetable/"))
        ingrediens.append(createIngredient(imperial: "4.5 oz millet groats", metric: "130 g millet groats"))
        ingrediens.append(createIngredient(imperial: "1/3 glass cane sugar", metric: "1/3 glass cane sugar"))
        ingrediens.append(createIngredient(imperial: "1 vanilla pod", metric: "1 vanilla pod"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Mix Katarzynki® with the biscuits, walnuts and cinnamon. Add melted butter (or coconut oil), mix and then lay the bottom and the sides of a tart tin with the pastry. Bake in the oven heated to 356ºF for about 15 minutes. Put aside.", step: "Mix Katarzynki® with the biscuits, walnuts and cinnamon. Add melted butter (or coconut oil), mix and then lay the bottom and the sides of a tart tin with the pastry. Bake in the oven heated to 180°C for about 15 minutes. Put aside."))
        methods.append(createMethodStep(imperialStep: "32 fl oz water boil with 1 glass of sugar and juice from two lemons. Halve the apples, get rid of apple cores and cut into thin slices (do not peel). Put the apples into the water and cook them for about 2 minutes (until the slices become elastic). Drain off and cool down. Put the millet groats into a saucepan, add sugar and vanilla seeds.", step: "900 ml water boil with 1 glass of sugar and juice from two lemons. Halve the apples, get rid of apple cores and cut into thin slices (do not peel). Put the apples into the water and cook them for about 2 minutes (until the slices become elastic). Drain off and cool down. Put the millet groats into a saucepan, add sugar and vanilla seeds."))
        methods.append(createMethodStep(imperialStep: "Pour the milk into the saucepan and heat it – cook for about 15 minutes (stirring from time to time). Next, mix it using a blender until it has the texture of pudding. Put the pudding mixture onto the earlier prepared base and then, place immediately the slices of apples rolled in shapes of roses (the best is to place a few slices of apples along next to each other, so they slightly come over and roll them). Such ‘apple roses’ can be optionally coated with, for example, honey or maple syrup, to get the glossy look. Cool the tart down in the fridge and then, serve it", step: "our the milk into the saucepan and heat it – cook for about 15 minutes (stirring from time to time). Next, mix it using a blender until it has the texture of pudding. Put the pudding mixture onto the earlier prepared base and then, place immediately the slices of apples rolled in shapes of roses (the best is to place a few slices of apples along next to each other, so they slightly come over and roll them). Such ‘apple roses’ can be optionally coated with, for example, honey or maple syrup, to get the glossy look. Cool the tart down in the fridge and then, serve it"))

        // ---- recipe
        recipe = Recipe(recipeID: 0, recipeName: "Tart with vanilla pudding and apple roses on gingerbread and walnut base", recipeAuthor: "Paulina Reczkowska", recipeBlog: "www.czekolada-utkane.pl", recipeThumb: "1t", recipeImage: "1", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/6 servings/", metric: "/6 servings/",title: true))
        ingrediens.append(createIngredient(imperial: "Rolls with gingerbread crumble", metric: "Rolls with gingerbread crumble",title: true))
        ingrediens.append(createIngredient(imperial: "18 oz wheat flour, type 550", metric: "500 g wheat flour, type 550"))
        ingrediens.append(createIngredient(imperial: "3 oz soft butter", metric: "90 g soft butter"))
        ingrediens.append(createIngredient(imperial: "1 oz fresh yeast", metric: "30 g fresh yeast"))
        ingrediens.append(createIngredient(imperial: "7 fl oz warm, skimmed milk", metric: "200 ml warm, skimmed milk"))
        ingrediens.append(createIngredient(imperial: "2 eggs /size L/", metric: "2 eggs /size L/"))
        ingrediens.append(createIngredient(imperial: "2 tbsp sugar", metric: "2 tbsp sugar"))
        ingrediens.append(createIngredient(imperial: "2 pieces Katarzynki® without chocolate ", metric: "2 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "Burgers", metric: "Burgers",title: true))
        ingrediens.append(createIngredient(imperial: "6 pieces Katarzynki® without chocolate", metric: "6 pieces Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "approx.26.5 oz beef mince", metric: "approx.750 g beef mince"))
        ingrediens.append(createIngredient(imperial: "3 tbsp French mustard", metric: "3 tbsp French mustard"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        ingrediens.append(createIngredient(imperial: "Shallot jam with brown beer", metric: "Shallot jam with brown beer",title: true))
        ingrediens.append(createIngredient(imperial: "6-8 shallots", metric: "6-8 shallots"))
        ingrediens.append(createIngredient(imperial: "12 fl oz brown beer", metric: "350 ml brown beer"))
        ingrediens.append(createIngredient(imperial: "3 tbsp butter", metric: "3 tbsp butter"))
        ingrediens.append(createIngredient(imperial: "1 tbsp buckwheat honey", metric: "1 tbsp buckwheat honey"))
        ingrediens.append(createIngredient(imperial: "3 bay leaves", metric: "3 bay leaves"))
        ingrediens.append(createIngredient(imperial: "5-6 grains allspice", metric: "5-6 grains allspice"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        ingrediens.append(createIngredient(imperial: "Spicy mayonnaise", metric: "Spicy mayonnaise",title: true))
        ingrediens.append(createIngredient(imperial: "1 big or 2 small yolks", metric: "1 big or 2 small yolks"))
        ingrediens.append(createIngredient(imperial: "1 tbsp vinegar /preferably rice vinegar/", metric: "1 tbsp vinegar /preferably rice vinegar/"))
        ingrediens.append(createIngredient(imperial: "1 tbsp mustard", metric: "1 tbsp mustard"))
        ingrediens.append(createIngredient(imperial: "½ glass rapeseed oil", metric: "½ glass rapeseed oil "))
        ingrediens.append(createIngredient(imperial: "¼ glass olive oil", metric: "¼ glass olive oil"))
        ingrediens.append(createIngredient(imperial: "1 tsp fine cane sugar", metric: "1 tsp fine cane sugar"))
        ingrediens.append(createIngredient(imperial: "ground cinnamon", metric: "ground cinnamon"))
        ingrediens.append(createIngredient(imperial: "grated nutmeg", metric: "grated nutmeg"))
        ingrediens.append(createIngredient(imperial: "chilli flakes", metric: "chilli flakes"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        ingrediens.append(createIngredient(imperial: "Extra", metric: "Extra",title: true))
        ingrediens.append(createIngredient(imperial: "3 ripe figs", metric: "3 ripe figs"))
        ingrediens.append(createIngredient(imperial: "oakleaf lettuce", metric: "oakleaf lettuce"))
        ingrediens.append(createIngredient(imperial: "dark opal basil", metric: "dark opal basil"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Rolls with gingerbread crumble", step: "Rolls with gingerbread crumble",title: true))
        methods.append(createMethodStep(imperialStep: "Sprinkle some sugar over the yeast and wait until dissolves. Stir it with a tablespoon of flour and 3 tablespoons of warm milk. Put aside the leaven to rise for about 15 minutes. Sift the flour and mix with remaining ingredients (apart from one egg). Knead to get smooth dough. It should be rather thin. Cover with a cloth and put aside to rise for about 40 minutes in a warm place.", step: "Sprinkle some sugar over the yeast and wait until dissolves. Stir it with a tablespoon of flour and 3 tablespoons of warm milk. Put aside the leaven to rise for about 15 minutes. Sift the flour and mix with remaining ingredients (apart from one egg). Knead to get smooth dough. It should be rather thin. Cover with a cloth and put aside to rise for about 40 minutes in a warm place."))
        methods.append(createMethodStep(imperialStep: "Divide the well-risen dough into 6 equal parts. Using hands greased in olive oil, form the rolls, put them apart on a baking tray laid with some baking paper. Put aside to rise for 30 minutes. Spread the bitten egg over the rolls and sprinkle them with crumbled gingerbread. Heat the oven to 392ºF. Put a baking tin filled with water on the lowest shelf so it makes steam. Put the baking tray with the rolls above and bake for about 35–40 minutes.", step: "Divide the well-risen dough into 6 equal parts. Using hands greased in olive oil, form the rolls, put them apart on a baking tray laid with some baking paper. Put aside to rise for 30 minutes. Spread the bitten egg over the rolls and sprinkle them with crumbled gingerbread. Heat the oven to 200°C. Put a baking tin filled with water on the lowest shelf so it makes steam. Put the baking tray with the rolls above and bake for about 35–40 minutes."))
        methods.append(createMethodStep(imperialStep: "Burgers", step: "Burgers",title: true))
        methods.append(createMethodStep(imperialStep: "Knead the beef with some mustard, salt and pepper, form equal burgers and flatten them. Crumble the gingerbread in a food processor until it looks like thick sand. Dredge the burgers in it. Grill for about 2 minutes, put aside for 3-4 minutes, to cool down.", step: "Knead the beef with some mustard, salt and pepper, form equal burgers and flatten them. Crumble the gingerbread in a food processor until it looks like thick sand. Dredge the burgers in it. Grill for about 2 minutes, put aside for 3-4 minutes, to cool down."))
        methods.append(createMethodStep(imperialStep: "Shallot jam with brown beer", step: "Shallot jam with brown beer",title:true))
        methods.append(createMethodStep(imperialStep: "Slice the shallots, put them with bay leaves and allspice into the preheated butter. Fry a little so the onion is tender. Pour the brown beer over, add some honey, salt and pepper. Fry the jam over low heat until the liquid disappears, and it becomes sticky.", step: "Slice the shallots, put them with bay leaves and allspice into the preheated butter. Fry a little so the onion is tender. Pour the brown beer over, add some honey, salt and pepper. Fry the jam over low heat until the liquid disappears, and it becomes sticky."))
        methods.append(createMethodStep(imperialStep: "Spicy mayonnaise", step: "Spicy mayonnaise",title: true))
        methods.append(createMethodStep(imperialStep: "Parboil the eggs. Separate whites from yolks. Mix the yolks with vinegar, mustard, sugar, salt and pepper. Pour slowly rapeseed oil and whisk or mix in a food processor, do not use blender).  When the mayonnaise starts to thicken pour the olive oil and mix until you get the required consistency. Season with cinnamon, nutmeg and chilli.", step: "Parboil the eggs. Separate whites from yolks. Mix the yolks with vinegar, mustard, sugar, salt and pepper. Pour slowly rapeseed oil and whisk or mix in a food processor, do not use blender).  When the mayonnaise starts to thicken pour the olive oil and mix until you get the required consistency. Season with cinnamon, nutmeg and chilli."))
        methods.append(createMethodStep(imperialStep: "At the end", step: "At the end",title: true))
        methods.append(createMethodStep(imperialStep: "Halve the cooled rolls and grill them from the inside. Spread some mayonnaise. Put the lettuce, burgers, jam, sliced figs, basil.", step: "Halve the cooled rolls and grill them from the inside. Spread some mayonnaise. Put the lettuce, burgers, jam, sliced figs, basil."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 1, recipeName: "Gingerbread burgers", recipeAuthor: "Jagoda Paździor-Saba", recipeBlog: "www.zucchini-blues.blogspot.com", recipeThumb: "2t", recipeImage: "2", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "7 oz butter", metric: "200 g butter"))
        ingrediens.append(createIngredient(imperial: "7.8 oz liquid honey", metric: "220 g liquid honey"))
        ingrediens.append(createIngredient(imperial: "7 fl oz milk", metric: "200 ml milk"))
        ingrediens.append(createIngredient(imperial: "4 eggs", metric: "4 eggs"))
        ingrediens.append(createIngredient(imperial: "12 oz wheat flour", metric: "340 g wheat flour"))
        ingrediens.append(createIngredient(imperial: "7 oz fine sugar", metric: "200 g fine sugar"))
        ingrediens.append(createIngredient(imperial: "2.8 oz frosted Serduszka Toruńskie (Toruń Hearts)", metric: "80 g frosted Serduszka Toruńskie (Toruń Hearts)"))
        ingrediens.append(createIngredient(imperial: "2 tsp baking soda", metric: "2 tsp baking soda"))
        ingrediens.append(createIngredient(imperial: "pinch of salt", metric: "pinch of salt"))
        ingrediens.append(createIngredient(imperial: "Cream", metric: "Cream",title: true))
        ingrediens.append(createIngredient(imperial: "2 blocks butter /at room temperature/", metric: "2 blocks butter /at room temperature/"))
        ingrediens.append(createIngredient(imperial: "5.3 oz icing sugar", metric: "150 g icing sugar"))
        ingrediens.append(createIngredient(imperial: "1.4 oz dark cocoa powder", metric: "40 g dark cocoa powder"))
        ingrediens.append(createIngredient(imperial: "3.5 oz milk chocolate", metric: "100 g milk chocolate"))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate", metric: "100 g dark chocolate"))
        ingrediens.append(createIngredient(imperial: "Extra", metric: "Extra",title: true))
        ingrediens.append(createIngredient(imperial: "2.8 oz Serduszka Toruńskie (Torun Hearts)", metric: "80 g Serduszka Toruńskie (Torun Hearts)"))
        ingrediens.append(createIngredient(imperial: "1 oz dark chocolate", metric: "30 g dark chocolate"))
        ingrediens.append(createIngredient(imperial: "1 oz white chocolate", metric: "30 g white chocolate"))
        ingrediens.append(createIngredient(imperial: "optionally fresh flowers", metric: "optionally fresh flowers"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Put the gingerbread into a food processor and mix into fine powder. Sift the flour with crumbled gingerbread, sugar, baking soda and salt. Put the butter and honey into a saucepan. Heat until it melts and then, add milk and stir precisely. Put aside to cool down. Next, pour it together with eggs into the flour. Mix all the ingredients until you get the homogeneous mixture and then, divide into two equal parts and pour into two 8-inch baking tins. Bake the cake in 356ºF for about 45 minutes, cool down on an oven shelf and when cooled, cut into half to get 4 slices.", step: "Put the gingerbread into a food processor and mix into fine powder. Sift the flour with crumbled gingerbread, sugar, baking soda and salt. Put the butter and honey into a saucepan. Heat until it melts and then, add milk and stir precisely. Put aside to cool down. Next, pour it together with eggs into the flour. Mix all the ingredients until you get the homogeneous mixture and then, divide into two equal parts and pour into two 20-centemetre baking tins. Bake the cake in 180°C for about 45 minutes, cool down on an oven shelf and when cooled, cut into half to get 4 slices."))
        methods.append(createMethodStep(imperialStep: "Prepare the cream: cream the butter with icing sugar and cocoa powder. Melt both kinds of chocolate over hot water. When the chocolate is cooled, add into the butter mixing all the time until it makes smooth cream. Leave a quarter of the cream to spread over the top and use the remaining part to spread the slices. Melt the chocolate for decoration over hot water. Next, spread the chocolate on bubble wrap and put into the fridge. After a few minutes, when the chocolate thickens, break it into smaller pieces. Decorate the cream cake with the broken chocolate, frosted Serduszka Toruńskie (Toruń Hearts) and fresh flowers. Before you serve, cool down for at least 2 hours.", step: "Prepare the cream: cream the butter with icing sugar and cocoa powder. Melt both kinds of chocolate over hot water. When the chocolate is cooled, add into the butter mixing all the time until it makes smooth cream. Leave a quarter of the cream to spread over the top and use the remaining part to spread the slices. Melt the chocolate for decoration over hot water. Next, spread the chocolate on bubble wrap and put into the fridge. After a few minutes, when the chocolate thickens, break it into smaller pieces. Decorate the cream cake with the broken chocolate, frosted Serduszka Toruńskie (Toruń Hearts) and fresh flowers. Before you serve, cool down for at least 2 hours."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 2, recipeName: "Honey and Gingerbread Cake", recipeAuthor: "Diana Kowalczyk", recipeBlog: "www.mientablog.com", recipeThumb: "3t", recipeImage: "3", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a baking tin sized 8 inch x 12 inch/", metric: "/a baking tin sized 20 x 30 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "4 oz Katarzynki® in chocolate", metric: "120 g Katarzynki® in chocolate"))
        ingrediens.append(createIngredient(imperial: "8.8 oz wheat cake flour", metric: "250 g wheat cake flour"))
        ingrediens.append(createIngredient(imperial: "7 oz butter", metric: "200 g butter"))
        ingrediens.append(createIngredient(imperial: "1 big egg", metric: "1 big egg"))
        ingrediens.append(createIngredient(imperial: "1/4 tsp salt", metric: "1/4 tsp salt"))
        ingrediens.append(createIngredient(imperial: "1 tsp baking powder", metric: "1 tsp baking powder"))
        ingrediens.append(createIngredient(imperial: "1 glass sugar", metric: "1 glass sugar"))
        ingrediens.append(createIngredient(imperial: "approx. 28 oz damson plums", metric: "approx. 800 g damson plums"))
        ingrediens.append(createIngredient(imperial: "icing sugar to sprinkle", metric: "icing sugar to sprinkle"))

        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Mix gingerbread in a food processor with a knife end. Add the remaining ingredients into Katarzynki® and mix until the dough starts to look like crumble. Take out of the food processor, knead shortly and put into the fridge for about 1 hour. The cooler, the better consistency it will have after the baking.", step: "Mix gingerbread in a food processor with a knife end. Add the remaining ingredients into Katarzynki® and mix until the dough starts to look like crumble. Take out of the food processor, knead shortly and put into the fridge for about 1 hour. The cooler, the better consistency it will have after the baking."))
        methods.append(createMethodStep(imperialStep: "The cake can also be made without the use of a food processor: sift the flour on the butter, add the remaining ingredients and rub with your fingers until you get the crumble. Note: this type of cake does not like warmth and it cannot be kneaded too long because it will be too hard. Wash the plums, halve, get rid of stones. Lay the baking tin with some baking paper.", step: "The cake can also be made without the use of a food processor: sift the flour on the butter, add the remaining ingredients and rub with your fingers until you get the crumble. Note: this type of cake does not like warmth and it cannot be kneaded too long because it will be too hard. Wash the plums, halve, get rid of stones. Lay the baking tin with some baking paper."))
        methods.append(createMethodStep(imperialStep: "Crumble two thirds of the dough at the bottom of the baking tin, gently squash with your hand, put the plums and cover with the remaining dough crumbling it onto the plums. Put the baking tin with the dough into the oven preheated to 338ºF and bake for about 45 minutes until it is golden brown. Take the cake out, cool down and cut into pieces. Before serving, sprinkle with some icing sugar.", step: "Crumble two thirds of the dough at the bottom of the baking tin, gently squash with your hand, put the plums and cover with the remaining dough crumbling it onto the plums. Put the baking tin with the dough into the oven preheated to 170°C and bake for about 45 minutes until it is golden brown. Take the cake out, cool down and cut into pieces. Before serving, sprinkle with some icing sugar."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 3, recipeName: "Gingerbread crumble cake with plums", recipeAuthor: "Agnieszka Czapska-Pruszak", recipeBlog: "www.zpamietnikapiekarnika.pl", recipeThumb: "4t", recipeImage: "4", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "Cake", metric: "Cake",title: true))
        ingrediens.append(createIngredient(imperial: "1 glass water", metric: "1 glass water"))
        ingrediens.append(createIngredient(imperial: "1 glass milk", metric: "1 glass milk"))
        ingrediens.append(createIngredient(imperial: "3.5 oz butter", metric: "100 g butter"))
        ingrediens.append(createIngredient(imperial: "1/4 tsp salt", metric: "1/4 tsp salt"))
        ingrediens.append(createIngredient(imperial: "5 oz wheat flour", metric: "150 g wheat flour"))
        ingrediens.append(createIngredient(imperial: "4 big eggs", metric: "4 big eggs"))
        ingrediens.append(createIngredient(imperial: "Cream", metric: "Cream",title: true))
        ingrediens.append(createIngredient(imperial: "14 fl oz whipping cream, 30% fat", metric: "400 ml whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "4 oz Katarzynki® in chocolate", metric: "120 g Katarzynki® in chocolate"))
        ingrediens.append(createIngredient(imperial: "Plum jam", metric: "Plum jam",title: true))
        ingrediens.append(createIngredient(imperial: "17.6 oz damson plums", metric: "1/2 kg damson plums"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Mix the milk with water, add salt and butter cut into pieces – cook. When it starts to boil, add the flour and mix briskly until the mixture thickens. Mix a little longer to let the mixture dry out and put aside to cool down. Add eggs one by one into the cooled mixture and mix after each one. You should get smooth and glossy dough. Put it into a pastry bag with an appropriate end and form 2-2.5-inch stripes on the baking tray laid with some baking paper.", step: "Mix the milk with water, add salt and butter cut into pieces – cook. When it starts to boil, add the flour and mix briskly until the mixture thickens. Mix a little longer to let the mixture dry out and put aside to cool down. Add eggs one by one into the cooled mixture and mix after each one. You should get smooth and glossy dough. Put it into a pastry bag with an appropriate end and form 5-6-centimetre stripes on the baking tray laid with some baking paper."))
        methods.append(createMethodStep(imperialStep: "Put the baking tray into the oven preheated to 392ºF and bake for about 15 minutes. Eclairs should be golden brown and dry from the outside and soft inside. Cut Katarzynki® into pieces. Pour the cream into a saucepan, add the cut Katarzynki®. Boil it and put aside. Stir it from time to time and try to crush the gingerbread. When Katarzynki® soak in cream, they will go soft. When everything cools down, mix using the blender and put into the fridge – to cool down completely. Cooled Katarzynki® cream will be so thick that the eclairs can be filled using an icing syringe.", step: "Put the baking tray into the oven preheated to 200°C and bake for about 15 minutes. Eclairs should be golden brown and dry from the outside and soft inside. Cut Katarzynki® into pieces. Pour the cream into a saucepan, add the cut Katarzynki®. Boil it and put aside. Stir it from time to time and try to crush the gingerbread. When Katarzynki® soak in cream, they will go soft. When everything cools down, mix using the blender and put into the fridge – to cool down completely. Cooled Katarzynki® cream will be so thick that the eclairs can be filled using an icing syringe."))
        methods.append(createMethodStep(imperialStep: "Wash the plums, halve them and get rid of stones. Put into a pot with a thick bottom and cook until you get thick jam. When all the ingredients are ready, start making eclairs. Halve the eclairs alongside. Spread the plum jam on the bottom part, put the Katarzynki® cream, cover with the top part and sprinkle with some icing sugar. Eclairs taste best on the first day after they are slightly cooled but still crispy.", step: "Wash the plums, halve them and get rid of stones. Put into a pot with a thick bottom and cook until you get thick jam. When all the ingredients are ready, start making eclairs. Halve the eclairs alongside. Spread the plum jam on the bottom part, put the Katarzynki® cream, cover with the top part and sprinkle with some icing sugar. Eclairs taste best on the first day after they are slightly cooled but still crispy."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 4, recipeName: "Eclairs with plum jam and Katarzynki® cream", recipeAuthor: "Agnieszka Czapska-Pruszak", recipeBlog: "www.zpamietnikapiekarnika.pl", recipeThumb: "5t", recipeImage: "5", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/7-inch cake tin/", metric: "/18-cm cake tin/",title: true))
        ingrediens.append(createIngredient(imperial: "Cake", metric: "Cake",title: true))
        ingrediens.append(createIngredient(imperial: "1 and 3/4 glass flour", metric: "1 and 3/4 glass flour"))
        ingrediens.append(createIngredient(imperial: "1.5 glass sugar", metric: "1.5 glass sugar"))
        ingrediens.append(createIngredient(imperial: "2 eggs", metric: "2 eggs"))
        ingrediens.append(createIngredient(imperial: "3/4 glass cocoa powder", metric: "3/4 glass cocoa powder"))
        ingrediens.append(createIngredient(imperial: "0.5 glass oil", metric: "0.5 glass oil"))
        ingrediens.append(createIngredient(imperial: "1 tsp baking powder", metric: "1 tsp baking powder"))
        ingrediens.append(createIngredient(imperial: "1.5 tsp baking soda", metric: "1.5 tsp baking soda"))
        ingrediens.append(createIngredient(imperial: "1 glass buttermilk", metric: "1 glass buttermilk"))
        ingrediens.append(createIngredient(imperial: "1 glass hot coffee ", metric: "1 glass hot coffee "))
        ingrediens.append(createIngredient(imperial: "0.5 tsp salt", metric: "0.5 tsp salt"))
        ingrediens.append(createIngredient(imperial: "1 tsp vanilla extract", metric: "1 tsp vanilla extract"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "Cream", metric: "Cream",title: true))
        ingrediens.append(createIngredient(imperial: "17.6 oz curd cheese", metric: "500 g curd cheese"))
        ingrediens.append(createIngredient(imperial: "3/4 glass icing sugar", metric: "3/4 glass icing sugar"))
        ingrediens.append(createIngredient(imperial: "2.8 oz white chocolate", metric: "80 g white chocolate"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "8.8 fl oz whipping cream, 30% fat", metric: "250 ml whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "3 any kinds of gingerbread from the Confectionary Factory ‘KOPERNIK’", metric: "3 any kinds of gingerbread from the Confectionary Factory ‘KOPERNIK’"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Sift the flour with cocoa powder, mix with sugar, salt, cinnamon, baking powder and baking soda. Mix the buttermilk with eggs, vanilla extract and oil. Combine the dry and wet ingredients – mix at low speed. While mixing, add coffee and mix a little longer. Pour the mixture into the cake tin laid with some baking paper. Preheat the oven to 180°C. Bake the cake for about 60 minutes checking with a stick if it is ready. Open the oven door after you have turned off the oven and let the cake cool down for several minutes. Cool the cake until it is completely cold – it is best to leave for the night.", step: "Sift the flour with cocoa powder, mix with sugar, salt, cinnamon, baking powder and baking soda. Mix the buttermilk with eggs, vanilla extract and oil. Combine the dry and wet ingredients – mix at low speed. While mixing, add coffee and mix a little longer. Pour the mixture into the cake tin laid with some baking paper. Preheat the oven to 180°C. Bake the cake for about 60 minutes checking with a stick if it is ready. Open the oven door after you have turned off the oven and let the cake cool down for several minutes. Cool the cake until it is completely cold – it is best to leave for the night."))
        methods.append(createMethodStep(imperialStep: "Cut the cooled cake into 3 parts. Prepare the cream: melt the chocolate over the hot water stirring all the time so it does not burn. Next, leave to cool for a few minutes. Cream the curd cheese with icing sugar and cinnamon. Add gradually chocolate into the curd cheese mixture and continue creaming. Pour the whipping cream into and mix until the cream is fluffy. Leave 4 tablespoons for decoration.", step: "Cut the cooled cake into 3 parts. Prepare the cream: melt the chocolate over the hot water stirring all the time so it does not burn. Next, leave to cool for a few minutes. Cream the curd cheese with icing sugar and cinnamon. Add gradually chocolate into the curd cheese mixture and continue creaming. Pour the whipping cream into and mix until the cream is fluffy. Leave 4 tablespoons for decoration."))
        methods.append(createMethodStep(imperialStep: "Spread all the parts of the cake with the cream and sprinkle each part with crumbled gingerbread. Spread the top and the sides of the cake with the cream. Put the cream cake into the fridge and for minimum 45 minutes. Prepare the glazing: heat the cream until it is hot but do not boil. Break the chocolate into cubes, add into the preheated cream and stir until the chocolate melts watching if there are no lumps. Leave to cool for about 1 minute. Take the cream cake out of the fridge and pour richly warm chocolate over. Decorate the cream cake as you wish using gingerbread, remaining cubes of chocolate, pieces of caramel topping and again, put it into the fridge for about 20 minutes.", step: "Spread all the parts of the cake with the cream and sprinkle each part with crumbled gingerbread. Spread the top and the sides of the cake with the cream. Put the cream cake into the fridge and for minimum 45 minutes. Prepare the glazing: heat the cream until it is hot but do not boil. Break the chocolate into cubes, add into the preheated cream and stir until the chocolate melts watching if there are no lumps. Leave to cool for about 1 minute. Take the cream cake out of the fridge and pour richly warm chocolate over. Decorate the cream cake as you wish using gingerbread, remaining cubes of chocolate, pieces of caramel topping and again, put it into the fridge for about 20 minutes."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 5, recipeName: "Gingerbread cream cake with curd cheese", recipeAuthor: "Agnieszka Trawińska", recipeBlog: "www.instagram.com/trawkagotuje", recipeThumb: "6t", recipeImage: "6", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "7 fl ozl whipping cream, 30% fat", metric: "200 ml whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "8.8 oz mascarpone", metric: "250 g mascarpone"))
        ingrediens.append(createIngredient(imperial: "2 tbsp icing sugar", metric: "2 tbsp icing sugar"))
        ingrediens.append(createIngredient(imperial: "1 tsp freshly grated nutmeg", metric: "1 tsp freshly grated nutmeg"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 tsp ground ginger", metric: "1 tsp ground ginger"))
        ingrediens.append(createIngredient(imperial: "1 tsp vanilla extract", metric: "1 tsp vanilla extract"))
        ingrediens.append(createIngredient(imperial: "3.5 oz raspberries", metric: "100 g raspberries"))
        ingrediens.append(createIngredient(imperial: "1 tsp cane sugar", metric: "1 tsp cane sugar"))
        ingrediens.append(createIngredient(imperial: "4 pieces Serca Toruńskie® (Torun Hearts)", metric: "4 pieces Serca Toruńskie® (Torun Hearts)"))
        ingrediens.append(createIngredient(imperial: "1 packet Pierniczki Nadziewane (Chocolate Covered Gingerbread with Filling)", metric: "1 packet Pierniczki Nadziewane (Chocolate Covered Gingerbread with Filling)"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Whip the cream in a food processor at high speed until it is firm. In the middle of mixing, add some icing sugar. Put into the fridge and cool for about 10 minutes. Meanwhile, mix the mascarpone, add all spices and vanilla extract, mix it. Gently combine the cream with mascarpone, put into the fridge and cool for at least 30 minutes.", step: "Whip the cream in a food processor at high speed until it is firm. In the middle of mixing, add some icing sugar. Put into the fridge and cool for about 10 minutes. Meanwhile, mix the mascarpone, add all spices and vanilla extract, mix it. Gently combine the cream with mascarpone, put into the fridge and cool for at least 30 minutes."))
        methods.append(createMethodStep(imperialStep: "Crumble Serca Toruńskie® into pieces of different sizes, add into the mixture just before serving and mix it gently. Heat the strawberries with a teaspoon of sugar in a saucepan with a thick bottom. When you see the juice, gently squash them with a fork and leave to simmer stirring from time to time. Serve the cream in glasses. Put Pierniczki Nadziewane on the bottom. Before serving, pour warm raspberry sauce and decorate with some frozen fruit.", step: "Crumble Serca Toruńskie® into pieces of different sizes, add into the mixture just before serving and mix it gently. Heat the strawberries with a teaspoon of sugar in a saucepan with a thick bottom. When you see the juice, gently squash them with a fork and leave to simmer stirring from time to time. Serve the cream in glasses. Put Pierniczki Nadziewane® on the bottom. Before serving, pour warm raspberry sauce and decorate with some frozen fruit."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 6, recipeName: "Spicy cream with warm raspberry sauce", recipeAuthor: "Aleksandra Kocerba", recipeBlog: "", recipeThumb: "7t", recipeImage: "7", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/approx. 8-10 servings/", metric: "/approx. 8-10 servings/"))
        ingrediens.append(createIngredient(imperial: "10.5 oz oat flakes", metric: "300 g oat flakes"))
        ingrediens.append(createIngredient(imperial: "3.5 oz hazelnuts, shelled", metric: "100 g hazelnuts, shelled"))
        ingrediens.append(createIngredient(imperial: "3.5 oz dates", metric: "100 g dates"))
        ingrediens.append(createIngredient(imperial: "2 tbsp cocoa powder", metric: "2 tbsp cocoa powder"))
        ingrediens.append(createIngredient(imperial: "1 orange", metric: "1 orange"))
        ingrediens.append(createIngredient(imperial: "5-6 pieces frosted Serduszka Toruńskie (Toruń Hearts)", metric: "5-6 pieces frosted Serduszka Toruńskie (Toruń Hearts)"))
        ingrediens.append(createIngredient(imperial: "coconut water", metric: "coconut water"))
        ingrediens.append(createIngredient(imperial: "natural yoghurt", metric: "natural yoghurt"))
        ingrediens.append(createIngredient(imperial: "fruit", metric: "fruit"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Soak the dates in boiling water for half an hour. After that time drain them off. Roast the hazelnuts on a dry frying pan until golden brown and crispy. Put 7 oz oat flakes into a bowl of a blender. Add drained dates, hazelnuts, cocoa powder, gingerbread and grated orange rind. Grind all the ingredients until they have the consistency of thick sand. Add the rest of oat flakes and mix everything with a spoon.", step: "Soak the dates in boiling water for half an hour. After that time drain them off. Roast the hazelnuts on a dry frying pan until golden brown and crispy. Put 200 g oat flakes into a bowl of a blender. Add drained dates, hazelnuts, cocoa powder, gingerbread and grated orange rind. Grind all the ingredients until they have the consistency of thick sand. Add the rest of oat flakes and mix everything with a spoon."))
        methods.append(createMethodStep(imperialStep: "Prepare porridge: put the earlier prepared mixture (5 oz-2.5 oz) into a small saucepan and pour coconut water into or usual water and your favourite milk. Cook the porridge for a few minutes until you get the required consistency. Serve with yoghurt and fruit. Keep the remaining mixture in a tightly-closed jar.", step: "Prepare porridge: put the earlier prepared mixture (60-70 g) into a small saucepan and pour coconut water into or usual water and your favourite milk. Cook the porridge for a few minutes until you get the required consistency. Serve with yoghurt and fruit. Keep the remaining mixture in a tightly-closed jar."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 7, recipeName: "Chocolate and gingerbread porridge with hazelnut and orange", recipeAuthor: "Aleksandra Obołończyk", recipeBlog: "www.olalacooking.com ", recipeThumb: "8t", recipeImage: "8", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/2 servings/", metric: "/2 servings/",title: true))
        ingrediens.append(createIngredient(imperial: "2 duck breast fillets with the skin approx. 8.8 oz", metric: "2 duck breast fillets with the skin approx. 250 g"))
        ingrediens.append(createIngredient(imperial: "1 small pak choi cabbage, thickly diced", metric: "1 small pak choi cabbage, thickly diced"))
        ingrediens.append(createIngredient(imperial: "1 small finely chopped onion", metric: "1 small finely chopped onion"))
        ingrediens.append(createIngredient(imperial: "2 finely chopped cloves garlic", metric: "2 finely chopped cloves garlic"))
        ingrediens.append(createIngredient(imperial: "1 tsp freshly grated ginger", metric: "1 tsp freshly grated ginger"))
        ingrediens.append(createIngredient(imperial: "5-6 damson plums, quartered", metric: "5-6 damson plums, quartered"))
        ingrediens.append(createIngredient(imperial: "fresh coriander", metric: "fresh coriander"))
        ingrediens.append(createIngredient(imperial: "Sauce", metric: "Sauce",title: true))
        ingrediens.append(createIngredient(imperial: "3 tbsp sauce hoisin", metric: "3 tbsp sauce hoisin"))
        ingrediens.append(createIngredient(imperial: "3 tbsp light soy sauce", metric: "3 tbsp light soy sauce"))
        ingrediens.append(createIngredient(imperial: "5 tbsp finely ground Katarzynki® without chocolate", metric: "5 tbsp finely ground Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "1 tbsp lemon juice", metric: "1 tbsp lemon juice"))
        ingrediens.append(createIngredient(imperial: "5-6 tbsp water", metric: "5-6 tbsp water"))
        ingrediens.append(createIngredient(imperial: "1.5 tsp freshly grated ginger", metric: "1.5 tsp freshly grated ginger"))
        ingrediens.append(createIngredient(imperial: "2 tbsp homemade plum jam", metric: "2 tbsp homemade plum jam"))
        ingrediens.append(createIngredient(imperial: "0.5 tsp finely chopped chilli", metric: "0.5 tsp finely chopped chilli"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Start with preparing the sauce. Put Katarzynki® into a bowl of a blender and grind into powder. Put the ingredients for the sauce into a saucepan and cook at low heat until the ingredients combine, and the sauce slightly thickens. If it is too thick, add more water. Put the sauce aside. Season the duck with salt and pepper. Cut the skin with a sharp knife every half centimetre but be careful not to cut the meat.", step: "Start with preparing the sauce. Put Katarzynki® into a bowl of a blender and grind into powder. Put the ingredients for the sauce into a saucepan and cook at low heat until the ingredients combine, and the sauce slightly thickens. If it is too thick, add more water. Put the sauce aside. Season the duck with salt and pepper. Cut the skin with a sharp knife every half centimetre but be careful not to cut the meat."))
        methods.append(createMethodStep(imperialStep: "Put the duck upside down, so the skin touches the bottom of a wok and cook at low heat until the fat of the skin melts and it becomes golden brown and crispy. Turn around the fillets and fry for next 3-4 minutes. Put the duck on the plate and pour out extra fat leaving only a tablespoon on the bottom. Put chopped onion, garlic, ginger on the hot fat and fry for a while. Add diced pak choi cabbage and fry.", step: "Put the duck upside down, so the skin touches the bottom of a wok and cook at low heat until the fat of the skin melts and it becomes golden brown and crispy. Turn around the fillets and fry for next 3-4 minutes. Put the duck on the plate and pour out extra fat leaving only a tablespoon on the bottom. Put chopped onion, garlic, ginger on the hot fat and fry for a while. Add diced pak choi cabbage and fry."))
        methods.append(createMethodStep(imperialStep: "Pour the sauce over the vegetables and mix everything. Add fresh damson plums. Slice the duck with a sharp knife and put it on the sauce for a short time to warm up and cook inside if it is to rare. Garnish the dish with fresh coriander and chilli. Serve with cooked jasmine rice.", step: "Pour the sauce over the vegetables and mix everything. Add fresh damson plums. Slice the duck with a sharp knife and put it on the sauce for a short time to warm up and cook inside if it is to rare. Garnish the dish with fresh coriander and chilli. Serve with cooked jasmine rice."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 8, recipeName: "Duck in gingerbread hoisin sauce with plums", recipeAuthor: "Aleksandra Obołończyk", recipeBlog: "www.olalacooking.com ", recipeThumb: "9t", recipeImage: "9", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "3 eggs", metric: "3 eggs"))
        ingrediens.append(createIngredient(imperial: "1.4 oz melted butter", metric: "40 g melted butter"))
        ingrediens.append(createIngredient(imperial: "2.8 oz flour", metric: "80 g flour"))
        ingrediens.append(createIngredient(imperial: "3 pieces Serca Toruńskie® (Torun Hearts)", metric: "3 pieces Serca Toruńskie® (Torun Hearts)"))
        ingrediens.append(createIngredient(imperial: "1 glass blueberries", metric: "1 glass blueberries"))
        ingrediens.append(createIngredient(imperial: "2 tsp vanilla sugar", metric: "2 tsp vanilla sugar"))
        ingrediens.append(createIngredient(imperial: "1/2 glass blueberries", metric: "1/2 glass blueberries"))
        ingrediens.append(createIngredient(imperial: "handful chopped walnuts", metric: "handful chopped walnuts"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Melt the butter and cool down. Crumble one of the Serca Toruńskie®. Separate yolks from whites. Beat the whites until firm. Beat the yolks with sugar into smooth fluffy mixture. Add cooled butter, vanilla sugar, crumbled Serca Toruńskie® and flour into yolks. Stir with a spoon to combine the ingredients. Add the whites into the mixture and stir gently. Butter a frying pan. Pour the mixture into the frying pan, add two Serca Toruńskie® and blueberries on top. Cover the frying pan tightly with a pan lid and fry for about 10 minutes at low heat. Sprinkle with blueberries and walnuts.", step: "Melt the butter and cool down. Crumble one of the Serca Toruńskie®. Separate yolks from whites. Beat the whites until firm. Beat the yolks with sugar into smooth fluffy mixture. Add cooled butter, vanilla sugar, crumbled Serca Toruńskie® and flour into yolks. Stir with a spoon to combine the ingredients. Add the whites into the mixture and stir gently. Butter a frying pan. Pour the mixture into the frying pan, add two Serca Toruńskie® and blueberries on top. Cover the frying pan tightly with a pan lid and fry for about 10 minutes at low heat. Sprinkle with blueberries and walnuts."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 9, recipeName: "Fried sponge cake from Heart", recipeAuthor: "Aneta Gwiner", recipeBlog: "www.kuchniawoparach.pl", recipeThumb: "10t", recipeImage: "10", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "9.5 oz wheat flour", metric: "270 g wheat flour"))
        ingrediens.append(createIngredient(imperial: "1.8 oz icing sugar", metric: "50 g icing sugar"))
        ingrediens.append(createIngredient(imperial: "7 oz cold butter", metric: "200 g cold butter"))
        ingrediens.append(createIngredient(imperial: "1 egg", metric: "1 egg"))
        ingrediens.append(createIngredient(imperial: "35 oz plums", metric: "1 kg plums"))
        ingrediens.append(createIngredient(imperial: "5.3 oz Pierniczki Nadziewane (Chocolate Covered Gingerbread with Filling)", metric: "150 g Pierniczki Nadziewane (Chocolate Covered Gingerbread with Filling)"))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate", metric: "100 g dark chocolate"))
        ingrediens.append(createIngredient(imperial: "3 tbsp potato flour", metric: "3 tbsp potato flour"))
        ingrediens.append(createIngredient(imperial: "3 tbsp cane sugar", metric: "3 tbsp cane sugar"))
        ingrediens.append(createIngredient(imperial: "1 egg to apply on top of the dough", metric: "1 egg to apply on top of the dough"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Sift the flour with icing sugar onto a pastry board, break the egg and add diced cold butter. Knead all ingredients. Flatten the dough, wrap in cling foil and put aside for minimum half an hour into the fridge. Preheat the oven to 392ºF without a fan. Cut the gingerbread into pieces, cut the chocolate into pieces. Put aside.", step: "Sift the flour with icing sugar onto a pastry board, break the egg and add diced cold butter. Knead all ingredients. Flatten the dough, wrap in cling foil and put aside for minimum half an hour into the fridge. Preheat the oven to 200°C without a fan. Cut the gingerbread into pieces, cut the chocolate into pieces. Put aside."))
        methods.append(createMethodStep(imperialStep: "Wash the plums, cut into quarters, get rid of stones. Put the plums into a bowl, sprinkle with potato flour and sugar, mix it. Add the earlier cut gingerbread and chocolate into the bowl and mix it. Put aside. Sprinkle the cooled dough a little with some flour and pin-out to the size of 0.20 inch, best to do it straight on the baking paper.", step: "Wash the plums, cut into quarters, get rid of stones. Put the plums into a bowl, sprinkle with potato flour and sugar, mix it. Add the earlier cut gingerbread and chocolate into the bowl and mix it. Put aside. Sprinkle the cooled dough a little with some flour and pin-out to the size of 5 mm, best to do it straight on the baking paper."))
        methods.append(createMethodStep(imperialStep: "Put the baking paper with the dough on the baking tray. Sprinkle the centre of the dough with some potato flour. Put the plums mixed with gingerbread and chocolate on top and leave approximately a 0.30-inch gap on the edges. Fold the edges of the dough over the stuffing and spread some whisked egg on top. Put the rustic tart into the preheated oven for about 35 minutes. Serve the gallete, this is another name for a rustic tart, sprinkled with grated chocolate.", step: "Put the baking paper with the dough on the baking tray. Sprinkle the centre of the dough with some potato flour. Put the plums mixed with gingerbread and chocolate on top and leave approximately a 7-centimetre gap on the edges. Fold the edges of the dough over the stuffing and spread some whisked egg on top. Put the rustic tart into the preheated oven for about 35 minutes. Serve the gallete, this is another name for a rustic tart, sprinkled with grated chocolate."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 10, recipeName: "Rustic plum and gingerbread tart", recipeAuthor: "Aniela Straszewska", recipeBlog: "www.fotografia.kawaiczekolada.pl", recipeThumb: "11t", recipeImage: "11", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "2 duck breasts", metric: "2 duck breasts"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp cumin", metric: "1/2 tsp cumin"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp coriander seeds", metric: "1/2 tsp coriander seeds"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp cinnamon", metric: "1/2 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp ground ginger", metric: "1/2 tsp ground ginger"))
        ingrediens.append(createIngredient(imperial: "1 pinch nutmeg", metric: "1 pinch nutmeg"))
        ingrediens.append(createIngredient(imperial: "1 pinch star anise", metric: "1 pinch star anise"))
        ingrediens.append(createIngredient(imperial: "2 cloves crushed in a mortar", metric: "2 cloves crushed in a mortar"))
        ingrediens.append(createIngredient(imperial: "1 tsp honey", metric: "1 tsp honey"))
        ingrediens.append(createIngredient(imperial: "1 pinch chilli flakes", metric: "1 pinch chilli flakes"))
        ingrediens.append(createIngredient(imperial: "2 beaten whites", metric: "2 beaten whites"))
        ingrediens.append(createIngredient(imperial: "1 tsp soy sauce", metric: "1 tsp soy sauce"))
        ingrediens.append(createIngredient(imperial: "1 clove garlic", metric: "1 clove garlic"))
        ingrediens.append(createIngredient(imperial: "1 tsp salt", metric: "1 tsp salt"))
        ingrediens.append(createIngredient(imperial: "1 piece Katarzynki® without chocolate /finely grated/", metric: "1 piece Katarzynki® without chocolate /finely grated/"))
        ingrediens.append(createIngredient(imperial: "Chutney", metric: "Chutney",title: true))
        ingrediens.append(createIngredient(imperial: "8.8 oz damson plums", metric: "250 g damson plums"))
        ingrediens.append(createIngredient(imperial: "pinch cinnamon and star anise", metric: "pinch cinnamon and star anise"))
        ingrediens.append(createIngredient(imperial: "2 tbsp wine vinegar", metric: "2 tbsp wine vinegar"))
        ingrediens.append(createIngredient(imperial: "Mustard and sesame sauce", metric: "Mustard and sesame sauce",title: true))
        ingrediens.append(createIngredient(imperial: "2 tbsp mustard", metric: "2 tbsp mustard"))
        ingrediens.append(createIngredient(imperial: "apple vinegar", metric: "apple vinegar"))
        ingrediens.append(createIngredient(imperial: "1 tbsp honey", metric: "1 tbsp honey"))
        ingrediens.append(createIngredient(imperial: "2 tsp tahini", metric: "2 tsp tahini"))
        ingrediens.append(createIngredient(imperial: "pepper or pinch star anise", metric: "pepper or pinch star anise"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Mince the meat with the skin. Season with salt, soy sauce, cinnamon, cloves, star anise, chilli. Add grated Katarzynki®, crushed garlic and beaten eggs. Form small meatballs with your moistened hands. Fry the meatballs in hot rapeseed oil. At the end of frying, pour some honey and caramelize for a while. Drain the meatballs on the kitchen roll.", step: "Mince the meat with the skin. Season with salt, soy sauce, cinnamon, cloves, star anise, chilli. Add grated Katarzynki®, crushed garlic and beaten eggs. Form small meatballs with your moistened hands. Fry the meatballs in hot rapeseed oil. At the end of frying, pour some honey and caramelize for a while. Drain the meatballs on the kitchen roll."))
        methods.append(createMethodStep(imperialStep: "Prepare the chutney: stone the plums, put into the saucepan, add seasoning and pour some water. Cover and stew until tender. At the end of stewing, add wine vinegar and reduce the juice. Mix until the mixture is smooth. Prepare the mustard and sesame sauce: combine all the ingredients into smooth mixture.", step: "Prepare the chutney: stone the plums, put into the saucepan, add seasoning and pour some water. Cover and stew until tender. At the end of stewing, add wine vinegar and reduce the juice. Mix until the mixture is smooth. Prepare the mustard and sesame sauce: combine all the ingredients into smooth mixture."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 11, recipeName: "Meatballs made from duck breast with gingerbread, plum chutney, mustard and sesame dip", recipeAuthor: "Anita Zegadło", recipeBlog: "www.azgotuj.blogspot.com", recipeThumb: "12t", recipeImage: "12", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "4 yolks", metric: "4 yolks"))
        ingrediens.append(createIngredient(imperial: "1/3 glass icing cane sugar", metric: "1/3 glass icing cane sugar"))
        ingrediens.append(createIngredient(imperial: "10.6 fl oz whipping cream, 30% fat", metric: "300 ml whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "6 pieces Katarzynki® without chocolate ", metric: "6 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "1 glass fresh diced pumpkin", metric: "1 glass fresh diced pumpkin"))
        ingrediens.append(createIngredient(imperial: "1/2 glass sweetened condensed milk", metric: "1/2 glass sweetened condensed milk"))
        ingrediens.append(createIngredient(imperial: "1/2 glass candied orange rind", metric: "1/2 glass candied orange rind"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp cinnamon", metric: "1/2 tsp cinnamon"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Put the pumpkin into a saucepan, pour the condensed milk and cook over low heat for about 20 minutes. During that time the pumpkin will go soft and start to fall apart. Cool down and blend it into purée. Put the yolks into a big bowl, add half portion of icing sugar. Put the bowl onto a pan with boiling water and whisk. Yolks should triple their volume. The mixture must be glossy and dense. Cool down the readymade mixture.", step: "Put the pumpkin into a saucepan, pour the condensed milk and cook over low heat for about 20 minutes. During that time the pumpkin will go soft and start to fall apart. Cool down and blend it into purée. Put the yolks into a big bowl, add half portion of icing sugar. Put the bowl onto a pan with boiling water and whisk. Yolks should triple their volume. The mixture must be glossy and dense. Cool down the readymade mixture."))
        methods.append(createMethodStep(imperialStep: "Whip the cooled cream with the remaining sugar until firm. Add the egg mixture into the cream and mix gently. Add the pumpkin purée, orange rind, cinnamon and crumbled Katarzynki® into the mixture. Pour it into a container and put into the freezer for at least 6 hours. Serve the readymade semifreddo with some maple syrup and orange rind.", step: "Whip the cooled cream with the remaining sugar until firm. Add the egg mixture into the cream and mix gently. Add the pumpkin purée, orange rind, cinnamon and crumbled Katarzynki® into the mixture. Pour it into a container and put into the freezer for at least 6 hours. Serve the readymade semifreddo with some maple syrup and orange rind."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 12, recipeName: "Ice semifreddo with pumpkin, orange rind and Katarzynki®", recipeAuthor: "Anna Łukasiewicz", recipeBlog: "www.piatapopoludniu.blogspot.com", recipeThumb: "13t", recipeImage: "13", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/7 servings/", metric: "/7 servings/"))
        ingrediens.append(createIngredient(imperial: "7 medium apples", metric: "7 medium apples"))
        ingrediens.append(createIngredient(imperial: "5.3 oz smoked bacon", metric: "150 g smoked bacon"))
        ingrediens.append(createIngredient(imperial: "1 medium onion", metric: "1 medium onion"))
        ingrediens.append(createIngredient(imperial: "1 large clove garlic", metric: "1 large clove garlic"))
        ingrediens.append(createIngredient(imperial: "3.5 oz fresh or frozen blackcurrants", metric: "100 g fresh or frozen blackcurrants"))
        ingrediens.append(createIngredient(imperial: "6 pieces Katarzynki® without chocolate ", metric: "6 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "bunch fresh herbs (rosemary, sage and marjoram)", metric: "bunch fresh herbs (rosemary, sage and marjoram)"))
        ingrediens.append(createIngredient(imperial: "2 tbsp balsamic vinegar", metric: "2 tbsp balsamic vinegar"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Preheat the oven to 374ºF. Meanwhile, cut the tops of the apples (parts with petioles) and put aside. Core the apples for the stuffing being careful not to cut the skin. Put the cores aside. Fry the diced bacon in a dry frying pan. When the fat melts, add sliced onion. Fry until the onion is tender, and then add chopped garlic, peeled blackcurrants, handful of cores and chopped herbs into the frying pan. Fry everything.", step: "Preheat the oven to 190°C. Meanwhile, cut the tops of the apples (parts with petioles) and put aside. Core the apples for the stuffing being careful not to cut the skin. Put the cores aside. Fry the diced bacon in a dry frying pan. When the fat melts, add sliced onion. Fry until the onion is tender, and then add chopped garlic, peeled blackcurrants, handful of cores and chopped herbs into the frying pan. Fry everything."))
        methods.append(createMethodStep(imperialStep: "Crumble the gingerbread in a food processor into thick crumbs, and then add it also into the frying pan. Fry the stuffing a bit longer until all the ingredients are combined. At the end, season with balsamic vinegar, salt and pepper. Stuff all the apples, put them on a baking tray laid with some baking paper and put into the preheated oven. Bake for about 25-30 minutes until the apples are soft and golden brown.", step: "Crumble the gingerbread in a food processor into thick crumbs, and then add it also into the frying pan. Fry the stuffing a bit longer until all the ingredients are combined. At the end, season with balsamic vinegar, salt and pepper. Stuff all the apples, put them on a baking tray laid with some baking paper and put into the preheated oven. Bake for about 25-30 minutes until the apples are soft and golden brown."))

        
        // ---- recipe
        recipe = Recipe(recipeID: 13, recipeName: "Apples baked with gingerbread, bacon, blackcurrants and herbs", recipeAuthor: "Anna Witkiewicz", recipeBlog: "www.everydayflavours.blogspot.com", recipeThumb: "14t", recipeImage: "14", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "35 oz pumpkin, peeled and cut into pieces", metric: "1 kg pumpkin, peeled and cut into pieces"))
        ingrediens.append(createIngredient(imperial: "1 onion", metric: "1 onion"))
        ingrediens.append(createIngredient(imperial: "1 carrot", metric: "1 carrot"))
        ingrediens.append(createIngredient(imperial: "3 pieces Katarzynki® without chocolate ", metric: "3 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "1 tbsp clarified butter", metric: "1 tbsp clarified butter"))
        ingrediens.append(createIngredient(imperial: "10 coriander seeds", metric: "10 coriander seeds"))
        ingrediens.append(createIngredient(imperial: "pinch cumin", metric: "pinch cumin"))
        ingrediens.append(createIngredient(imperial: "1 bay leaf", metric: "1 bay leaf"))
        ingrediens.append(createIngredient(imperial: "2 glasses vegetable stock", metric: "2 glasses vegetable stock"))
        ingrediens.append(createIngredient(imperial: "1 tbsp clarified butter", metric: "1 tbsp clarified butter"))
        ingrediens.append(createIngredient(imperial: "1/4 small cauliflower", metric: "1/4 small cauliflower"))
        ingrediens.append(createIngredient(imperial: "crumbled and baked Katarzynki® without chocolate", metric: "crumbled and baked Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "fresh lovage leaves ", metric: "fresh lovage leaves "))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Heat the clarified butter in a saucepan. Add the seasoning: coriander, cumin and bay leaf, fry for a while until you can smell the aroma of the seasoning. Add the vegetables cut into pieces. Fry the vegetables stirring with a spoon for about 10 minutes, and then, add the gingerbread, pour the vegetable stock and cook until tender. Take the bay leaf out and mix everything into smooth cream soup.", step: "Heat the clarified butter in a saucepan. Add the seasoning: coriander, cumin and bay leaf, fry for a while until you can smell the aroma of the seasoning. Add the vegetables cut into pieces. Fry the vegetables stirring with a spoon for about 10 minutes, and then, add the gingerbread, pour the vegetable stock and cook until tender. Take the bay leaf out and mix everything into smooth cream soup."))
        methods.append(createMethodStep(imperialStep: "Slice the cauliflower or divide into small pieces. Heat the butter in the frying pan and fry the pieces of cauliflower until golden brown. Crumble the gingerbread and bake in a dry frying pan. Pour the soup into bowls, serve with the pieces of cauliflower, sprinkled gingerbread and garnished with fresh leaves of lovage.", step: "Slice the cauliflower or divide into small pieces. Heat the butter in the frying pan and fry the pieces of cauliflower until golden brown. Crumble the gingerbread and bake in a dry frying pan. Pour the soup into bowls, serve with the pieces of cauliflower, sprinkled gingerbread and garnished with fresh leaves of lovage."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 14, recipeName: "Pumpkin cream with baked cauliflower and gingerbread bite", recipeAuthor: "Arkadiusz Czapski-Pruszak", recipeBlog: "", recipeThumb: "15t", recipeImage: "15", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "a few pieces Serca Toruńskie® (Torun Hearts)", metric: "a few pieces Serca Toruńskie® (Torun Hearts)"))
        ingrediens.append(createIngredient(imperial: "wholemeal baguette", metric: "wholemeal baguette"))
        ingrediens.append(createIngredient(imperial: "Ricotta cheese", metric: "Ricotta cheese"))
        ingrediens.append(createIngredient(imperial: "a few blackberries", metric: "a few blackberries"))
        ingrediens.append(createIngredient(imperial: "3 figs", metric: "3 figs"))
        ingrediens.append(createIngredient(imperial: "honey", metric: "honey"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Cut the baguette into pieces and bake for a moment in the oven or in a toaster until it is crusty. Spread every piece with thick layer of Ricotta cheese, put the slices of figs and a few blackberries on top, pour richly with honey and sprinkle the crumbled gingerbread. After baking, you can also spread the toast with some butter.", step: "Cut the baguette into pieces and bake for a moment in the oven or in a toaster until it is crusty. Spread every piece with thick layer of Ricotta cheese, put the slices of figs and a few blackberries on top, pour richly with honey and sprinkle the crumbled gingerbread. After baking, you can also spread the toast with some butter."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 15, recipeName: "Fruit toast with a sprinkle of Toruń Gingerbread and honey", recipeAuthor: "Bogumiła Szymańska", recipeBlog: "www.slodkiezycie.com", recipeThumb: "16t", recipeImage: "16", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/5 three-inch tartlets/", metric: "/5 eight-centimetre tartlets/",title: true))
        ingrediens.append(createIngredient(imperial: "4 pieces Katarzynki® VEGAN", metric: "4 pieces Katarzynki® VEGAN"))
        ingrediens.append(createIngredient(imperial: "4 tbsp coconut oil", metric: "4 tbsp coconut oil"))
        ingrediens.append(createIngredient(imperial: "2 tbsp maple syrup", metric: "2 tbsp maple syrup"))
        ingrediens.append(createIngredient(imperial: "approx. 5.3 oz sweet potato", metric: "approx. 150 g sweet potato"))
        ingrediens.append(createIngredient(imperial: "5.3 oz dark chocolate", metric: "150 g dark chocolate"))
        ingrediens.append(createIngredient(imperial: "2 tbsp peanut butter", metric: "2 tbsp peanut butter"))
        ingrediens.append(createIngredient(imperial: "1/2 glass coconut milk", metric: "1/2 glass coconut milk"))
        ingrediens.append(createIngredient(imperial: "1/3 glass maple syrup", metric: "1/3 glass maple syrup"))
        ingrediens.append(createIngredient(imperial: "fresh fruit: figs, raspberries, blackberries", metric: "fresh fruit: figs, raspberries, blackberries"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the Katarzynki® VEGAN using a blender, add coconut oil and maple syrup. Mix until it resembles soggy sand. Lay the sides and the bottom of the tartlet tins with the gingerbread pressing precisely. Put the tartlets into the fridge for about 2 hours. Wrap the sweet potatoes in some tinfoil and bake at 200°C for about an hour. Put aside.", step: "Crumble the Katarzynki® VEGAN using a blender, add coconut oil and maple syrup. Mix until it resembles soggy sand. Lay the sides and the bottom of the tartlet tins with the gingerbread pressing precisely. Put the tartlets into the fridge for about 2 hours. Wrap the sweet potatoes in some tinfoil and bake at 200°C for about an hour. Put aside."))
        methods.append(createMethodStep(imperialStep: "Put the baked and cooled sweet potato, peanut butter and milk into a bowl of the blender. Mix until you get the smooth mixture. Add chocolate melted over hot water and mix again. Next, add maple syrup to get required sweetness. Take the gingerbread bases out from the tartlet tins carefully. Put the chocolate mousse on the cooled tartlets and put them into the fridge again. After 2 hours, you can start decorating the tartlets. Decorate them with fresh fruit the way you like.", step: "Put the baked and cooled sweet potato, peanut butter and milk into a bowl of the blender. Mix until you get the smooth mixture. Add chocolate melted over hot water and mix again. Next, add maple syrup to get required sweetness. Take the gingerbread bases out from the tartlet tins carefully. Put the chocolate mousse on the cooled tartlets and put them into the fridge again. After 2 hours, you can start decorating the tartlets. Decorate them with fresh fruit the way you like."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 16, recipeName: "Vegan tartlets with chocolate mousse on a sweet potato base", recipeAuthor: "Bogumiła Szymańska", recipeBlog: "www.slodkiezycie.com", recipeThumb: "17t", recipeImage: "17", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a 8-inch cake tin/", metric: "/a 20-centimetre cake tin/",title: true))
        ingrediens.append(createIngredient(imperial: "Base", metric: "Base",title: true))
        ingrediens.append(createIngredient(imperial: "1 packet Uszatki®", metric: "1 packet Uszatki®"))
        ingrediens.append(createIngredient(imperial: "3.5 fl oz canned coconut milk /the liquid part/ ", metric: "100 ml canned coconut milk /the liquid part/ "))
        ingrediens.append(createIngredient(imperial: "2 tbsp coconut oil", metric: "2 tbsp coconut oil"))
        ingrediens.append(createIngredient(imperial: "Mixture", metric: "Mixture",title: true))
        ingrediens.append(createIngredient(imperial: "3 cubes natural tofu (6.3 ounces)", metric: "3 cubes natural tofu (180 grams)"))
        ingrediens.append(createIngredient(imperial: "5.3 oz xylitol", metric: "150 gr xylitol"))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate, containing 80% cocoa", metric: "100 gr dark chocolate, containing 80% cocoa"))
        ingrediens.append(createIngredient(imperial: "2 tbsp potato flour ", metric: "2 tbsp potato flour "))
        ingrediens.append(createIngredient(imperial: "juice from one lemon", metric: "juice from one lemon"))
        ingrediens.append(createIngredient(imperial: "solid part of coconut milk ", metric: "solid part of coconut milk "))
        ingrediens.append(createIngredient(imperial: "Glazing", metric: "Glazing",title: true))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate ", metric: "100 gr dark chocolate "))
        ingrediens.append(createIngredient(imperial: "1.8 fl oz any vegetable milk", metric: "50 ml any vegetable milk"))
        ingrediens.append(createIngredient(imperial: "For decoration", metric: "For decoration",title: true))
        ingrediens.append(createIngredient(imperial: "Uszatki® or Katarzynki® in chocolate", metric: "Uszatki® or Katarzynki® in chocolate"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the Uszatki® in a food processor into flour. Add the liquid part of the coconut milk and 2 tablespoons of coconut oil. Mix and put the mixture into the cake tin laid with some baking paper. Put the base of Tofurnik into the fridge for about 30 minutes. Melt the chocolate over hot water. Drain the tofu, add lemon juice, the solid part of coconut milk, xylitol, melted chocolate and potato flour. Mix the mixture precisely – it must be smooth and with no lumps. Pour it on the base. Bake at 302ºF for about 40 minutes. Check with a stick if Tofurnik is baked. Prepare the glazing: melt the chocolate with vegetable milk. Pour over the cooled Tofurnik.", step: "Crumble the Uszatki® in a food processor into flour. Add the liquid part of the coconut milk and 2 tablespoons of coconut oil. Mix and put the mixture into the cake tin laid with some baking paper. Put the base of Tofurnik into the fridge for about 30 minutes. Melt the chocolate over hot water. Drain the tofu, add lemon juice, the solid part of coconut milk, xylitol, melted chocolate and potato flour. Mix the mixture precisely – it must be smooth and with no lumps. Pour it on the base. Bake at 150°C for about 40 minutes. Check with a stick if Tofurnik is baked. Prepare the glazing: melt the chocolate with vegetable milk. Pour over the cooled Tofurnik."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 17, recipeName: "Gingerbread Tofurnik", recipeAuthor: "Emilia Konkol", recipeBlog: "www.konkolpolny.pl", recipeThumb: "18t", recipeImage: "18", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "7 oz baked sweet potatoes", metric: "200 g baked sweet potatoes"))
        ingrediens.append(createIngredient(imperial: "14 fl oz coconut milk ", metric: "400 ml coconut milk "))
        ingrediens.append(createIngredient(imperial: "10.6 oz frozen fruit: raspberries, redcurrants, blackcurrants", metric: "300 g frozen fruit: raspberries, redcurrants, blackcurrants"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp vanilla extract", metric: "1/2 tsp vanilla extract"))
        ingrediens.append(createIngredient(imperial: "1/3 tsp salt", metric: "1/3 tsp salt"))
        ingrediens.append(createIngredient(imperial: "a few packets Katarzynki® without chocolate or Katarzynki® VEGAN", metric: "a few packets Katarzynki® without chocolate or Katarzynki® VEGAN"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Peel sweet potatoes and all the ingredients, apart from fruit, put into a small saucepan. Heat until the ingredients melt. Blend and put aside to cool down. Add frozen fruit into the cooled mixture. Blend it. Pour the mixture into a plastic box and put into the freezer for 3 hours mixing precisely every 30 minutes. Serve with Katarzynki® or Katarzynki® VEGAN.", step: "Peel sweet potatoes and all the ingredients, apart from fruit, put into a small saucepan. Heat until the ingredients melt. Blend and put aside to cool down. Add frozen fruit into the cooled mixture. Blend it. Pour the mixture into a plastic box and put into the freezer for 3 hours mixing precisely every 30 minutes. Serve with Katarzynki® or Katarzynki® VEGAN."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 18, recipeName: "Icy Katarzynka", recipeAuthor: "Ewelina Jaroszczuk", recipeBlog: "www.instagram.com/wszechlinka", recipeThumb: "19t", recipeImage: "19", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "Dumplings", metric: "Dumplings",title: true))
        ingrediens.append(createIngredient(imperial: "8.8 oz curd cheese", metric: "250 g curd cheese"))
        ingrediens.append(createIngredient(imperial: "3/4 glass spelt flour", metric: "3/4 glass spelt flour"))
        ingrediens.append(createIngredient(imperial: "2 pieces Katarzynki® in chocolate ", metric: "2 pieces Katarzynki® in chocolate "))
        ingrediens.append(createIngredient(imperial: "1 large egg", metric: "1 large egg"))
        ingrediens.append(createIngredient(imperial: "salt", metric: "salt"))
        ingrediens.append(createIngredient(imperial: "Plum sauce", metric: "Plum sauce",title: true))
        ingrediens.append(createIngredient(imperial: "15 damson plums", metric: "15 damson plums"))
        ingrediens.append(createIngredient(imperial: "1 tbsp butter", metric: "1 tbsp butter"))
        ingrediens.append(createIngredient(imperial: "1 tbsp muscovado sugar", metric: "1 tbsp muscovado sugar"))
        ingrediens.append(createIngredient(imperial: "3 tbsp brandy", metric: "3 tbsp brandy"))
        ingrediens.append(createIngredient(imperial: "2 pieces Katarzynki® in chocolate", metric: "2 pieces Katarzynki® in chocolate"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the curd cheese with a fork. Grind the gingerbread with spelt flour and salt into powder – add into the curd cheese together with the egg and knead smooth and firm dough. Wrap the dough in cling foil and put into the fridge for 1 hour. Halve the plums, get rid of stones. Melt the butter in the frying pan, add some sugar and plums. After short frying, pour brandy. Cook until it thickens. Crumble the gingerbread in chocolate into the frying pan.", step: "Crumble the curd cheese with a fork. Grind the gingerbread with spelt flour and salt into powder – add into the curd cheese together with the egg and knead smooth and firm dough. Wrap the dough in cling foil and put into the fridge for 1 hour. Halve the plums, get rid of stones. Melt the butter in the frying pan, add some sugar and plums. After short frying, pour brandy. Cook until it thickens. Crumble the gingerbread in chocolate into the frying pan."))
        methods.append(createMethodStep(imperialStep: "Take the dough out of the fridge, divide into balls a little bigger than walnuts. Flatten every ball gently and make a dimple in the middle. Cook the dumplings in salted water for about 5 minutes. Put them on the plate. Serve it warm with the plum sauce.", step: "Take the dough out of the fridge, divide into balls a little bigger than walnuts. Flatten every ball gently and make a dimple in the middle. Cook the dumplings in salted water for about 5 minutes. Put them on the plate. Serve it warm with the plum sauce."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 19, recipeName: "Gingerbread dumplings with plum sauce", recipeAuthor: "Iwona Jakubowska", recipeBlog: "www.najedzeni.blogspot.com", recipeThumb: "20t", recipeImage: "20", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "Brownie", metric: "Brownie",title: true))
        ingrediens.append(createIngredient(imperial: "frosted orange Serca Toruńskie®", metric: "frosted orange Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "a bar of chocolate", metric: "a bar of chocolate"))
        ingrediens.append(createIngredient(imperial: "2 glasses sponge flour", metric: "2 glasses sponge flour"))
        ingrediens.append(createIngredient(imperial: "1 and 1/2 glass baked pumpkin purée ", metric: "1 and 1/2 glass baked pumpkin purée "))
        ingrediens.append(createIngredient(imperial: "1/2 glass sugar", metric: "1/2 glass sugar"))
        ingrediens.append(createIngredient(imperial: "2 eggs", metric: "2 eggs"))
        ingrediens.append(createIngredient(imperial: "1 glass natural yoghurt", metric: "1 glass natural yoghurt"))
        ingrediens.append(createIngredient(imperial: "1/4 glass oil", metric: "1/4 glass oil"))
        ingrediens.append(createIngredient(imperial: "1/4 glass melted butter", metric: "1/4 glass melted butter"))
        ingrediens.append(createIngredient(imperial: "Pumpkin mixture", metric: "Pumpkin mixture",title: true))
        ingrediens.append(createIngredient(imperial: "1 glass pumpkin purée", metric: "1 glass pumpkin purée"))
        ingrediens.append(createIngredient(imperial: "8.8 oz mascarpone cheese", metric: "250 g mascarpone cheese"))
        ingrediens.append(createIngredient(imperial: "7 fl oz whipping cream, 30% fat", metric: "200 ml whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "colourless jelly", metric: "colourless jelly"))
        ingrediens.append(createIngredient(imperial: "5 tbsp icing sugar", metric: "5 tbsp icing sugar"))
        ingrediens.append(createIngredient(imperial: "Extra", metric: "Extra",title: true))
        ingrediens.append(createIngredient(imperial: "a bar of chocolate", metric: "a bar of chocolate"))
        ingrediens.append(createIngredient(imperial: "sugar for caramel", metric: "sugar for caramel"))
        ingrediens.append(createIngredient(imperial: "fruit for decoration", metric: "fruit for decoration"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Whisk the eggs with sugar into the smooth mixture. Add yoghurt, melted butter, oil, pumpkin purée and melted cooled chocolate. Add some crumbled Serca Toruńskie® into the flour. Mix everything and bake at 356°F for 30 minutes. Cool the cake down. Whip cooled whipping cream, add cold mascarpone cheese, icing sugar, pumpkin puree and stiffening jelly dissolved in a half glass of water. Pour the mixture on brownie and cool for over 1 hour. Pour melted chocolate over the cake and decorate with caramel – sugar melted in a hot frying pan and poured in different shapes on the baking paper – with some gingerbread and fruit.", step: "Whisk the eggs with sugar into the smooth mixture. Add yoghurt, melted butter, oil, pumpkin purée and melted cooled chocolate. Add some crumbled Serca Toruńskie® into the flour. Mix everything and bake at 180°C for 30 minutes. Cool the cake down. Whip cooled whipping cream, add cold mascarpone cheese, icing sugar, pumpkin puree and stiffening jelly dissolved in a half glass of water. Pour the mixture on brownie and cool for over 1 hour. Pour melted chocolate over the cake and decorate with caramel – sugar melted in a hot frying pan and poured in different shapes on the baking paper – with some gingerbread and fruit."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 20, recipeName: "Gingerbread, pumpkin and chocolate cream cake with mascarpone froth with pumpkin", recipeAuthor: "Izabela Michalczyk", recipeBlog: "", recipeThumb: "21t", recipeImage: "21", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "1 glass wheat flour", metric: "1 glass wheat flour"))
        ingrediens.append(createIngredient(imperial: "5.3 oz Katarzynki® without chocolate", metric: "150 g Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "2 eggs /size L/", metric: "2 eggs /size L/"))
        ingrediens.append(createIngredient(imperial: "3.5 oz melted butter", metric: "100 g melted butter"))
        ingrediens.append(createIngredient(imperial: "1.5 tsp baking powder", metric: "1.5 tsp baking powder"))
        ingrediens.append(createIngredient(imperial: "approx.1 glass milk", metric: "approx.1 glass milk"))
        ingrediens.append(createIngredient(imperial: "3-4 pears", metric: "3-4 pears"))
        ingrediens.append(createIngredient(imperial: "approx. 5.3 oz gorgonzola cheese", metric: "approx. 150 g gorgonzola cheese"))
        ingrediens.append(createIngredient(imperial: "thyme", metric: "thyme"))
        ingrediens.append(createIngredient(imperial: "rosemary", metric: "rosemary"))
        ingrediens.append(createIngredient(imperial: "salt", metric: "salt"))
        ingrediens.append(createIngredient(imperial: "chilli", metric: "chilli"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the gingerbread in a food processor and mix with the remaining ingredients until the mixture is smooth. Bake the wafers in a wafer iron for about 3-4 minutes. Cut the pears into 0.4-inch slices and grill. Put the pears on top of the wafers together with cut gorgonzola cheese and chopped herbs", step: "Crumble the gingerbread in a food processor and mix with the remaining ingredients until the mixture is smooth. Bake the wafers in a wafer iron for about 3-4 minutes. Cut the pears into 1.5-centimetre slices and grill. Put the pears on top of the wafers together with cut gorgonzola cheese and chopped herbs"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 21, recipeName: "Savoury gingerbread wafers with pears and gorgonzola cheese", recipeAuthor: "Jagoda Paździor-Saba", recipeBlog: "www.zucchini-blues.blogspot.com", recipeThumb: "22t", recipeImage: "22", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/for 10 pops/", metric: "/for 10 pops/"))
        ingrediens.append(createIngredient(imperial: "1 packet Katarzynki® without chocolate", metric: "1 packet Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "5.3 oz peanuts, unsalted", metric: "150 g peanuts, unsalted"))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate", metric: "100 g dark chocolate"))
        ingrediens.append(createIngredient(imperial: "handful coconut shreds or coconut flakes", metric: "handful coconut shreds or coconut flakes"))
        ingrediens.append(createIngredient(imperial: "handful sesame or finely chopped peanuts, seeds", metric: "handful sesame or finely chopped peanuts, seeds"))
        ingrediens.append(createIngredient(imperial: "Extra", metric: "Extra",title: true))
        ingrediens.append(createIngredient(imperial: "10 wooden skewers, e.g. for barbecue and bow or raffia – for decoration", metric: "10 wooden skewers, e.g. for barbecue and bow or raffia – for decoration"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Put the peanuts into a food processor or a blender and mix for about 5-7 minutes until you get smooth peanut butter without lumps. Add crumbled gingerbread and continue mixing until you get the consistency of soggy sand. Next, form walnut-sized balls, put on a large plate and put it into the fridge for at least 1 hour. Before you remove the balls from the fridge, break or chop some chocolate and melt over hot water.", step: "Put the peanuts into a food processor or a blender and mix for about 5-7 minutes until you get smooth peanut butter without lumps. Add crumbled gingerbread and continue mixing until you get the consistency of soggy sand. Next, form walnut-sized balls, put on a large plate and put it into the fridge for at least 1 hour. Before you remove the balls from the fridge, break or chop some chocolate and melt over hot water."))
        methods.append(createMethodStep(imperialStep: "Boil the water in a small saucepan, put a bowl on it so it does not touch the water. Put the chocolate into the bowl. Reduce the heat and wait for a moment until the chocolate melts. Prepare different plates with coconut shreds and sesame (or nuts, seeds). Take the cooled gingerbread balls out of the fridge and skewer one by one. Dip every ball in the melted chocolate and next, in chosen coating, e.g. coconut shreds, sesame or nuts. Put all the gingerbread pops into a high container (e.g. jar) for the chocolate to cool down. Serve straightaway or store in a fridge for 2-3 days most.", step: "Boil the water in a small saucepan, put a bowl on it so it does not touch the water. Put the chocolate into the bowl. Reduce the heat and wait for a moment until the chocolate melts. Prepare different plates with coconut shreds and sesame (or nuts, seeds). Take the cooled gingerbread balls out of the fridge and skewer one by one. Dip every ball in the melted chocolate and next, in chosen coating, e.g. coconut shreds, sesame or nuts. Put all the gingerbread pops into a high container (e.g. jar) for the chocolate to cool down. Serve straightaway or store in a fridge for 2-3 days most."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 22, recipeName: "Gingerbread pops", recipeAuthor: "Joanna Byczkiewicz", recipeBlog: "www.opietruszka.pl", recipeThumb: "23t", recipeImage: "23", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/2 servings/", metric: "/2 servings/",title:true))
        ingrediens.append(createIngredient(imperial: "1 packet Katarzynki® VEGAN ", metric: "1 packet Katarzynki® VEGAN "))
        ingrediens.append(createIngredient(imperial: "1 can homemade or readymade coconut milk", metric: "1 can homemade or readymade coconut milk"))
        ingrediens.append(createIngredient(imperial: "1 handful cashew nuts /earlier soaked for minimum 2 hours/", metric: "1 handful cashew nuts /earlier soaked for minimum 2 hours/"))
        ingrediens.append(createIngredient(imperial: "1 small frozen banana /cut into pieces/", metric: "1 small frozen banana /cut into pieces/"))
        ingrediens.append(createIngredient(imperial: "half tsp cinnamon", metric: "half tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "0.4 in rootstalk fresh ginger", metric: "1 cm rootstalk fresh ginger"))
        ingrediens.append(createIngredient(imperial: "coconut cream", metric: "coconut cream"))
        ingrediens.append(createIngredient(imperial: "handful coconut flakes", metric: "handful coconut flakes"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Blend soaked and dried cashew nuts thoroughly to get smooth mixture. Pour some coconut milk. Add 3 pieces of crumbled gingerbread, frozen banana and cinnamon. Grate the ginger finely and add into the remaining ingredients. Blend everything once more. Pour into glasses. Whip coconut cream, put a few teaspoons on top of the cocktail and mix a little. Decorate with crumbled gingerbread and coconut flakes. Decorate with one piece of gingerbread.", step: "Blend soaked and dried cashew nuts thoroughly to get smooth mixture. Pour some coconut milk. Add 3 pieces of crumbled gingerbread, frozen banana and cinnamon. Grate the ginger finely and add into the remaining ingredients. Blend everything once more. Pour into glasses. Whip coconut cream, put a few teaspoons on top of the cocktail and mix a little. Decorate with crumbled gingerbread and coconut flakes. Decorate with one piece of gingerbread"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 23, recipeName: "Vegan gingerbread cocktail", recipeAuthor: "Joanna Byczkiewicz", recipeBlog: "www.opietruszka.pl", recipeThumb: "24t", recipeImage: "24", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "Creampuffs", metric: "Creampuffs",title: true))
        ingrediens.append(createIngredient(imperial: "8.8 fl oz milk", metric: "250 ml milk"))
        ingrediens.append(createIngredient(imperial: "8.8 fl oz water", metric: "250 ml water"))
        ingrediens.append(createIngredient(imperial: "7 oz butter", metric: "200 g butter"))
        ingrediens.append(createIngredient(imperial: "12 oz wheat flout", metric: "350 g wheat flout"))
        ingrediens.append(createIngredient(imperial: "1.4 oz natural cocoa powder", metric: "40 g natural cocoa powder"))
        ingrediens.append(createIngredient(imperial: "9 eggs /size M/", metric: "9 eggs /size M/"))
        ingrediens.append(createIngredient(imperial: "1 tbsp sugar", metric: "1 tbsp sugar"))
        ingrediens.append(createIngredient(imperial: "Gingerbread topping", metric: "Gingerbread topping",title: true))
        ingrediens.append(createIngredient(imperial: "2 pieces frosted orange Serca Toruńskie®", metric: "2 pieces frosted orange Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "4 tbsp coarse sugar", metric: "4 tbsp coarse sugar"))
        ingrediens.append(createIngredient(imperial: "Orange Curd", metric: "Orange Curd",title: true))
        ingrediens.append(createIngredient(imperial: "1.8 fl oz freshly squeezed orange juice", metric: "50 ml freshly squeezed orange juice"))
        ingrediens.append(createIngredient(imperial: "1.8 oz butter", metric: "50 g butter"))
        ingrediens.append(createIngredient(imperial: "2 tbsp sugar", metric: "2 tbsp sugar"))
        ingrediens.append(createIngredient(imperial: "2 yolks", metric: "2 yolks"))
        ingrediens.append(createIngredient(imperial: "small pinch of salt", metric: "small pinch of salt"))
        ingrediens.append(createIngredient(imperial: "Gingerbread and orange cream", metric: "Gingerbread and orange cream",title: true))
        ingrediens.append(createIngredient(imperial: "8.8 oz whipping cream, 36% fat /well cooled/", metric: "250 g whipping cream, 36% fat /well cooled/"))
        ingrediens.append(createIngredient(imperial: "8.8 oz mascarpone cheese", metric: "250 g mascarpone cheese"))
        ingrediens.append(createIngredient(imperial: "1 tbsp icing sugar", metric: "1 tbsp icing sugar"))
        ingrediens.append(createIngredient(imperial: "3 pieces frosted orange Serca Toruńskie®", metric: "3 pieces frosted orange Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "earlier prepared Orange Curd", metric: "earlier prepared Orange Curd"))
        ingrediens.append(createIngredient(imperial: "Caramel", metric: "Caramel",title: true))
        ingrediens.append(createIngredient(imperial: "17.6 oz sugar", metric: "500 g sugar"))
        ingrediens.append(createIngredient(imperial: "3.5 fl oz water", metric: "100 ml water"))
        ingrediens.append(createIngredient(imperial: "For decoration", metric: "For decoration",title: true))
        ingrediens.append(createIngredient(imperial: "A few pieces of frosted orange Serca Toruńskie ® and orange rind.", metric: "A few pieces of frosted orange Serca Toruńskie ® and orange rind."))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Gingerbread topping", step: "Gingerbread topping",title: true))
        methods.append(createMethodStep(imperialStep: "Grind the gingerbread in a coffee grinder into powder, next, mix with some coarse sugar.", step: "Grind the gingerbread in a coffee grinder into powder, next, mix with some coarse sugar."))
        methods.append(createMethodStep(imperialStep: "Creampuffs", step: "Creampuffs",title: true))
        methods.append(createMethodStep(imperialStep: "Put sugar, water, milk and diced butter in a saucepan, cook until the butter melts. Put sifted flour combined with cocoa powder and heat mixing for some time - it should become very thick. Take off the cooker and cool down. Add sequentially eggs into the cooled mixture and mix thoroughly (the mixture will look curdy but after a moment it will smoothen). Preheat the oven to 200°C. Put the creampuff dough into the pastry bag with a round end. Form small round portions on a large flat baking tray keeping the gaps between them and then, sprinkle with gingerbread powder.", step: "Put sugar, water, milk and diced butter in a saucepan, cook until the butter melts. Put sifted flour combined with cocoa powder and heat mixing for some time - it should become very thick. Take off the cooker and cool down. Add sequentially eggs into the cooled mixture and mix thoroughly (the mixture will look curdy but after a moment it will smoothen). Preheat the oven to 200°C. Put the creampuff dough into the pastry bag with a round end. Form small round portions on a large flat baking tray keeping the gaps between them and then, sprinkle with gingerbread powder."))
        methods.append(createMethodStep(imperialStep: "When the first baking tray is done, put it into the oven and bake at 200°C for about minutes 12-14 minutes until done. Meanwhile, prepare the second part. Take out the baked creampuffs from the oven and put aside to cool down. ", step: "When the first baking tray is done, put it into the oven and bake at 200°C for about minutes 12-14 minutes until done. Meanwhile, prepare the second part. Take out the baked creampuffs from the oven and put aside to cool down. "))
        methods.append(createMethodStep(imperialStep: "Orange Curd", step: "Orange Curd",title: true))
        methods.append(createMethodStep(imperialStep: "Put sugar, orange juice, salt and yolks in a metal bowl, whisk and put on top of a pot with boiling water. Whisk continually, heat until the mixture becomes frothy and hot. Add diced butter and keep on steaming for a few more minutes until Orange Curd thickens. Cool down.", step: "Put sugar, orange juice, salt and yolks in a metal bowl, whisk and put on top of a pot with boiling water. Whisk continually, heat until the mixture becomes frothy and hot. Add diced butter and keep on steaming for a few more minutes until Orange Curd thickens. Cool down."))
        methods.append(createMethodStep(imperialStep: "Gingerbread and orange cream", step: "Gingerbread and orange cream",title: true))
        methods.append(createMethodStep(imperialStep: "Mix mascarpone cheese adding sequentially cooled Orange Curd. Grind the gingerbread in a coffee grinder into powder and add it into the mascarpone mixture. In a different bowl, whip the cooled cream until firm adding the icing sugar at the end. Combine the whipped cream with gingerbread and orange mascarpone cheese and mix awhile. If you need, put more sugar into the cream. Stuff the cooled creampuffs with the cream from the bottom. The easiest way to do it is to use a pastry bag or an icing syringe with long, thin and round end.", step: "Mix mascarpone cheese adding sequentially cooled Orange Curd. Grind the gingerbread in a coffee grinder into powder and add it into the mascarpone mixture. In a different bowl, whip the cooled cream until firm adding the icing sugar at the end. Combine the whipped cream with gingerbread and orange mascarpone cheese and mix awhile. If you need, put more sugar into the cream. Stuff the cooled creampuffs with the cream from the bottom. The easiest way to do it is to use a pastry bag or an icing syringe with long, thin and round end."))
        methods.append(createMethodStep(imperialStep: "Caramel", step: "Caramel",title: true))
        methods.append(createMethodStep(imperialStep: "Put the sugar into a saucepan and pour some water. Heat until the sugar dissolves and changes into amber caramel. Note – do not stir the caramel, you can only gently tilt the saucepan from side to side.", step: "Put the sugar into a saucepan and pour some water. Heat until the sugar dissolves and changes into amber caramel. Note – do not stir the caramel, you can only gently tilt the saucepan from side to side."))
        methods.append(createMethodStep(imperialStep: "Dip the bottoms of the stuffed creampuffs carefully in the hot amber caramel and stick them onto an earlier prepared cardboard cone covered with some baking paper (my cone was 24 in high and approximately 6 in in diameter). Seal the creampuffs from the bottom to the top creating a spectacular pyramid, sticking some gingerbread here and there. At the end, fill in the gaps with orange rind. Now, take a fork and dip in caramel, pour the top of the creampuff pyramid creating threads that will shelter the tower. If the caramel in the saucepan hardens, it is enough if you heat it again over low heat until it liquidises again.", step: "Dip the bottoms of the stuffed creampuffs carefully in the hot amber caramel and stick them onto an earlier prepared cardboard cone covered with some baking paper (my cone was 60 cm high and approximately 15 cm in diameter). Seal the creampuffs from the bottom to the top creating a spectacular pyramid, sticking some gingerbread here and there. At the end, fill in the gaps with orange rind. Now, take a fork and dip in caramel, pour the top of the creampuff pyramid creating threads that will shelter the tower. If the caramel in the saucepan hardens, it is enough if you heat it again over low heat until it liquidises again."))
        methods.append(createMethodStep(imperialStep: "Serve the chocolate creampuff tower straightaway – within some time the creampuffs go soft and caramel threads influenced by humid start to melt. Enjoy!", step: "Serve the chocolate creampuff tower straightaway – within some time the creampuffs go soft and caramel threads influenced by humid start to melt. Enjoy!"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 24, recipeName: "Creampuff chocolate tower with gingerbread and orange bite", recipeAuthor: "Justyna Dragan", recipeBlog: "www.nastoletniewypiekanie.blogspot.com", recipeThumb: "25t", recipeImage: "25", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "2 bars dark chocolate", metric: "2 bars dark chocolate"))
        ingrediens.append(createIngredient(imperial: "8-10 pieces Uszatki®", metric: "8-10 pieces Uszatki®"))
        ingrediens.append(createIngredient(imperial: "3/4 block butter ", metric: "3/4 block butter "))
        ingrediens.append(createIngredient(imperial: "1 glass milk", metric: "1 glass milk"))
        ingrediens.append(createIngredient(imperial: "1 glass sugar", metric: "1 glass sugar"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 glass frozen or fresh raspberries", metric: "1 glass frozen or fresh raspberries"))
        ingrediens.append(createIngredient(imperial: "6 eggs", metric: "6 eggs"))
        ingrediens.append(createIngredient(imperial: "3 spoonful cocoa powder", metric: "3 spoonful cocoa powder"))
        ingrediens.append(createIngredient(imperial: "5.3 oz wheat flour ", metric: "150 g wheat flour "))
        ingrediens.append(createIngredient(imperial: "1 fl oz rum /optional/", metric: "30 ml rum /optional/"))
        ingrediens.append(createIngredient(imperial: "Mascarpone cream", metric: "Mascarpone cream",title: true))
        ingrediens.append(createIngredient(imperial: "7 oz whipping cream, 30% fat", metric: "200 g whipping cream, 30% fat"))
        ingrediens.append(createIngredient(imperial: "8.8 oz mascarpone cheese", metric: "250 g mascarpone cheese"))
        ingrediens.append(createIngredient(imperial: "2 tbsp icing sugar", metric: "2 tbsp icing sugar"))
        ingrediens.append(createIngredient(imperial: "For decoration", metric: "For decoration",title: true))
        ingrediens.append(createIngredient(imperial: "cocoa powder to sprinkle", metric: "cocoa powder to sprinkle"))
        ingrediens.append(createIngredient(imperial: "Serca Toruńskie® and Uszatki®", metric: "Serca Toruńskie® and Uszatki®"))
        ingrediens.append(createIngredient(imperial: "frozen raspberries", metric: "frozen raspberries"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Melt the chocolate with butter and milk over hot water. Mix everything and put aside to cool down. Start making the cake from mixing the eggs with sugar into light fluffy mixture. Pour the rum and mix it again. Add sifted flour, cocoa powder and cinnamon into the mixture and stir with a spatula. At the end, add finely cut gingerbread (0.4-inch pieces) and raspberries. Lay a 10-inch cake tin with some baking paper. Pour the mixture into the cake tin and bake for 50-60 minutes at 320ºF until, so called, ‘dry stick’. When baked, take the cake out of the oven and leave to cool down.", step: "Melt the chocolate with butter and milk over hot water. Mix everything and put aside to cool down. Start making the cake from mixing the eggs with sugar into light fluffy mixture. Pour the rum and mix it again. Add sifted flour, cocoa powder and cinnamon into the mixture and stir with a spatula. At the end, add finely cut gingerbread (1-cm pieces) and raspberries. Lay a 26-centimetre cake tin with some baking paper. Pour the mixture into the cake tin and bake for 50-60 minutes at 160°C until, so called, ‘dry stick’. When baked, take the cake out of the oven and leave to cool down."))
        methods.append(createMethodStep(imperialStep: "When the cake is cooled, prepare the cream: mix the cream with icing sugar at highest speed until it is firm and next, add mascarpone cheese and mix awhile – until the ingredients combine. Put the cream on top of the cake with a spatula creating a cloud or put it into an icing syringe and make small rosettes on the brownie. Sprinkle the cream with cocoa powder and then, decorate with gingerbread and frozen raspberries.", step: "When the cake is cooled, prepare the cream: mix the cream with icing sugar at highest speed until it is firm and next, add mascarpone cheese and mix awhile – until the ingredients combine. Put the cream on top of the cake with a spatula creating a cloud or put it into an icing syringe and make small rosettes on the brownie. Sprinkle the cream with cocoa powder and then, decorate with gingerbread and frozen raspberries."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 25, recipeName: "Gingerbread brownie", recipeAuthor: "Klaudia Sroczyńska", recipeBlog: "www.dusiowakuchnia.pl", recipeThumb: "26t", recipeImage: "26", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "14 oz venison loin", metric: "400 g venison loin"))
        ingrediens.append(createIngredient(imperial: "1.8 oz Katarzynki® without chocolate", metric: "50 g Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "10 oz pumpkin", metric: "300 g pumpkin"))
        ingrediens.append(createIngredient(imperial: "2 tbsp olive oil", metric: "2 tbsp olive oil"))
        ingrediens.append(createIngredient(imperial: "3 sprigs rosemary", metric: "3 sprigs rosemary"))
        ingrediens.append(createIngredient(imperial: "1 fresh bay leaf", metric: "1 fresh bay leaf"))
        ingrediens.append(createIngredient(imperial: "1 sprig thyme", metric: "1 sprig thyme"))
        ingrediens.append(createIngredient(imperial: "coriander leaves", metric: "coriander leaves"))
        ingrediens.append(createIngredient(imperial: "1 clove garlic", metric: "1 clove garlic"))
        ingrediens.append(createIngredient(imperial: "5.3 fl oz red dry wine", metric: "150 ml red dry wine"))
        ingrediens.append(createIngredient(imperial: "1 orange", metric: "1 orange"))
        ingrediens.append(createIngredient(imperial: "1 ripe pear", metric: "1 ripe pear"))
        ingrediens.append(createIngredient(imperial: "1 parsley root", metric: "1 parsley root"))
        ingrediens.append(createIngredient(imperial: "2 tbsp honey", metric: "2 tbsp honey"))
        ingrediens.append(createIngredient(imperial: "3 tbsp butter", metric: "3 tbsp butter"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Saddle of venison", step: "Saddle of venison",title: true))
        methods.append(createMethodStep(imperialStep: "Soak the meat in milk and leave in a cool place for a few hours. Mix chopped coriander and rosemary with salt and fresh black pepper. Rub the saddle of venison in spices and fry it in the olive oil awhile. Put the meat into a casserole dish. Next, add crushed garlic and a little bit of water. Put into the oven and roast for 30 minutes at 356°F. Pour the gravy into a frying pan. Pour the wine and juice squeezed from one orange. When boiled, reduce the heat. Add fresh bay leaf, a sprig of thyme and crumbled gingerbread. Season everything with a hint of salt and pepper. Heat until it thickens. Pour the sauce over the sliced saddle.", step: "Soak the meat in milk and leave in a cool place for a few hours. Mix chopped coriander and rosemary with salt and fresh black pepper. Rub the saddle of venison in spices and fry it in the olive oil awhile. Put the meat into a casserole dish. Next, add crushed garlic and a little bit of water. Put into the oven and roast for 30 minutes at 180°C. Pour the gravy into a frying pan. Pour the wine and juice squeezed from one orange. When boiled, reduce the heat. Add fresh bay leaf, a sprig of thyme and crumbled gingerbread. Season everything with a hint of salt and pepper. Heat until it thickens. Pour the sauce over the sliced saddle."))
        methods.append(createMethodStep(imperialStep: "Pumpkin purée", step: "Pumpkin purée"))
        methods.append(createMethodStep(imperialStep: "Cut the pumpkin into small pieces and get rid of the seed. Preheat the oven to 180°C. Put it into the oven and bake for about 35 minutes. Cool down the baked pumpkin, peel and mix using a blender until you get smooth purée. ", step: "Cut the pumpkin into small pieces and get rid of the seed. Preheat the oven to 180°C. Put it into the oven and bake for about 35 minutes. Cool down the baked pumpkin, peel and mix using a blender until you get smooth purée."))
        methods.append(createMethodStep(imperialStep: "Baked pear", step: "Baked pear"))
        methods.append(createMethodStep(imperialStep: "Preheat the oven to 356°F. Halve the pear alongside and get rid of the core. Melt the butter in the frying pan and add some honey. Mix everything thoroughly. Spread the pear frequently with the butter and honey sauce during the baking. Bake for about 30 minutes.", step: "Preheat the oven to 180°C. Halve the pear alongside and get rid of the core. Melt the butter in the frying pan and add some honey. Mix everything thoroughly. Spread the pear frequently with the butter and honey sauce during the baking. Bake for about 30 minutes."))
        methods.append(createMethodStep(imperialStep: "Parsley crisps", step: "Parsley crisps"))
        methods.append(createMethodStep(imperialStep: "Wash and peel the parsley root. Heat the oil in a deep-fryer. Cut the parsley root into thin tagliatelle. Put into the hot oil and fry until golden brown. Drain on the  kitchen roll.", step: "Wash and peel the parsley root. Heat the oil in a deep-fryer. Cut the parsley root into thin tagliatelle. Put into the hot oil and fry until golden brown. Drain on the  kitchen roll."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 26, recipeName: "Saddle of venison in savoury gingerbread sauce with baked pear and pumpkin purée", recipeAuthor: "Krzysztof Tonder", recipeBlog: "", recipeThumb: "27t", recipeImage: "27", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "1 tinned chickpea /14 oz/", metric: "1 tinned chickpea /400 g/"))
        ingrediens.append(createIngredient(imperial: "3 pieces Katarzynki® without chocolate", metric: "3 pieces Katarzynki® without chocolate"))
        ingrediens.append(createIngredient(imperial: "3 tbsp tahini paste", metric: "3 tbsp tahini paste"))
        ingrediens.append(createIngredient(imperial: "1 tbsp lemon juice", metric: "1 tbsp lemon juice"))
        ingrediens.append(createIngredient(imperial: "5 tbsp olive oil", metric: "5 tbsp olive oil"))
        ingrediens.append(createIngredient(imperial: "1 clove garlic", metric: "1 clove garlic"))
        ingrediens.append(createIngredient(imperial: "1 tsp cumin", metric: "1 tsp cumin"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Grind the Katarzynki® into powder and leave two tablespoons. Drain the chickpea and put into a high dish with the remaining ingredients. Mix for a few minutes to get smooth consistency. If the hummus is too dense, add a little bit of the chickpea juice or warm water. Put the hummus into a bowl and sprinkle with some powdered gingerbread.", step: "Grind the Katarzynki® into powder and leave two tablespoons. Drain the chickpea and put into a high dish with the remaining ingredients. Mix for a few minutes to get smooth consistency. If the hummus is too dense, add a little bit of the chickpea juice or warm water. Put the hummus into a bowl and sprinkle with some powdered gingerbread."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 27, recipeName: "Gingerbread Hummus", recipeAuthor: "Magdalena Ornowska", recipeBlog: "", recipeThumb: "28t", recipeImage: "28", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "17.6 oz beef ", metric: "500 g beef "))
        ingrediens.append(createIngredient(imperial: "14 fl oz caramel beer", metric: "400 ml caramel beer"))
        ingrediens.append(createIngredient(imperial: "17.6 fl oz warm meat stock ", metric: "500 ml warm meat stock "))
        ingrediens.append(createIngredient(imperial: "3.2 oz Uszatki® ", metric: "90 g Uszatki® "))
        ingrediens.append(createIngredient(imperial: "7 oz mini champignons", metric: "200 g mini champignons"))
        ingrediens.append(createIngredient(imperial: "4 small carrots ", metric: "4 small carrots "))
        ingrediens.append(createIngredient(imperial: "1 shallot onion /large/", metric: "1 shallot onion /large/"))
        ingrediens.append(createIngredient(imperial: "2 cloves garlic /large/", metric: "2 cloves garlic /large/"))
        ingrediens.append(createIngredient(imperial: "1 tsp ginger", metric: "1 tsp ginger"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 tsp chilli", metric: "1 tsp chilli"))
        ingrediens.append(createIngredient(imperial: "2 tsp hot mustard", metric: "2 tsp hot mustard"))
        ingrediens.append(createIngredient(imperial: "2 bay leaves", metric: "2 bay leaves"))
        ingrediens.append(createIngredient(imperial: "4 cloves", metric: "4 cloves"))
        ingrediens.append(createIngredient(imperial: "a few sprigs of fresh thyme", metric: "a few sprigs of fresh thyme"))
        ingrediens.append(createIngredient(imperial: "3 tbsp flour", metric: "3 tbsp flour"))
        ingrediens.append(createIngredient(imperial: "2 tbsp butter", metric: "2 tbsp butter"))
        ingrediens.append(createIngredient(imperial: "2 tbsp olive oil", metric: "2 tbsp olive oil"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Prepare the marinade for the meat: chop the garlic finely. Pick the leaves from 2-3 thyme sprigs. Rub 1 oz gingerbread in a mortar. Add garlic, thyme leaves (you can replace with some dried thyme – approx. half a teaspoon), ginger, cinnamon a bit of pepper, salt and olive oil. Mix it all. Rub the marinade into the meat and leave for about 10 minutes.", step: "Prepare the marinade for the meat: chop the garlic finely. Pick the leaves from 2-3 thyme sprigs. Rub 30 g gingerbread in a mortar. Add garlic, thyme leaves (you can replace with some dried thyme – approx. half a teaspoon), ginger, cinnamon a bit of pepper, salt and olive oil. Mix it all. Rub the marinade into the meat and leave for about 10 minutes."))
        methods.append(createMethodStep(imperialStep: "Melt a tablespoon of butter and put the champignons in a pot with a thick bottom (it is best to use the pot that can be put into the oven). Season with salt and pepper, add thyme leaves from one sprig. Fry the champignons over high heat, do not stew but fry (they should be golden brown). Put the champignons on the plate. Melt a tablespoon of the butter in the same frying pan and put diagonally cut (approximately 0.20-inch slices) carrots. Season just like the champignons and fry for about 3 minutes. Put into the champignons and leave. ", step: "Melt a tablespoon of butter and put the champignons in a pot with a thick bottom (it is best to use the pot that can be put into the oven). Season with salt and pepper, add thyme leaves from one sprig. Fry the champignons over high heat, do not stew but fry (they should be golden brown). Put the champignons on the plate. Melt a tablespoon of the butter in the same frying pan and put diagonally cut (approximately 4-mm slices) carrots. Season just like the champignons and fry for about 3 minutes. Put into the champignons and leave. "))
        methods.append(createMethodStep(imperialStep: "The meat at room temperature fry in the same pot over high heat – it is important for the pores to close preventing juices from coming out. The beef should have a nice golden colour. Sprinkle the meat with some flour and mix it. After 2 minutes, pour cold caramel beer. When boiled, reduce the heat and add warm meat stock and bring it to boil again. Add carrots, champignons and crumbled gingerbread. Season with bay leaves, cloves, thyme, mustard, salt, pepper and alternatively chilli pepper.", step: "The meat at room temperature fry in the same pot over high heat – it is important for the pores to close preventing juices from coming out. The beef should have a nice golden colour. Sprinkle the meat with some flour and mix it. After 2 minutes, pour cold caramel beer. When boiled, reduce the heat and add warm meat stock and bring it to boil again. Add carrots, champignons and crumbled gingerbread. Season with bay leaves, cloves, thyme, mustard, salt, pepper and alternatively chilli pepper."))
        methods.append(createMethodStep(imperialStep: "Cover it tightly with a lid and stew at the lowest heat for 150 minutes. You can put it into the oven and stew at 194°F for the same amount of time. At the end, uncover, season and stew for half an hour – without a lid.", step: "Cover it tightly with a lid and stew at the lowest heat for 150 minutes. You can put it into the oven and stew at 90°C for the same amount of time. At the end, uncover, season and stew for half an hour – without a lid."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 28, recipeName: "Beef stew in caramel and gingerbread sauce", recipeAuthor: "Małgorzata Socha", recipeBlog: "www.relaksodkuchni.pl", recipeThumb: "29t", recipeImage: "29", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "8 pieces matjes herrings in vinegar sauce /well pickled/ ", metric: "8 pieces matjes herrings in vinegar sauce /well pickled/ "))
        ingrediens.append(createIngredient(imperial: "20 pieces prunes ", metric: "20 pieces prunes "))
        ingrediens.append(createIngredient(imperial: "zest from one lemon", metric: "zest from one lemon"))
        ingrediens.append(createIngredient(imperial: "5 red onions ", metric: "5 red onions "))
        ingrediens.append(createIngredient(imperial: "sea salt ", metric: "sea salt "))
        ingrediens.append(createIngredient(imperial: "freshly ground black pepper", metric: "freshly ground black pepper"))
        ingrediens.append(createIngredient(imperial: "cayenne pepper", metric: "cayenne pepper"))
        ingrediens.append(createIngredient(imperial: "rapeseed oil for frying onion", metric: "rapeseed oil for frying onion"))
        ingrediens.append(createIngredient(imperial: "Gingerbread sauce", metric: "Gingerbread sauce",title:true))
        ingrediens.append(createIngredient(imperial: "1 glass unrefined rapeseed oil ", metric: "1 glass unrefined rapeseed oil "))
        ingrediens.append(createIngredient(imperial: "1/2 tsp ground cardamom", metric: "1/2 tsp ground cardamom"))
        ingrediens.append(createIngredient(imperial: "1 tsp ground cloves", metric: "1 tsp ground cloves"))
        ingrediens.append(createIngredient(imperial: "1 tsp ground ginger", metric: "1 tsp ground ginger"))
        ingrediens.append(createIngredient(imperial: "1 tsp chilli", metric: "1 tsp chilli"))
        ingrediens.append(createIngredient(imperial: "3 tsp honey", metric: "3 tsp honey"))
        ingrediens.append(createIngredient(imperial: "2 pieces Katarzynki® without chocolate ", metric: "2 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "1/2 glass boiled water ", metric: "1/2 glass boiled water "))
        ingrediens.append(createIngredient(imperial: "1/2 lemon juice", metric: "1/2 lemon juice"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Cut the herrings into large pieces and dice finely the prunes. Fry the onions cut into rings in the frying pan. Prepare the sauce: Heat the rapeseed oil in a small saucepan, add all the spices and mix for one minute to get the flavour. Next, add powdered gingerbread and 3 teaspoons of honey. To mix the mixture you can use a beater and dilute with water. At the end, add lemon juice (you cannot add earlier because the sauce will curdle. Take the dish in which you will serve it and lay alternately herrings, onions, prunes, lemon zest, sauce. You can make a few layers depending on the shape of the dish. Leave in the fridge for 2-3 days so everything gets a unique flavour.", step: "Cut the herrings into large pieces and dice finely the prunes. Fry the onions cut into rings in the frying pan. Prepare the sauce: Heat the rapeseed oil in a small saucepan, add all the spices and mix for one minute to get the flavour. Next, add powdered gingerbread and 3 teaspoons of honey. To mix the mixture you can use a beater and dilute with water. At the end, add lemon juice (you cannot add earlier because the sauce will curdle. Take the dish in which you will serve it and lay alternately herrings, onions, prunes, lemon zest, sauce. You can make a few layers depending on the shape of the dish. Leave in the fridge for 2-3 days so everything gets a unique flavour."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 29, recipeName: "Toruń-style herrings", recipeAuthor: "Małgorzata Kufel", recipeBlog: "", recipeThumb: "30t", recipeImage: "30", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/1 serving/ ", metric: "/1 serving/ "))
        ingrediens.append(createIngredient(imperial: "1 courgette flower", metric: "1 courgette flower"))
        ingrediens.append(createIngredient(imperial: "1.8 oz goat cheese", metric: "50 g goat cheese"))
        ingrediens.append(createIngredient(imperial: "0.7 oz walnuts ", metric: "20 g walnuts "))
        ingrediens.append(createIngredient(imperial: "0.7 oz apples ", metric: "20 g apples "))
        ingrediens.append(createIngredient(imperial: "1.8 oz new courgette", metric: "50 g new courgette"))
        ingrediens.append(createIngredient(imperial: "1 piece Serca Toruńskie®", metric: "1 piece Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "1 tsp sesame oil ", metric: "1 tsp sesame oil "))
        ingrediens.append(createIngredient(imperial: "1.8 fl oz gingerbread beer ", metric: "50 ml gingerbread beer "))
        ingrediens.append(createIngredient(imperial: "0.35 oz butter ", metric: "10 g butter "))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Get rid of the stamen and gently clean (preferably with a brush). Prepare the stuffing to fill the flower. Cut the apple and goat cheese into 0.4 inch x 0.4 inch cubes. Chop the walnuts and half portion of the gingerbread. At the end, add the sesame oil. Mix thoroughly and leave for 10-15 minutes.", step: "Get rid of the stamen and gently clean (preferably with a brush). Prepare the stuffing to fill the flower. Cut the apple and goat cheese into 1cm x 1cm cubes. Chop the walnuts and half portion of the gingerbread. At the end, add the sesame oil. Mix thoroughly and leave for 10-15 minutes."))
        methods.append(createMethodStep(imperialStep: "Cut the new courgette into 0.4 inch x 0.4 inch cubes, put into a saucepan, add beer and heat it until the juice is reduced, take off the cooker and add some butter mixing gently. Carefully fill the flower with the goat cheese. Heat the frying pan and without any fat, fry the flower turning around on the hot surface for 1 minute. Before putting it on the plate, chop the remaining gingerbread which can be used as crunch under the courgette.", step: "Cut the new courgette into 1cm x 1cm cubes, put into a saucepan, add beer and heat it until the juice is reduced, take off the cooker and add some butter mixing gently. Carefully fill the flower with the goat cheese. Heat the frying pan and without any fat, fry the flower turning around on the hot surface for 1 minute. Before putting it on the plate, chop the remaining gingerbread which can be used as crunch under the courgette."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 30, recipeName: "Courgette flower stuffed with goat cheese and gingerbread", recipeAuthor: "Courgette flower stuffed with goat cheese and gingerbread", recipeBlog: "", recipeThumb: "31t", recipeImage: "31", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a 9.4-inch cake tin/", metric: "/a 24-cm cake tin/",title: true))
        ingrediens.append(createIngredient(imperial: "Base", metric: "Base",title: true))
        ingrediens.append(createIngredient(imperial: "3 packets Katarzynki® VEGAN ", metric: "3 packets Katarzynki® VEGAN "))
        ingrediens.append(createIngredient(imperial: "2 tbsp coconut sugar /unrefined/ ", metric: "2 tbsp coconut sugar /unrefined/ "))
        ingrediens.append(createIngredient(imperial: "2 tsp orange extract", metric: "2 tsp orange extract"))
        ingrediens.append(createIngredient(imperial: "3 tbsp vegetable butter", metric: "3 tbsp vegetable butter"))
        ingrediens.append(createIngredient(imperial: "Cheese mixture", metric: "Cheese mixture",title: true))
        ingrediens.append(createIngredient(imperial: "28 oz tofu", metric: "800 g tofu"))
        ingrediens.append(createIngredient(imperial: "17.6 fl oz canned coconut milk", metric: "500 ml canned coconut milk"))
        ingrediens.append(createIngredient(imperial: "2 packets vegan vanilla pudding /powdered/", metric: "2 packets vegan vanilla pudding /powdered/"))
        ingrediens.append(createIngredient(imperial: "3 tbsp coconut flour", metric: "3 tbsp coconut flour"))
        ingrediens.append(createIngredient(imperial: "1 glass icing cane sugar", metric: "1 glass icing cane sugar"))
        ingrediens.append(createIngredient(imperial: "1 tsp almond extract", metric: "1 tsp almond extract"))
        ingrediens.append(createIngredient(imperial: "1 tsp baking powder ", metric: "1 tsp baking powder "))
        ingrediens.append(createIngredient(imperial: "3 tbsp freshly squeezed lemon juice", metric: "3 tbsp freshly squeezed lemon juice"))
        ingrediens.append(createIngredient(imperial: "zest from one lemon ", metric: "zest from one lemon "))
        ingrediens.append(createIngredient(imperial: "For decoration", metric: "For decoration",title: true))
        ingrediens.append(createIngredient(imperial: "1 small jar plum jam ", metric: "1 small jar plum jam "))
        ingrediens.append(createIngredient(imperial: "1 packet Katarzynki® VEGAN", metric: "1 packet Katarzynki® VEGAN"))
        ingrediens.append(createIngredient(imperial: "14 fl oz whipping coconut cream", metric: "400 ml whipping coconut cream"))
        ingrediens.append(createIngredient(imperial: "1.8 oz coconut crisps", metric: "50 g coconut crisps"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Base", step: "Base",title: true))
        methods.append(createMethodStep(imperialStep: "Put all ingredients for the base into a food processor and mix until you get the consistency resembling fine sand. Put it into the cake tin and squash with a spoon. Put into the fridge for the time of preparing the cheese mixture.", step: "Put all ingredients for the base into a food processor and mix until you get the consistency resembling fine sand. Put it into the cake tin and squash with a spoon. Put into the fridge for the time of preparing the cheese mixture."))
        methods.append(createMethodStep(imperialStep: "Cheese mixture", step: "Cheese mixture",title: true))
        methods.append(createMethodStep(imperialStep: "Drain the tofu. Put it with all the remaining ingredients into the food processor needed to make cheese mixture. Mix thoroughly until the mixture is smooth. Lay the cake tin with some baking paper. Pour the mixture onto the gingerbread base. Bake for about 60 minutes at 320°F with a fan. Leave the cake to cool down, preferably for the night.", step: "Drain the tofu. Put it with all the remaining ingredients into the food processor needed to make cheese mixture. Mix thoroughly until the mixture is smooth. Lay the cake tin with some baking paper. Pour the mixture onto the gingerbread base. Bake for about 60 minutes at 160°C with a fan. Leave the cake to cool down, preferably for the night."))
        methods.append(createMethodStep(imperialStep: "Decoration", step: "Decoration",title: true))
        methods.append(createMethodStep(imperialStep: "Whip the cream until firm. Spread the top of the tofurnik with plum jam. Put the whipped cream, crumbled gingerbread and sprinkle with some coconut crisps.", step: "Whip the cream until firm. Spread the top of the tofurnik with plum jam. Put the whipped cream, crumbled gingerbread and sprinkle with some coconut crisps."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 31, recipeName: "Vegan tofurnik on gingerbread and orange base with coconut cream", recipeAuthor: "Monika Kozak", recipeBlog: "www.pinupcooksuperfood.blogspot.com", recipeThumb: "32t", recipeImage: "32", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/16 pieces/", metric: "/16 pieces/",title: true))
        ingrediens.append(createIngredient(imperial: "10.6 oz wheat flour, type 650", metric: "300 g wheat flour, type 650"))
        ingrediens.append(createIngredient(imperial: "5.3 fl oz skimmed milk, 3,2% fat", metric: "150 ml skimmed milk, 3,2% fat"))
        ingrediens.append(createIngredient(imperial: "0.9 oz melted butter", metric: "25 g melted butter"))
        ingrediens.append(createIngredient(imperial: "0.4 oz fresh yeast", metric: "12 g fresh yeast"))
        ingrediens.append(createIngredient(imperial: "0.9 oz sugar", metric: "25 g sugar"))
        ingrediens.append(createIngredient(imperial: "1 egg", metric: "1 egg"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "pinch salt", metric: "pinch salt"))
        ingrediens.append(createIngredient(imperial: "Stuffing", metric: "Stuffing",title: true))
        ingrediens.append(createIngredient(imperial: "7 oz Pierniki Nadziewane® (Gingerbread with Filling) in white chocolate From Factory’s Heart ®", metric: "200 g Pierniki Nadziewane® (Gingerbread with Filling) in white chocolate From Factory’s Heart ®"))
        ingrediens.append(createIngredient(imperial: "3 tbsp liquid honey", metric: "3 tbsp liquid honey"))
        ingrediens.append(createIngredient(imperial: "3 tbsp blackcurrant jam /optionally plum jam/", metric: "3 tbsp blackcurrant jam /optionally plum jam/"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the yeast into a bowl, add lukewarm milk, sugar, 2 tablespoons of flour. Mix everything and put aside for 15 minutes letting the leaven to work. Pour some melted butter to the leaven. Add sifted flour, salt, cinnamon, break the egg and knead smooth elastic dough. If the dough is too soft, add some more flour. Form a ball and put into the bowl, cover with a linen cloth and leave in a warm place for an hour to order to double its size.", step: "Crumble the yeast into a bowl, add lukewarm milk, sugar, 2 tablespoons of flour. Mix everything and put aside for 15 minutes letting the leaven to work. Pour some melted butter to the leaven. Add sifted flour, salt, cinnamon, break the egg and knead smooth elastic dough. If the dough is too soft, add some more flour. Form a ball and put into the bowl, cover with a linen cloth and leave in a warm place for an hour to order to double its size."))
        methods.append(createMethodStep(imperialStep: "Prepare the stuffing. Put halved pieces of gingerbread into a blender, add some honey and jam, blend until you get nice and smooth mixture. Knead the prepared dough awhile on a floured pastry board, pin out into a rectangular. Spread with the prepared stuffing. Roll the dough starting from the longer side of the rectangular. Using a sharp knife, cut it into 0.4-inch slices. Put the rolls on a baking tray laid with some baking paper leaving the gaps in between because the dough is going to rise. Cover everything with a linen cloth and leave in a warm place for about 25 minutes to rise.", step: "Prepare the stuffing. Put halved pieces of gingerbread into a blender, add some honey and jam, blend until you get nice and smooth mixture. Knead the prepared dough awhile on a floured pastry board, pin out into a rectangular. Spread with the prepared stuffing. Roll the dough starting from the longer side of the rectangular. Using a sharp knife, cut it into 1-cm slices. Put the rolls on a baking tray laid with some baking paper leaving the gaps in between because the dough is going to rise. Cover everything with a linen cloth and leave in a warm place for about 25 minutes to rise."))
        methods.append(createMethodStep(imperialStep: "Preheat the oven to 356°F with the up-down heating. Put the rolls into the oven and bake for 20-22 minutes until golden brown. Put aside to cool down, even warm are delicious!", step: "Preheat the oven to 180°C with the up-down heating. Put the rolls into the oven and bake for 20-22 minutes until golden brown. Put aside to cool down, even warm are delicious!"))
        
        // ---- recipe
        recipe = Recipe(recipeID: 32, recipeName: "Rolls with gingerbread filling", recipeAuthor: "Monika Adamczyk", recipeBlog: "www.matkawariatka.net", recipeThumb: "33t", recipeImage: "33", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a 8-inch cake tin/", metric: "/a 20-cm cake tin/",title: true))
        ingrediens.append(createIngredient(imperial: "Cake", metric: "Cake",title: true))
        ingrediens.append(createIngredient(imperial: "8 pieces Serca Toruńskie®", metric: "8 pieces Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "8.8 oz wheat flour", metric: "250 g wheat flour"))
        ingrediens.append(createIngredient(imperial: "3.5 oz butter", metric: "100 g butter"))
        ingrediens.append(createIngredient(imperial: "1 tbsp icing sugar", metric: "1 tbsp icing sugar"))
        ingrediens.append(createIngredient(imperial: "1 tsp baking powder", metric: "1 tsp baking powder"))
        ingrediens.append(createIngredient(imperial: "pinch salt", metric: "pinch salt"))
        ingrediens.append(createIngredient(imperial: "1 egg", metric: "1 egg"))
        ingrediens.append(createIngredient(imperial: "1 yolk", metric: "1 yolk"))
        ingrediens.append(createIngredient(imperial: "2 tbsp sour cream", metric: "2 tbsp sour cream"))
        ingrediens.append(createIngredient(imperial: "Apple mixture", metric: "Apple mixture",title: true))
        ingrediens.append(createIngredient(imperial: "21 oz peeled apples", metric: "600 g peeled apples"))
        ingrediens.append(createIngredient(imperial: "3 tbsp sugar", metric: "3 tbsp sugar"))
        ingrediens.append(createIngredient(imperial: "1 tsp cinnamon", metric: "1 tsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 tbsp lemon juice", metric: "1 tbsp lemon juice"))
        ingrediens.append(createIngredient(imperial: "1.5 tsp gelatine", metric: "1.5 tsp gelatine"))
        ingrediens.append(createIngredient(imperial: "2 tbsp boiling water", metric: "2 tbsp boiling water"))
        ingrediens.append(createIngredient(imperial: "Crumble", metric: "Crumble",title: true))
        ingrediens.append(createIngredient(imperial: "2 pieces Serca Toruńskie®", metric: "2 pieces Serca Toruńskie®"))
        ingrediens.append(createIngredient(imperial: "0.7 fl oz butter", metric: "20 g butter"))
        ingrediens.append(createIngredient(imperial: "1 tbsp wheat flour", metric: "1 tbsp wheat flour"))
        ingrediens.append(createIngredient(imperial: "2 tbsp cane sugar", metric: "2 tbsp cane sugar"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Break the gingerbread for the cake and for the crumble into pieces, put on a baking tray and leave for 8-10 hours. Mix the dried gingerbread into powder. Mix 5.6 oz ground gingerbread with flour, salt, icing sugar and baking powder on a pastry board. Add some butter and chop everything into small pieces. Add egg, yolk and cream. Knead the dough and form a ball. Next, pin out until you get about 12-inch diameter dough.", step: "Break the gingerbread for the cake and for the crumble into pieces, put on a baking tray and leave for 8-10 hours. Mix the dried gingerbread into powder. Mix 160 g ground gingerbread with flour, salt, icing sugar and baking powder on a pastry board. Add some butter and chop everything into small pieces. Add egg, yolk and cream. Knead the dough and form a ball. Next, pin out until you get about 30-cm diameter dough."))
        methods.append(createMethodStep(imperialStep: "Put into a greased cake tin covering the bottom and the edges. Put into the fridge for 30 minutes. Cut the apples into 1cm x 1cm cubes. Put into a saucepan and stir with some sugar. Heat it until the apples release the juice and the sugar dissolves. Add lemon juice and cinnamon. Dissolve the gelatine in boiling water and pour into the apples. Mix and take off the cooker. Preheat the oven to 392°F.", step: "Put into a greased cake tin covering the bottom and the edges. Put into the fridge for 30 minutes. Cut the apples into 1cm x 1cm cubes. Put into a saucepan and stir with some sugar. Heat it until the apples release the juice and the sugar dissolves. Add lemon juice and cinnamon. Dissolve the gelatine in boiling water and pour into the apples. Mix and take off the cooker. Preheat the oven to 200°C."))
        methods.append(createMethodStep(imperialStep: "Make the crumble from the given ingredients. Take the cake tin from the fridge, prick the base with the fork in a few places. Put into the oven and bake for 10 minutes. Take it out, fill the cake with the apple mixture and sprinkle the top with the crumble. Put it into the oven again and bake for 25 minutes. After baking, take the ring of the cake tin and put the apple pie aside for at least 5 hours to cool down.", step: "Make the crumble from the given ingredients. Take the cake tin from the fridge, prick the base with the fork in a few places. Put into the oven and bake for 10 minutes. Take it out, fill the cake with the apple mixture and sprinkle the top with the crumble. Put it into the oven again and bake for 25 minutes. After baking, take the ring of the cake tin and put the apple pie aside for at least 5 hours to cool down."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 33, recipeName: "Gingerbread apple pie", recipeAuthor: "Natalia Bogubowicz", recipeBlog: "www.deliciousplacebynatalia.blogspot.com", recipeThumb: "34t", recipeImage: "34", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "3 salted herrings", metric: "3 salted herrings"))
        ingrediens.append(createIngredient(imperial: "baguette", metric: "baguette"))
        ingrediens.append(createIngredient(imperial: "Plum mousse", metric: "Plum mousse",title: true))
        ingrediens.append(createIngredient(imperial: "10.6 oz plums", metric: "300 g plums"))
        ingrediens.append(createIngredient(imperial: "4 pieces Katarzynki® without chocolate ", metric: "4 pieces Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "1-2 pinches spices", metric: "1-2 pinches spices"))
        ingrediens.append(createIngredient(imperial: "Red onion jam", metric: "Red onion jam",title: true))
        ingrediens.append(createIngredient(imperial: "4 large onions", metric: "4 large onions"))
        ingrediens.append(createIngredient(imperial: "3.5 fl oz Port wine", metric: "100 ml Port wine"))
        ingrediens.append(createIngredient(imperial: "4 tbsp balsamic wine", metric: "4 tbsp balsamic wine"))
        ingrediens.append(createIngredient(imperial: "1.8 oz butter", metric: "50 g butter"))
        ingrediens.append(createIngredient(imperial: "salt and pepper", metric: "salt and pepper"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Stone the plums and put them into a saucepan with a non-stick layer. Add crumbled gingerbread and the spices. Cook for about 20 minutes over low heat until the plums fall apart, and everything thickens. Next, blend it.", step: "Stone the plums and put them into a saucepan with a non-stick layer. Add crumbled gingerbread and the spices. Cook for about 20 minutes over low heat until the plums fall apart, and everything thickens. Next, blend it."))
        methods.append(createMethodStep(imperialStep: "Jam", step: "Jam",title: true))
        methods.append(createMethodStep(imperialStep: "Slice the onion and put into a saucepan in melted butter and salt it. Fry until it is tender. Then, add the remaining ingredients and cook over low heat to reduce the juice. Season with salt and pepper. There will be more jam than you need so when hot put into a jar and leave upside down for the next time.", step: "Slice the onion and put into a saucepan in melted butter and salt it. Fry until it is tender. Then, add the remaining ingredients and cook over low heat to reduce the juice. Season with salt and pepper. There will be more jam than you need so when hot put into a jar and leave upside down for the next time."))
        methods.append(createMethodStep(imperialStep: "Make sure that the herrings are not too salty. If so, put them into cold water. Cut the baguette into 0.40 inch-0.60 inch pieces. Toast them in a dry frying pan, spread them with plum mousse and put a piece of herring on top. Finish them with onion jam and sprinkle with crumbled gingerbread and pepper. You can also spread some plum mousse on a plate, spread a herring with some onion jam, roll into a roulade and clip with a cocktail stick. Put it on the plum mousse and sprinkle with some crumbled gingerbread and pepper.", step: "Make sure that the herrings are not too salty. If so, put them into cold water. Cut the baguette into 1-1.5 cm pieces. Toast them in a dry frying pan, spread them with plum mousse and put a piece of herring on top. Finish them with onion jam and sprinkle with crumbled gingerbread and pepper. You can also spread some plum mousse on a plate, spread a herring with some onion jam, roll into a roulade and clip with a cocktail stick. Put it on the plum mousse and sprinkle with some crumbled gingerbread and pepper."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 34, recipeName: "Herrings with onion jam", recipeAuthor: "Renata Wolszczak", recipeBlog: "", recipeThumb: "35t", recipeImage: "35", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "1 glass crumbled Katarzynki® without chocolate or Uszatki®", metric: "1 glass crumbled Katarzynki® without chocolate or Uszatki®"))
        ingrediens.append(createIngredient(imperial: "1 glass favourite nuts /e.g. cashew, hazelnuts, almonds and peanuts/", metric: "1 glass favourite nuts /e.g. cashew, hazelnuts, almonds and peanuts/"))
        ingrediens.append(createIngredient(imperial: "1 glass oat flakes", metric: "1 glass oat flakes"))
        ingrediens.append(createIngredient(imperial: "2 tbsp linseed", metric: "2 tbsp linseed"))
        ingrediens.append(createIngredient(imperial: "2 tbsp sesame", metric: "2 tbsp sesame"))
        ingrediens.append(createIngredient(imperial: "3/4 glass honey", metric: "3/4 glass honey"))
        ingrediens.append(createIngredient(imperial: "17.6 fl oz yoghurt", metric: "500 ml yoghurt"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Crumble the gingerbread and add nuts, oat flakes, linseed, sesame and ½ glass of honey. Mix and put on the baking tray laid with some baking paper. Put into the preheated oven to 302°F for about 30 minutes. Mix the remaining honey with the yoghurt. Put the yoghurt alternately with granola in the glass dish.", step: "Crumble the gingerbread and add nuts, oat flakes, linseed, sesame and ½ glass of honey. Mix and put on the baking tray laid with some baking paper. Put into the preheated oven to 150°C for about 30 minutes. Mix the remaining honey with the yoghurt. Put the yoghurt alternately with granola in the glass dish."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 35, recipeName: "Gingerbread granola with yoghurt", recipeAuthor: "Roger Kołomocki ", recipeBlog: "www.cookzoom.pl", recipeThumb: "36t", recipeImage: "36", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "17.6 fl oz whipping cream, 36% fat", metric: "500 ml whipping cream, 36% fat"))
        ingrediens.append(createIngredient(imperial: "1 packet Katarzynki® without chocolate ", metric: "1 packet Katarzynki® without chocolate "))
        ingrediens.append(createIngredient(imperial: "4 yolks", metric: "4 yolks"))
        ingrediens.append(createIngredient(imperial: "1.8 oz sugar", metric: "50 g sugar"))
        ingrediens.append(createIngredient(imperial: "1/2 bar dark chocolate", metric: "1/2 bar dark chocolate"))
        ingrediens.append(createIngredient(imperial: "1.8 oz nuts", metric: "50 g nuts"))
        ingrediens.append(createIngredient(imperial: "1.8 oz raisins or other dried fruit and nuts", metric: "50 g raisins or other dried fruit and nuts"))

        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Mix the yolks with sugar into smooth mixture. Pour the cream and put crumbled gingerbread in another dish. Mix adding earlier whisked yolks. At the end, add finely crushed chocolate, fruit and nuts. Mix all together. Put the ice cream into a metal tin or a plastic box and freeze all night. At the very beginning, mix the mixture every hour (3-4 times). You can decorate the ice cream with some gingerbread and sprinkle with some cinnamon or cocoa powder.", step: "Mix the yolks with sugar into smooth mixture. Pour the cream and put crumbled gingerbread in another dish. Mix adding earlier whisked yolks. At the end, add finely crushed chocolate, fruit and nuts. Mix all together. Put the ice cream into a metal tin or a plastic box and freeze all night. At the very beginning, mix the mixture every hour (3-4 times). You can decorate the ice cream with some gingerbread and sprinkle with some cinnamon or cocoa powder."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 36, recipeName: "Gingerbread ice cream", recipeAuthor: "Roger Kołomocki", recipeBlog: "www.cookzoom.pl", recipeThumb: "37t", recipeImage: "37", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a baking tin sized 8 inch x 11 inch/", metric: "/a baking tin sized 21 cm x 28 cm/",title: true))
        ingrediens.append(createIngredient(imperial: "Cake", metric: "Cake",title: true))
        ingrediens.append(createIngredient(imperial: "7 oz butter", metric: "200 g butter"))
        ingrediens.append(createIngredient(imperial: "7 oz dark chocolate, containing 70% cocoa", metric: "200 g dark chocolate, containing 70% cocoa"))
        ingrediens.append(createIngredient(imperial: "4 eggs ", metric: "4 eggs "))
        ingrediens.append(createIngredient(imperial: "8 oz cane sugar", metric: "230 g cane sugar"))
        ingrediens.append(createIngredient(imperial: "6.3 oz spelt flour 6.3 oz", metric: "180 g spelt flour 180g"))
        ingrediens.append(createIngredient(imperial: "pinch Himalayan salt", metric: "pinch Himalayan salt"))
        ingrediens.append(createIngredient(imperial: "0.5 tsp cardamom", metric: "0.5 tsp cardamom"))
        ingrediens.append(createIngredient(imperial: "1 tbsp nut liqueur /optional/", metric: "1 tbsp nut liqueur /optional/"))
        ingrediens.append(createIngredient(imperial: "1 packet Mieszanka Toruńska", metric: "1 packet Mieszanka Toruńska"))
        ingrediens.append(createIngredient(imperial: "Glazing", metric: "Glazing",title: true))
        ingrediens.append(createIngredient(imperial: "3.5 oz dark chocolate, containing 70% cocoa", metric: "100 g dark chocolate, containing 70% cocoa"))
        ingrediens.append(createIngredient(imperial: "For decoration", metric: "For decoration",title: true))
        ingrediens.append(createIngredient(imperial: "melon balls and kiwi", metric: "melon balls and kiwi"))
        ingrediens.append(createIngredient(imperial: "watermelon hearts", metric: "watermelon hearts"))
        ingrediens.append(createIngredient(imperial: "mint", metric: "mint"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Melt the butter in a saucepan over low heat and next, add some chocolate broken into pieces. Heat the ingredients and mix until they all melt and combine completely. Take the mixture off the cooker and leave to cool down a little. Mix (using a beater or a hand blender) eggs, cane sugar and nut liqueur in a bowl.", step: "Melt the butter in a saucepan over low heat and next, add some chocolate broken into pieces. Heat the ingredients and mix until they all melt and combine completely. Take the mixture off the cooker and leave to cool down a little. Mix (using a beater or a hand blender) eggs, cane sugar and nut liqueur in a bowl."))
        methods.append(createMethodStep(imperialStep: "Add the earlier prepared butter and chocolate mixture into the egg mixture. Mix (or blend awhile) to combine the ingredients. Add sifted spelt flour, a pinch of Himalayan salt and cardamom. Mix until the ingredients combine. Add thickly cut Mieszanka Toruńska (leave 5 pieces of gingerbread for decoration)", step: "Add the earlier prepared butter and chocolate mixture into the egg mixture. Mix (or blend awhile) to combine the ingredients. Add sifted spelt flour, a pinch of Himalayan salt and cardamom. Mix until the ingredients combine. Add thickly cut Mieszanka Toruńska (leave 5 pieces of gingerbread for decoration)"))
        methods.append(createMethodStep(imperialStep: "Mix the prepared mixture a little to combine cut gingerbread with the cake. Pour it into a baking tin laid with some baking paper. Put the gingerbread, left earlier, on top and squash slightly the inside. Bake the cake at 320°F for about 35 minutes. Pour the dark chocolate melted over hot water on the baked and cooled cake. Use melon, kiwi, watermelon and mint to decorate.", step: "Mix the prepared mixture a little to combine cut gingerbread with the cake. Pour it into a baking tin laid with some baking paper. Put the gingerbread, left earlier, on top and squash slightly the inside. Bake the cake at 160°C for about 35 minutes. Pour the dark chocolate melted over hot water on the baked and cooled cake. Use melon, kiwi, watermelon and mint to decorate."))
    
        
        // ---- recipe
        recipe = Recipe(recipeID: 37, recipeName: "Brownie with Mieszanka Toruńska® (Toruń Gingerbread Mix)", recipeAuthor: "Roksana Wilk", recipeBlog: "www.instagram.com/planet.kai", recipeThumb: "38t", recipeImage: "38", recipeIngredients: ingrediens, recipeMethods: methods)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/a recipe for approx. 8-10 pieces/", metric: "/a recipe for approx. 8-10 pieces/",title: true))
        ingrediens.append(createIngredient(imperial: "Pancake batter", metric: "Pancake batter",title: true))
        ingrediens.append(createIngredient(imperial: "1 glass skimmed milk, 3,2 % fat", metric: "1 glass skimmed milk, 3,2 % fat"))
        ingrediens.append(createIngredient(imperial: "1 glass wheat flour", metric: "1 glass wheat flour"))
        ingrediens.append(createIngredient(imperial: "pinch cinnamon", metric: "pinch cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 tsp vanilla sugar", metric: "1 tsp vanilla sugar"))
        ingrediens.append(createIngredient(imperial: "2 eggs", metric: "2 eggs"))
        ingrediens.append(createIngredient(imperial: "3/4 glass sparking water", metric: "3/4 glass sparking water"))
        ingrediens.append(createIngredient(imperial: "for frying: coconut oil", metric: "for frying: coconut oil"))
        ingrediens.append(createIngredient(imperial: "for decorating: 1 packet Katarzynki® VEGAN", metric: "for decorating: 1 packet Katarzynki® VEGAN"))
        ingrediens.append(createIngredient(imperial: "Plum mousse", metric: "Plum mousse",title: true))
        ingrediens.append(createIngredient(imperial: "approx. 35.3 oz damson plums", metric: "approx. 1 kg damson plums"))
        ingrediens.append(createIngredient(imperial: "1 cinnamon stick", metric: "1 cinnamon stick"))
        ingrediens.append(createIngredient(imperial: "3-4 tbsp sugar or xylitol", metric: "3-4 tbsp sugar or xylitol"))
        ingrediens.append(createIngredient(imperial: "1 vanilla pod /you can replace it with vanilla extract/", metric: "1 vanilla pod /you can replace it with vanilla extract/"))
        ingrediens.append(createIngredient(imperial: "1/2 tsp nutmeg + 2 sprigs fresh thyme", metric: "1/2 tsp nutmeg + 2 sprigs fresh thyme"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "To make pancakes: mix all ingredients for the batter into smooth mixture. Cool the batter in the fridge for about 30 minutes. Fry the pancakes on coconut oil in a preheated frying pan.", step: "To make pancakes: mix all ingredients for the batter into smooth mixture. Cool the batter in the fridge for about 30 minutes. Fry the pancakes on coconut oil in a preheated frying pan."))
        methods.append(createMethodStep(imperialStep: "To make plum mousse: Heat the sugar, fresh thyme and cinnamon in a pot with cast-iron bottom. Next, add stoned plums and vanilla pod cut alongside. Bring to boil, reduce the heat and leave covered for about 15 minutes – mixing from time to time to prevent the plums from sticking to the bottom. Serve the pancakes with warm plum mousse and thickly grated gingerbread.", step: "To make plum mousse: Heat the sugar, fresh thyme and cinnamon in a pot with cast-iron bottom. Next, add stoned plums and vanilla pod cut alongside. Bring to boil, reduce the heat and leave covered for about 15 minutes – mixing from time to time to prevent the plums from sticking to the bottom. Serve the pancakes with warm plum mousse and thickly grated gingerbread."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 38, recipeName: "Pancakes with gingerbread topping", recipeAuthor: "Zofia Cudny", recipeBlog: "www.makecookingeasier.pl", recipeThumb: "39t", recipeImage: "39", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "/2 servings/", metric: "/2 servings/",title: true))
        ingrediens.append(createIngredient(imperial: "1/2 glass oat flakes", metric: "1/2 glass oat flakes"))
        ingrediens.append(createIngredient(imperial: "1 tbsp tahini paste", metric: "1 tbsp tahini paste"))
        ingrediens.append(createIngredient(imperial: "1 glass almond milk", metric: "1 glass almond milk"))
        ingrediens.append(createIngredient(imperial: "1 tbsp maple syrup", metric: "1 tbsp maple syrup"))
        ingrediens.append(createIngredient(imperial: "1/2 glass seasonal fruit", metric: "1/2 glass seasonal fruit"))
        ingrediens.append(createIngredient(imperial: "1 packet Katarzynki® VEGAN", metric: "1 packet Katarzynki® VEGAN"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Heat the oat flakes with almond milk, maple syrup and sesame paste (tahini) in a saucepan. Cook until the mixture thickens. When the oat flakes absorb the liquid, blend it into smooth mixture. If it is too dense, add some more milk. Put the mixture into cups alternately with gingerbread and fruit. Put the dessert aside for 5-10 minutes so the flavours combine.", step: "Heat the oat flakes with almond milk, maple syrup and sesame paste (tahini) in a saucepan. Cook until the mixture thickens. When the oat flakes absorb the liquid, blend it into smooth mixture. If it is too dense, add some more milk. Put the mixture into cups alternately with gingerbread and fruit. Put the dessert aside for 5-10 minutes so the flavours combine."))
        methods.append(createMethodStep(imperialStep: "Options", step: "Options",title: true))
        methods.append(createMethodStep(imperialStep: "Instead of oat flakes you can use: millet groats, rice, spelt flakes or rye flakes.", step: "Instead of oat flakes you can use: millet groats, rice, spelt flakes or rye flakes."))
        methods.append(createMethodStep(imperialStep: "You can replace tahini paste with any peanut butter.", step: "You can replace tahini paste with any peanut butter."))
        methods.append(createMethodStep(imperialStep: "You can replace almond milk with any vegetable milk or natural cow’s milk.", step: "You can replace almond milk with any vegetable milk or natural cow’s milk."))
        methods.append(createMethodStep(imperialStep: "You can replace maple syrup with: agave syrup, dates, honey, xylitol, coconut sugar.", step: "You can replace maple syrup with: agave syrup, dates, honey, xylitol, coconut sugar."))
        
        // ---- recipe
        recipe = Recipe(recipeID: 39, recipeName: "Oat pudding with Katarzynki® VEGAN gingerbread", recipeAuthor: "Kinga Paruzel", recipeBlog: "www.kingaparuzel.pl", recipeThumb: "40t", recipeImage: "40", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        // -- recipe
        // ---- ingredients
        ingrediens = []
        ingrediens.append(createIngredient(imperial: "3-4 packet Katarzynki® VEGAN", metric: "3-4 packet Katarzynki® VEGAN"))
        ingrediens.append(createIngredient(imperial: "10.6 oz baked pumpkin e.g. Hokkaido", metric: "300 g baked pumpkin e.g. Hokkaido"))
        ingrediens.append(createIngredient(imperial: "7 oz millet or spelt flour", metric: "200 g millet or spelt flour"))
        ingrediens.append(createIngredient(imperial: "4-5 tbsp cane or coconut sugar", metric: "4-5 tbsp cane or coconut sugar"))
        ingrediens.append(createIngredient(imperial: "approximately 1/2 glass vegetable milk", metric: "approximately 1/2 glass vegetable milk"))
        ingrediens.append(createIngredient(imperial: "2 bar dark chocolate", metric: "2 bar dark chocolate"))
        ingrediens.append(createIngredient(imperial: "1-2 tbsp dried ginger", metric: "1-2 tbsp dried ginger"))
        ingrediens.append(createIngredient(imperial: "1 tbsp cinnamon", metric: "1 tbsp cinnamon"))
        ingrediens.append(createIngredient(imperial: "1 tbsp gluten-free baking powder", metric: "1 tbsp gluten-free baking powder"))
        ingrediens.append(createIngredient(imperial: "2 tbsp oil", metric: "2 tbsp oil"))
        ingrediens.append(createIngredient(imperial: "pinch salt", metric: "pinch salt"))
        
        // ---- methods
        methods = []
        methods.append(createMethodStep(imperialStep: "Halve the pumpkin. Bake at 356ºF-392ºF for about 20 minutes until tender. Put the cooled pumpkin pulp into the bowl. Blend it into smooth mixture. Add millet flour, sugar, spices, baking powder and salt. Mix it. Slowly add milk so the mixture stays soggy and slightly runny. Melt the chocolate over hot water, add it into the prepared mixture and mix it thoroughly. The dough should have plain colour. At the end, add some oil and mix it again.", step: "Halve the pumpkin. Bake at 180-200ºC for about 20 minutes until tender. Put the cooled pumpkin pulp into the bowl. Blend it into smooth mixture. Add millet flour, sugar, spices, baking powder and salt. Mix it. Slowly add milk so the mixture stays soggy and slightly runny. Melt the chocolate over hot water, add it into the prepared mixture and mix it thoroughly. The dough should have plain colour. At the end, add some oil and mix it again."))
        methods.append(createMethodStep(imperialStep: "Grease a little the bottom of the baking tin, put Katarzynki® VEGAN to cover the bottom of it. Pour the dough and smoothen with a paddle. If there is a piece of gingerbread left, you can crumble it and sprinkle the top of the cake. Bake it in the oven preheated to 347ºF-356ºF to so called ’dry stick’.", step: "Grease a little the bottom of the baking tin, put Katarzynki® VEGAN to cover the bottom of it. Pour the dough and smoothen with a paddle. If there is a piece of gingerbread left, you can crumble it and sprinkle the top of the cake. Bake it in the oven preheated to 175-180ºC to so called ’dry stick’."))
        methods.append(createMethodStep(imperialStep: "Eat warm and preferably with ice cream!", step: "Eat warm and preferably with ice cream! "))
        
        // ---- recipe
        recipe = Recipe(recipeID: 40, recipeName: "Spicy brownie with pumpkin on a gingerbread base", recipeAuthor: "Alicja Rokicka", recipeBlog: "www.wegannerd.blogspot.com", recipeThumb: "41t", recipeImage: "41", recipeIngredients: ingrediens, recipeMethods: methods, recipeGuest: true)
        recipes.append(recipe)
        // --------------------------------------------------------------------------------
        
        
        
        
        
    }
    return recipes
}

func createIngredient(imperial:String,metric:String,title:Bool = false) -> Ingredient {
    let ingredientName:Ingredient = Ingredient(ingredientNameImperial: imperial, ingredientNameMetric: metric, isTitle:title)
    return ingredientName
}

func createMethodStep(imperialStep: String = "",step:String,title:Bool = false) -> Method {
    let methodStep:Method = Method(imperialStep: imperialStep, methodStep: step, isTitle: title)
    return methodStep
}

func createEmptyRecipe() -> Recipe {
    let recipe = Recipe(recipeID: 0, recipeName: "", recipeAuthor: "", recipeBlog: "", recipeThumb: "", recipeImage: "", recipeIngredients: [], recipeMethods: [])
    return recipe
}
